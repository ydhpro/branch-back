package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.AddExp;
import com.jd.branch.api.request.ReqAddExpSave;
import com.jd.branch.api.request.ReqAddExpSearch;
import com.jd.branch.api.response.RespAddExpDetail;
import java.util.List;


/**
 * <p>
 * 增项费用 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface AddExpService {
    PageInfo<RespAddExpDetail> search(LoginRedisObj loginRedisObj,ReqAddExpSearch reqAddExpSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqAddExpSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqAddExpSave save) throws Exception;

    RespAddExpDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespAddExpDetail> findList(LoginRedisObj loginRedisObj, ReqAddExpSearch item) throws Exception;
}