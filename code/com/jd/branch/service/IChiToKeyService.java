package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ChiToKey;
import com.jd.branch.api.request.ReqChiToKeySave;
import com.jd.branch.api.request.ReqChiToKeySearch;
import com.jd.branch.api.response.RespChiToKeyDetail;
import java.util.List;


/**
 * <p>
 * 中文对应键 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface ChiToKeyService {
    PageInfo<RespChiToKeyDetail> search(LoginRedisObj loginRedisObj,ReqChiToKeySearch reqChiToKeySearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqChiToKeySave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqChiToKeySave save) throws Exception;

    RespChiToKeyDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespChiToKeyDetail> findList(LoginRedisObj loginRedisObj, ReqChiToKeySearch item) throws Exception;
}