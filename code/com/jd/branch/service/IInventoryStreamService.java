package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.InventoryStream;
import com.jd.branch.api.request.ReqInventoryStreamSave;
import com.jd.branch.api.request.ReqInventoryStreamSearch;
import com.jd.branch.api.response.RespInventoryStreamDetail;
import java.util.List;


/**
 * <p>
 * 库存流水 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface InventoryStreamService {
    PageInfo<RespInventoryStreamDetail> search(LoginRedisObj loginRedisObj,ReqInventoryStreamSearch reqInventoryStreamSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqInventoryStreamSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqInventoryStreamSave save) throws Exception;

    RespInventoryStreamDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespInventoryStreamDetail> findList(LoginRedisObj loginRedisObj, ReqInventoryStreamSearch item) throws Exception;
}