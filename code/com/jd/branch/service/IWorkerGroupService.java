package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.WorkerGroup;
import com.jd.branch.api.request.ReqWorkerGroupSave;
import com.jd.branch.api.request.ReqWorkerGroupSearch;
import com.jd.branch.api.response.RespWorkerGroupDetail;
import java.util.List;


/**
 * <p>
 * 师傅组 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface WorkerGroupService {
    PageInfo<RespWorkerGroupDetail> search(LoginRedisObj loginRedisObj,ReqWorkerGroupSearch reqWorkerGroupSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqWorkerGroupSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqWorkerGroupSave save) throws Exception;

    RespWorkerGroupDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespWorkerGroupDetail> findList(LoginRedisObj loginRedisObj, ReqWorkerGroupSearch item) throws Exception;
}