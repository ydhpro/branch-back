package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.AppointModel;
import com.jd.branch.api.request.ReqAppointModelSave;
import com.jd.branch.api.request.ReqAppointModelSearch;
import com.jd.branch.api.response.RespAppointModelDetail;
import java.util.List;


/**
 * <p>
 * 预约信息模板 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface AppointModelService {
    PageInfo<RespAppointModelDetail> search(LoginRedisObj loginRedisObj,ReqAppointModelSearch reqAppointModelSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqAppointModelSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqAppointModelSave save) throws Exception;

    RespAppointModelDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespAppointModelDetail> findList(LoginRedisObj loginRedisObj, ReqAppointModelSearch item) throws Exception;
}