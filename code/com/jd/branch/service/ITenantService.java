package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Tenant;
import com.jd.branch.api.request.ReqTenantSave;
import com.jd.branch.api.request.ReqTenantSearch;
import com.jd.branch.api.response.RespTenantDetail;
import java.util.List;


/**
 * <p>
 * 租户 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface TenantService {
    PageInfo<RespTenantDetail> search(LoginRedisObj loginRedisObj,ReqTenantSearch reqTenantSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqTenantSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqTenantSave save) throws Exception;

    RespTenantDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespTenantDetail> findList(LoginRedisObj loginRedisObj, ReqTenantSearch item) throws Exception;
}