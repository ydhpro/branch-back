package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ComBrand;
import com.jd.branch.api.request.ReqComBrandSave;
import com.jd.branch.api.request.ReqComBrandSearch;
import com.jd.branch.api.response.RespComBrandDetail;
import java.util.List;


/**
 * <p>
 * 合作品牌相关参数 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface ComBrandService {
    PageInfo<RespComBrandDetail> search(LoginRedisObj loginRedisObj,ReqComBrandSearch reqComBrandSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqComBrandSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqComBrandSave save) throws Exception;

    RespComBrandDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespComBrandDetail> findList(LoginRedisObj loginRedisObj, ReqComBrandSearch item) throws Exception;
}