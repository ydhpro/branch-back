package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.SurveyInstallOrder;
import com.jd.branch.api.request.ReqSurveyInstallOrderSave;
import com.jd.branch.api.request.ReqSurveyInstallOrderSearch;
import com.jd.branch.api.response.RespSurveyInstallOrderDetail;
import java.util.List;


/**
 * <p>
 * 勘安工单 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface SurveyInstallOrderService {
    PageInfo<RespSurveyInstallOrderDetail> search(LoginRedisObj loginRedisObj,ReqSurveyInstallOrderSearch reqSurveyInstallOrderSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave save) throws Exception;

    RespSurveyInstallOrderDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespSurveyInstallOrderDetail> findList(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch item) throws Exception;
}