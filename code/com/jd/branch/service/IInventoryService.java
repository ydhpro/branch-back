package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Inventory;
import com.jd.branch.api.request.ReqInventorySave;
import com.jd.branch.api.request.ReqInventorySearch;
import com.jd.branch.api.response.RespInventoryDetail;
import java.util.List;


/**
 * <p>
 * 库存 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface InventoryService {
    PageInfo<RespInventoryDetail> search(LoginRedisObj loginRedisObj,ReqInventorySearch reqInventorySearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqInventorySave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqInventorySave save) throws Exception;

    RespInventoryDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespInventoryDetail> findList(LoginRedisObj loginRedisObj, ReqInventorySearch item) throws Exception;
}