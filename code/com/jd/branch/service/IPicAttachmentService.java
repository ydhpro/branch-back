package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.PicAttachment;
import com.jd.branch.api.request.ReqPicAttachmentSave;
import com.jd.branch.api.request.ReqPicAttachmentSearch;
import com.jd.branch.api.response.RespPicAttachmentDetail;
import java.util.List;


/**
 * <p>
 * 图片附件 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface PicAttachmentService {
    PageInfo<RespPicAttachmentDetail> search(LoginRedisObj loginRedisObj,ReqPicAttachmentSearch reqPicAttachmentSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqPicAttachmentSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqPicAttachmentSave save) throws Exception;

    RespPicAttachmentDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespPicAttachmentDetail> findList(LoginRedisObj loginRedisObj, ReqPicAttachmentSearch item) throws Exception;
}