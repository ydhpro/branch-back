package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.PicTypeDic;
import com.jd.branch.api.request.ReqPicTypeDicSave;
import com.jd.branch.api.request.ReqPicTypeDicSearch;
import com.jd.branch.api.response.RespPicTypeDicDetail;
import java.util.List;


/**
 * <p>
 * 图片类型字典 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface PicTypeDicService {
    PageInfo<RespPicTypeDicDetail> search(LoginRedisObj loginRedisObj,ReqPicTypeDicSearch reqPicTypeDicSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqPicTypeDicSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqPicTypeDicSave save) throws Exception;

    RespPicTypeDicDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespPicTypeDicDetail> findList(LoginRedisObj loginRedisObj, ReqPicTypeDicSearch item) throws Exception;
}