package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ProLedgerItem;
import com.jd.branch.api.request.ReqProLedgerItemSave;
import com.jd.branch.api.request.ReqProLedgerItemSearch;
import com.jd.branch.api.response.RespProLedgerItemDetail;
import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface ProLedgerItemService {
    PageInfo<RespProLedgerItemDetail> search(LoginRedisObj loginRedisObj,ReqProLedgerItemSearch reqProLedgerItemSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqProLedgerItemSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqProLedgerItemSave save) throws Exception;

    RespProLedgerItemDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespProLedgerItemDetail> findList(LoginRedisObj loginRedisObj, ReqProLedgerItemSearch item) throws Exception;
}