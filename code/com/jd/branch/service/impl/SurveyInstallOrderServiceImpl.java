package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqSurveyInstallOrderSave;
import com.jd.branch.api.request.ReqSurveyInstallOrderSearch;
import com.jd.branch.api.response.RespSurveyInstallOrderDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.SurveyInstallOrder;
import com.jd.branch.mapper.SurveyInstallOrderMapper;
import com.jd.branch.service.SurveyInstallOrderService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 勘安工单 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class SurveyInstallOrderServiceImpl  implements ISurveyInstallOrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger( SurveyInstallOrderServiceImpl.class);
    @Autowired
    private SurveyInstallOrderMapper mapper;


    @Override
    public PageInfo<RespSurveyInstallOrderDetail> search(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch param) {
        //1.构造查询条件
        QueryWrapper<SurveyInstallOrder> wrapper = new QueryWrapper<>(
        SurveyInstallOrder.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<SurveyInstallOrder> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<SurveyInstallOrder> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespSurveyInstallOrderDetail> details = new ArrayList<>();
        for(SurveyInstallOrder item:iPage.getRecords()){
            RespSurveyInstallOrderDetail resp = DefindBeanUtil.copyProperties(item, RespSurveyInstallOrderDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespSurveyInstallOrderDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespSurveyInstallOrderDetail> findList(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch req)throws Exception{
        SurveyInstallOrder param = JSONObject.parseObject(JSONObject.toJSONString(req), SurveyInstallOrder.class);
        List<SurveyInstallOrder> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespSurveyInstallOrderDetail> details = new ArrayList<>();
        for(SurveyInstallOrder item:list){
            RespSurveyInstallOrderDetail detail = DefindBeanUtil.copyProperties(item, RespSurveyInstallOrderDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqSurveyInstallOrderSave item) {
        SurveyInstallOrder update = DefindBeanUtil.copyProperties(item, SurveyInstallOrder.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave add){
        SurveyInstallOrder save = JSONObject.parseObject(JSONObject.toJSONString(add), SurveyInstallOrder.class);
        mapper.insert(save);
    }

    @Override
    public RespSurveyInstallOrderDetail detail(LoginRedisObj loginRedisObj, Long id){
        SurveyInstallOrder byId = mapper.selectById(id);
        RespSurveyInstallOrderDetail resp = transferOne(byId);
        return resp;
    }

    private RespSurveyInstallOrderDetail transferOne(SurveyInstallOrder item){
        RespSurveyInstallOrderDetail resp = DefindBeanUtil.copyProperties(item, RespSurveyInstallOrderDetail.class);
        return resp;
    }
}
