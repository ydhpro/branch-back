package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqAddCategorySave;
import com.jd.branch.api.request.ReqAddCategorySearch;
import com.jd.branch.api.response.RespAddCategoryDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.AddCategory;
import com.jd.branch.mapper.AddCategoryMapper;
import com.jd.branch.service.AddCategoryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 增项类目 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class AddCategoryServiceImpl  implements IAddCategoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger( AddCategoryServiceImpl.class);
    @Autowired
    private AddCategoryMapper mapper;


    @Override
    public PageInfo<RespAddCategoryDetail> search(LoginRedisObj loginRedisObj, ReqAddCategorySearch param) {
        //1.构造查询条件
        QueryWrapper<AddCategory> wrapper = new QueryWrapper<>(
        AddCategory.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<AddCategory> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<AddCategory> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespAddCategoryDetail> details = new ArrayList<>();
        for(AddCategory item:iPage.getRecords()){
            RespAddCategoryDetail resp = DefindBeanUtil.copyProperties(item, RespAddCategoryDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespAddCategoryDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespAddCategoryDetail> findList(LoginRedisObj loginRedisObj, ReqAddCategorySearch req)throws Exception{
        AddCategory param = JSONObject.parseObject(JSONObject.toJSONString(req), AddCategory.class);
        List<AddCategory> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespAddCategoryDetail> details = new ArrayList<>();
        for(AddCategory item:list){
            RespAddCategoryDetail detail = DefindBeanUtil.copyProperties(item, RespAddCategoryDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqAddCategorySave item) {
        AddCategory update = DefindBeanUtil.copyProperties(item, AddCategory.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqAddCategorySave add){
        AddCategory save = JSONObject.parseObject(JSONObject.toJSONString(add), AddCategory.class);
        mapper.insert(save);
    }

    @Override
    public RespAddCategoryDetail detail(LoginRedisObj loginRedisObj, Long id){
        AddCategory byId = mapper.selectById(id);
        RespAddCategoryDetail resp = transferOne(byId);
        return resp;
    }

    private RespAddCategoryDetail transferOne(AddCategory item){
        RespAddCategoryDetail resp = DefindBeanUtil.copyProperties(item, RespAddCategoryDetail.class);
        return resp;
    }
}
