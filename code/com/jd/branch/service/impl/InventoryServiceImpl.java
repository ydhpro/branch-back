package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqInventorySave;
import com.jd.branch.api.request.ReqInventorySearch;
import com.jd.branch.api.response.RespInventoryDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Inventory;
import com.jd.branch.mapper.InventoryMapper;
import com.jd.branch.service.InventoryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 库存 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class InventoryServiceImpl  implements IInventoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger( InventoryServiceImpl.class);
    @Autowired
    private InventoryMapper mapper;


    @Override
    public PageInfo<RespInventoryDetail> search(LoginRedisObj loginRedisObj, ReqInventorySearch param) {
        //1.构造查询条件
        QueryWrapper<Inventory> wrapper = new QueryWrapper<>(
        Inventory.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<Inventory> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<Inventory> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespInventoryDetail> details = new ArrayList<>();
        for(Inventory item:iPage.getRecords()){
            RespInventoryDetail resp = DefindBeanUtil.copyProperties(item, RespInventoryDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespInventoryDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespInventoryDetail> findList(LoginRedisObj loginRedisObj, ReqInventorySearch req)throws Exception{
        Inventory param = JSONObject.parseObject(JSONObject.toJSONString(req), Inventory.class);
        List<Inventory> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespInventoryDetail> details = new ArrayList<>();
        for(Inventory item:list){
            RespInventoryDetail detail = DefindBeanUtil.copyProperties(item, RespInventoryDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqInventorySave item) {
        Inventory update = DefindBeanUtil.copyProperties(item, Inventory.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqInventorySave add){
        Inventory save = JSONObject.parseObject(JSONObject.toJSONString(add), Inventory.class);
        mapper.insert(save);
    }

    @Override
    public RespInventoryDetail detail(LoginRedisObj loginRedisObj, Long id){
        Inventory byId = mapper.selectById(id);
        RespInventoryDetail resp = transferOne(byId);
        return resp;
    }

    private RespInventoryDetail transferOne(Inventory item){
        RespInventoryDetail resp = DefindBeanUtil.copyProperties(item, RespInventoryDetail.class);
        return resp;
    }
}
