package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqPicAttachmentSave;
import com.jd.branch.api.request.ReqPicAttachmentSearch;
import com.jd.branch.api.response.RespPicAttachmentDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.PicAttachment;
import com.jd.branch.mapper.PicAttachmentMapper;
import com.jd.branch.service.PicAttachmentService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 图片附件 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class PicAttachmentServiceImpl  implements IPicAttachmentService {
    private static final Logger LOGGER = LoggerFactory.getLogger( PicAttachmentServiceImpl.class);
    @Autowired
    private PicAttachmentMapper mapper;


    @Override
    public PageInfo<RespPicAttachmentDetail> search(LoginRedisObj loginRedisObj, ReqPicAttachmentSearch param) {
        //1.构造查询条件
        QueryWrapper<PicAttachment> wrapper = new QueryWrapper<>(
        PicAttachment.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<PicAttachment> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<PicAttachment> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespPicAttachmentDetail> details = new ArrayList<>();
        for(PicAttachment item:iPage.getRecords()){
            RespPicAttachmentDetail resp = DefindBeanUtil.copyProperties(item, RespPicAttachmentDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespPicAttachmentDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespPicAttachmentDetail> findList(LoginRedisObj loginRedisObj, ReqPicAttachmentSearch req)throws Exception{
        PicAttachment param = JSONObject.parseObject(JSONObject.toJSONString(req), PicAttachment.class);
        List<PicAttachment> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespPicAttachmentDetail> details = new ArrayList<>();
        for(PicAttachment item:list){
            RespPicAttachmentDetail detail = DefindBeanUtil.copyProperties(item, RespPicAttachmentDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqPicAttachmentSave item) {
        PicAttachment update = DefindBeanUtil.copyProperties(item, PicAttachment.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqPicAttachmentSave add){
        PicAttachment save = JSONObject.parseObject(JSONObject.toJSONString(add), PicAttachment.class);
        mapper.insert(save);
    }

    @Override
    public RespPicAttachmentDetail detail(LoginRedisObj loginRedisObj, Long id){
        PicAttachment byId = mapper.selectById(id);
        RespPicAttachmentDetail resp = transferOne(byId);
        return resp;
    }

    private RespPicAttachmentDetail transferOne(PicAttachment item){
        RespPicAttachmentDetail resp = DefindBeanUtil.copyProperties(item, RespPicAttachmentDetail.class);
        return resp;
    }
}
