package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqOrderFeeSave;
import com.jd.branch.api.request.ReqOrderFeeSearch;
import com.jd.branch.api.response.RespOrderFeeDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.OrderFee;
import com.jd.branch.mapper.OrderFeeMapper;
import com.jd.branch.service.OrderFeeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 单据费用 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class OrderFeeServiceImpl  implements IOrderFeeService {
    private static final Logger LOGGER = LoggerFactory.getLogger( OrderFeeServiceImpl.class);
    @Autowired
    private OrderFeeMapper mapper;


    @Override
    public PageInfo<RespOrderFeeDetail> search(LoginRedisObj loginRedisObj, ReqOrderFeeSearch param) {
        //1.构造查询条件
        QueryWrapper<OrderFee> wrapper = new QueryWrapper<>(
        OrderFee.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<OrderFee> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<OrderFee> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespOrderFeeDetail> details = new ArrayList<>();
        for(OrderFee item:iPage.getRecords()){
            RespOrderFeeDetail resp = DefindBeanUtil.copyProperties(item, RespOrderFeeDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespOrderFeeDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespOrderFeeDetail> findList(LoginRedisObj loginRedisObj, ReqOrderFeeSearch req)throws Exception{
        OrderFee param = JSONObject.parseObject(JSONObject.toJSONString(req), OrderFee.class);
        List<OrderFee> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespOrderFeeDetail> details = new ArrayList<>();
        for(OrderFee item:list){
            RespOrderFeeDetail detail = DefindBeanUtil.copyProperties(item, RespOrderFeeDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqOrderFeeSave item) {
        OrderFee update = DefindBeanUtil.copyProperties(item, OrderFee.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqOrderFeeSave add){
        OrderFee save = JSONObject.parseObject(JSONObject.toJSONString(add), OrderFee.class);
        mapper.insert(save);
    }

    @Override
    public RespOrderFeeDetail detail(LoginRedisObj loginRedisObj, Long id){
        OrderFee byId = mapper.selectById(id);
        RespOrderFeeDetail resp = transferOne(byId);
        return resp;
    }

    private RespOrderFeeDetail transferOne(OrderFee item){
        RespOrderFeeDetail resp = DefindBeanUtil.copyProperties(item, RespOrderFeeDetail.class);
        return resp;
    }
}
