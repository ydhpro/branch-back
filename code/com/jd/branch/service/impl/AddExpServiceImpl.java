package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqAddExpSave;
import com.jd.branch.api.request.ReqAddExpSearch;
import com.jd.branch.api.response.RespAddExpDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.AddExp;
import com.jd.branch.mapper.AddExpMapper;
import com.jd.branch.service.AddExpService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 增项费用 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class AddExpServiceImpl  implements IAddExpService {
    private static final Logger LOGGER = LoggerFactory.getLogger( AddExpServiceImpl.class);
    @Autowired
    private AddExpMapper mapper;


    @Override
    public PageInfo<RespAddExpDetail> search(LoginRedisObj loginRedisObj, ReqAddExpSearch param) {
        //1.构造查询条件
        QueryWrapper<AddExp> wrapper = new QueryWrapper<>(
        AddExp.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<AddExp> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<AddExp> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespAddExpDetail> details = new ArrayList<>();
        for(AddExp item:iPage.getRecords()){
            RespAddExpDetail resp = DefindBeanUtil.copyProperties(item, RespAddExpDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespAddExpDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespAddExpDetail> findList(LoginRedisObj loginRedisObj, ReqAddExpSearch req)throws Exception{
        AddExp param = JSONObject.parseObject(JSONObject.toJSONString(req), AddExp.class);
        List<AddExp> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespAddExpDetail> details = new ArrayList<>();
        for(AddExp item:list){
            RespAddExpDetail detail = DefindBeanUtil.copyProperties(item, RespAddExpDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqAddExpSave item) {
        AddExp update = DefindBeanUtil.copyProperties(item, AddExp.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqAddExpSave add){
        AddExp save = JSONObject.parseObject(JSONObject.toJSONString(add), AddExp.class);
        mapper.insert(save);
    }

    @Override
    public RespAddExpDetail detail(LoginRedisObj loginRedisObj, Long id){
        AddExp byId = mapper.selectById(id);
        RespAddExpDetail resp = transferOne(byId);
        return resp;
    }

    private RespAddExpDetail transferOne(AddExp item){
        RespAddExpDetail resp = DefindBeanUtil.copyProperties(item, RespAddExpDetail.class);
        return resp;
    }
}
