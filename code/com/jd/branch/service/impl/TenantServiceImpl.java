package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqTenantSave;
import com.jd.branch.api.request.ReqTenantSearch;
import com.jd.branch.api.response.RespTenantDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Tenant;
import com.jd.branch.mapper.TenantMapper;
import com.jd.branch.service.TenantService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 租户 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class TenantServiceImpl  implements ITenantService {
    private static final Logger LOGGER = LoggerFactory.getLogger( TenantServiceImpl.class);
    @Autowired
    private TenantMapper mapper;


    @Override
    public PageInfo<RespTenantDetail> search(LoginRedisObj loginRedisObj, ReqTenantSearch param) {
        //1.构造查询条件
        QueryWrapper<Tenant> wrapper = new QueryWrapper<>(
        Tenant.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<Tenant> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<Tenant> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespTenantDetail> details = new ArrayList<>();
        for(Tenant item:iPage.getRecords()){
            RespTenantDetail resp = DefindBeanUtil.copyProperties(item, RespTenantDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespTenantDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespTenantDetail> findList(LoginRedisObj loginRedisObj, ReqTenantSearch req)throws Exception{
        Tenant param = JSONObject.parseObject(JSONObject.toJSONString(req), Tenant.class);
        List<Tenant> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespTenantDetail> details = new ArrayList<>();
        for(Tenant item:list){
            RespTenantDetail detail = DefindBeanUtil.copyProperties(item, RespTenantDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqTenantSave item) {
        Tenant update = DefindBeanUtil.copyProperties(item, Tenant.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqTenantSave add){
        Tenant save = JSONObject.parseObject(JSONObject.toJSONString(add), Tenant.class);
        mapper.insert(save);
    }

    @Override
    public RespTenantDetail detail(LoginRedisObj loginRedisObj, Long id){
        Tenant byId = mapper.selectById(id);
        RespTenantDetail resp = transferOne(byId);
        return resp;
    }

    private RespTenantDetail transferOne(Tenant item){
        RespTenantDetail resp = DefindBeanUtil.copyProperties(item, RespTenantDetail.class);
        return resp;
    }
}
