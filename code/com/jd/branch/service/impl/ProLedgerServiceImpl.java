package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqProLedgerSave;
import com.jd.branch.api.request.ReqProLedgerSearch;
import com.jd.branch.api.response.RespProLedgerDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ProLedger;
import com.jd.branch.mapper.ProLedgerMapper;
import com.jd.branch.service.ProLedgerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 物业台账 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class ProLedgerServiceImpl  implements IProLedgerService {
    private static final Logger LOGGER = LoggerFactory.getLogger( ProLedgerServiceImpl.class);
    @Autowired
    private ProLedgerMapper mapper;


    @Override
    public PageInfo<RespProLedgerDetail> search(LoginRedisObj loginRedisObj, ReqProLedgerSearch param) {
        //1.构造查询条件
        QueryWrapper<ProLedger> wrapper = new QueryWrapper<>(
        ProLedger.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<ProLedger> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<ProLedger> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespProLedgerDetail> details = new ArrayList<>();
        for(ProLedger item:iPage.getRecords()){
            RespProLedgerDetail resp = DefindBeanUtil.copyProperties(item, RespProLedgerDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespProLedgerDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespProLedgerDetail> findList(LoginRedisObj loginRedisObj, ReqProLedgerSearch req)throws Exception{
        ProLedger param = JSONObject.parseObject(JSONObject.toJSONString(req), ProLedger.class);
        List<ProLedger> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespProLedgerDetail> details = new ArrayList<>();
        for(ProLedger item:list){
            RespProLedgerDetail detail = DefindBeanUtil.copyProperties(item, RespProLedgerDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqProLedgerSave item) {
        ProLedger update = DefindBeanUtil.copyProperties(item, ProLedger.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqProLedgerSave add){
        ProLedger save = JSONObject.parseObject(JSONObject.toJSONString(add), ProLedger.class);
        mapper.insert(save);
    }

    @Override
    public RespProLedgerDetail detail(LoginRedisObj loginRedisObj, Long id){
        ProLedger byId = mapper.selectById(id);
        RespProLedgerDetail resp = transferOne(byId);
        return resp;
    }

    private RespProLedgerDetail transferOne(ProLedger item){
        RespProLedgerDetail resp = DefindBeanUtil.copyProperties(item, RespProLedgerDetail.class);
        return resp;
    }
}
