package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqWorkerGroupSave;
import com.jd.branch.api.request.ReqWorkerGroupSearch;
import com.jd.branch.api.response.RespWorkerGroupDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.WorkerGroup;
import com.jd.branch.mapper.WorkerGroupMapper;
import com.jd.branch.service.WorkerGroupService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 师傅组 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class WorkerGroupServiceImpl  implements IWorkerGroupService {
    private static final Logger LOGGER = LoggerFactory.getLogger( WorkerGroupServiceImpl.class);
    @Autowired
    private WorkerGroupMapper mapper;


    @Override
    public PageInfo<RespWorkerGroupDetail> search(LoginRedisObj loginRedisObj, ReqWorkerGroupSearch param) {
        //1.构造查询条件
        QueryWrapper<WorkerGroup> wrapper = new QueryWrapper<>(
        WorkerGroup.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<WorkerGroup> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<WorkerGroup> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespWorkerGroupDetail> details = new ArrayList<>();
        for(WorkerGroup item:iPage.getRecords()){
            RespWorkerGroupDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerGroupDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespWorkerGroupDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespWorkerGroupDetail> findList(LoginRedisObj loginRedisObj, ReqWorkerGroupSearch req)throws Exception{
        WorkerGroup param = JSONObject.parseObject(JSONObject.toJSONString(req), WorkerGroup.class);
        List<WorkerGroup> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespWorkerGroupDetail> details = new ArrayList<>();
        for(WorkerGroup item:list){
            RespWorkerGroupDetail detail = DefindBeanUtil.copyProperties(item, RespWorkerGroupDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqWorkerGroupSave item) {
        WorkerGroup update = DefindBeanUtil.copyProperties(item, WorkerGroup.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqWorkerGroupSave add){
        WorkerGroup save = JSONObject.parseObject(JSONObject.toJSONString(add), WorkerGroup.class);
        mapper.insert(save);
    }

    @Override
    public RespWorkerGroupDetail detail(LoginRedisObj loginRedisObj, Long id){
        WorkerGroup byId = mapper.selectById(id);
        RespWorkerGroupDetail resp = transferOne(byId);
        return resp;
    }

    private RespWorkerGroupDetail transferOne(WorkerGroup item){
        RespWorkerGroupDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerGroupDetail.class);
        return resp;
    }
}
