package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqChiToKeySave;
import com.jd.branch.api.request.ReqChiToKeySearch;
import com.jd.branch.api.response.RespChiToKeyDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ChiToKey;
import com.jd.branch.mapper.ChiToKeyMapper;
import com.jd.branch.service.ChiToKeyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 中文对应键 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class ChiToKeyServiceImpl  implements IChiToKeyService {
    private static final Logger LOGGER = LoggerFactory.getLogger( ChiToKeyServiceImpl.class);
    @Autowired
    private ChiToKeyMapper mapper;


    @Override
    public PageInfo<RespChiToKeyDetail> search(LoginRedisObj loginRedisObj, ReqChiToKeySearch param) {
        //1.构造查询条件
        QueryWrapper<ChiToKey> wrapper = new QueryWrapper<>(
        ChiToKey.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<ChiToKey> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<ChiToKey> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespChiToKeyDetail> details = new ArrayList<>();
        for(ChiToKey item:iPage.getRecords()){
            RespChiToKeyDetail resp = DefindBeanUtil.copyProperties(item, RespChiToKeyDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespChiToKeyDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespChiToKeyDetail> findList(LoginRedisObj loginRedisObj, ReqChiToKeySearch req)throws Exception{
        ChiToKey param = JSONObject.parseObject(JSONObject.toJSONString(req), ChiToKey.class);
        List<ChiToKey> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespChiToKeyDetail> details = new ArrayList<>();
        for(ChiToKey item:list){
            RespChiToKeyDetail detail = DefindBeanUtil.copyProperties(item, RespChiToKeyDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqChiToKeySave item) {
        ChiToKey update = DefindBeanUtil.copyProperties(item, ChiToKey.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqChiToKeySave add){
        ChiToKey save = JSONObject.parseObject(JSONObject.toJSONString(add), ChiToKey.class);
        mapper.insert(save);
    }

    @Override
    public RespChiToKeyDetail detail(LoginRedisObj loginRedisObj, Long id){
        ChiToKey byId = mapper.selectById(id);
        RespChiToKeyDetail resp = transferOne(byId);
        return resp;
    }

    private RespChiToKeyDetail transferOne(ChiToKey item){
        RespChiToKeyDetail resp = DefindBeanUtil.copyProperties(item, RespChiToKeyDetail.class);
        return resp;
    }
}
