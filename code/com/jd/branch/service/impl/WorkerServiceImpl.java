package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqWorkerSave;
import com.jd.branch.api.request.ReqWorkerSearch;
import com.jd.branch.api.response.RespWorkerDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Worker;
import com.jd.branch.mapper.WorkerMapper;
import com.jd.branch.service.WorkerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 师傅信息 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class WorkerServiceImpl  implements IWorkerService {
    private static final Logger LOGGER = LoggerFactory.getLogger( WorkerServiceImpl.class);
    @Autowired
    private WorkerMapper mapper;


    @Override
    public PageInfo<RespWorkerDetail> search(LoginRedisObj loginRedisObj, ReqWorkerSearch param) {
        //1.构造查询条件
        QueryWrapper<Worker> wrapper = new QueryWrapper<>(
        Worker.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<Worker> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<Worker> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespWorkerDetail> details = new ArrayList<>();
        for(Worker item:iPage.getRecords()){
            RespWorkerDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespWorkerDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespWorkerDetail> findList(LoginRedisObj loginRedisObj, ReqWorkerSearch req)throws Exception{
        Worker param = JSONObject.parseObject(JSONObject.toJSONString(req), Worker.class);
        List<Worker> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespWorkerDetail> details = new ArrayList<>();
        for(Worker item:list){
            RespWorkerDetail detail = DefindBeanUtil.copyProperties(item, RespWorkerDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqWorkerSave item) {
        Worker update = DefindBeanUtil.copyProperties(item, Worker.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqWorkerSave add){
        Worker save = JSONObject.parseObject(JSONObject.toJSONString(add), Worker.class);
        mapper.insert(save);
    }

    @Override
    public RespWorkerDetail detail(LoginRedisObj loginRedisObj, Long id){
        Worker byId = mapper.selectById(id);
        RespWorkerDetail resp = transferOne(byId);
        return resp;
    }

    private RespWorkerDetail transferOne(Worker item){
        RespWorkerDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerDetail.class);
        return resp;
    }
}
