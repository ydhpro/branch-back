package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqAppointModelSave;
import com.jd.branch.api.request.ReqAppointModelSearch;
import com.jd.branch.api.response.RespAppointModelDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.AppointModel;
import com.jd.branch.mapper.AppointModelMapper;
import com.jd.branch.service.AppointModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 预约信息模板 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class AppointModelServiceImpl  implements IAppointModelService {
    private static final Logger LOGGER = LoggerFactory.getLogger( AppointModelServiceImpl.class);
    @Autowired
    private AppointModelMapper mapper;


    @Override
    public PageInfo<RespAppointModelDetail> search(LoginRedisObj loginRedisObj, ReqAppointModelSearch param) {
        //1.构造查询条件
        QueryWrapper<AppointModel> wrapper = new QueryWrapper<>(
        AppointModel.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<AppointModel> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<AppointModel> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespAppointModelDetail> details = new ArrayList<>();
        for(AppointModel item:iPage.getRecords()){
            RespAppointModelDetail resp = DefindBeanUtil.copyProperties(item, RespAppointModelDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespAppointModelDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespAppointModelDetail> findList(LoginRedisObj loginRedisObj, ReqAppointModelSearch req)throws Exception{
        AppointModel param = JSONObject.parseObject(JSONObject.toJSONString(req), AppointModel.class);
        List<AppointModel> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespAppointModelDetail> details = new ArrayList<>();
        for(AppointModel item:list){
            RespAppointModelDetail detail = DefindBeanUtil.copyProperties(item, RespAppointModelDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqAppointModelSave item) {
        AppointModel update = DefindBeanUtil.copyProperties(item, AppointModel.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqAppointModelSave add){
        AppointModel save = JSONObject.parseObject(JSONObject.toJSONString(add), AppointModel.class);
        mapper.insert(save);
    }

    @Override
    public RespAppointModelDetail detail(LoginRedisObj loginRedisObj, Long id){
        AppointModel byId = mapper.selectById(id);
        RespAppointModelDetail resp = transferOne(byId);
        return resp;
    }

    private RespAppointModelDetail transferOne(AppointModel item){
        RespAppointModelDetail resp = DefindBeanUtil.copyProperties(item, RespAppointModelDetail.class);
        return resp;
    }
}
