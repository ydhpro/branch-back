package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqInventoryStreamSave;
import com.jd.branch.api.request.ReqInventoryStreamSearch;
import com.jd.branch.api.response.RespInventoryStreamDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.InventoryStream;
import com.jd.branch.mapper.InventoryStreamMapper;
import com.jd.branch.service.InventoryStreamService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 库存流水 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class InventoryStreamServiceImpl  implements IInventoryStreamService {
    private static final Logger LOGGER = LoggerFactory.getLogger( InventoryStreamServiceImpl.class);
    @Autowired
    private InventoryStreamMapper mapper;


    @Override
    public PageInfo<RespInventoryStreamDetail> search(LoginRedisObj loginRedisObj, ReqInventoryStreamSearch param) {
        //1.构造查询条件
        QueryWrapper<InventoryStream> wrapper = new QueryWrapper<>(
        InventoryStream.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<InventoryStream> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<InventoryStream> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespInventoryStreamDetail> details = new ArrayList<>();
        for(InventoryStream item:iPage.getRecords()){
            RespInventoryStreamDetail resp = DefindBeanUtil.copyProperties(item, RespInventoryStreamDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespInventoryStreamDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespInventoryStreamDetail> findList(LoginRedisObj loginRedisObj, ReqInventoryStreamSearch req)throws Exception{
        InventoryStream param = JSONObject.parseObject(JSONObject.toJSONString(req), InventoryStream.class);
        List<InventoryStream> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespInventoryStreamDetail> details = new ArrayList<>();
        for(InventoryStream item:list){
            RespInventoryStreamDetail detail = DefindBeanUtil.copyProperties(item, RespInventoryStreamDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqInventoryStreamSave item) {
        InventoryStream update = DefindBeanUtil.copyProperties(item, InventoryStream.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqInventoryStreamSave add){
        InventoryStream save = JSONObject.parseObject(JSONObject.toJSONString(add), InventoryStream.class);
        mapper.insert(save);
    }

    @Override
    public RespInventoryStreamDetail detail(LoginRedisObj loginRedisObj, Long id){
        InventoryStream byId = mapper.selectById(id);
        RespInventoryStreamDetail resp = transferOne(byId);
        return resp;
    }

    private RespInventoryStreamDetail transferOne(InventoryStream item){
        RespInventoryStreamDetail resp = DefindBeanUtil.copyProperties(item, RespInventoryStreamDetail.class);
        return resp;
    }
}
