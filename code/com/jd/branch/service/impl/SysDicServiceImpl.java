package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqSysDicSave;
import com.jd.branch.api.request.ReqSysDicSearch;
import com.jd.branch.api.response.RespSysDicDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.SysDic;
import com.jd.branch.mapper.SysDicMapper;
import com.jd.branch.service.SysDicService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统字典表 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class SysDicServiceImpl  implements ISysDicService {
    private static final Logger LOGGER = LoggerFactory.getLogger( SysDicServiceImpl.class);
    @Autowired
    private SysDicMapper mapper;


    @Override
    public PageInfo<RespSysDicDetail> search(LoginRedisObj loginRedisObj, ReqSysDicSearch param) {
        //1.构造查询条件
        QueryWrapper<SysDic> wrapper = new QueryWrapper<>(
        SysDic.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<SysDic> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<SysDic> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespSysDicDetail> details = new ArrayList<>();
        for(SysDic item:iPage.getRecords()){
            RespSysDicDetail resp = DefindBeanUtil.copyProperties(item, RespSysDicDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespSysDicDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespSysDicDetail> findList(LoginRedisObj loginRedisObj, ReqSysDicSearch req)throws Exception{
        SysDic param = JSONObject.parseObject(JSONObject.toJSONString(req), SysDic.class);
        List<SysDic> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespSysDicDetail> details = new ArrayList<>();
        for(SysDic item:list){
            RespSysDicDetail detail = DefindBeanUtil.copyProperties(item, RespSysDicDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqSysDicSave item) {
        SysDic update = DefindBeanUtil.copyProperties(item, SysDic.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqSysDicSave add){
        SysDic save = JSONObject.parseObject(JSONObject.toJSONString(add), SysDic.class);
        mapper.insert(save);
    }

    @Override
    public RespSysDicDetail detail(LoginRedisObj loginRedisObj, Long id){
        SysDic byId = mapper.selectById(id);
        RespSysDicDetail resp = transferOne(byId);
        return resp;
    }

    private RespSysDicDetail transferOne(SysDic item){
        RespSysDicDetail resp = DefindBeanUtil.copyProperties(item, RespSysDicDetail.class);
        return resp;
    }
}
