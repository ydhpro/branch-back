package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ProLedger;
import com.jd.branch.api.request.ReqProLedgerSave;
import com.jd.branch.api.request.ReqProLedgerSearch;
import com.jd.branch.api.response.RespProLedgerDetail;
import java.util.List;


/**
 * <p>
 * 物业台账 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
public interface ProLedgerService {
    PageInfo<RespProLedgerDetail> search(LoginRedisObj loginRedisObj,ReqProLedgerSearch reqProLedgerSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqProLedgerSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqProLedgerSave save) throws Exception;

    RespProLedgerDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespProLedgerDetail> findList(LoginRedisObj loginRedisObj, ReqProLedgerSearch item) throws Exception;
}