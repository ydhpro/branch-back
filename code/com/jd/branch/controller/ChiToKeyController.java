package com.jd.branch.controller;


import com.jd.branch.api.request.ReqChiToKeySave;
import com.jd.branch.api.request.ReqChiToKeySearch;
import com.jd.branch.api.response.RespChiToKeyDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.ChiToKey;
import com.jd.branch.service.ChiToKeyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 中文对应键 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class ChiToKeyController extends BaseController {
    @Autowired
    private IChiToKeyService service;

    @PostMapping("/api/v1/ChiToKey")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqChiToKeySave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/ChiToKey")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/ChiToKey/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespChiToKeyDetail>> findList(HttpServletRequest request,@RequestBody ReqChiToKeySearch item)throws Exception {
        List<RespChiToKeyDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/ChiToKey")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqChiToKeySave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/ChiToKey/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespChiToKeyDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespChiToKeyDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/ChiToKey")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespChiToKeyDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqChiToKeySearch reqChiToKeySearch) throws Exception {

        PageInfo<RespChiToKeyDetail> search = service.search(this.getLoginObj(request),reqChiToKeySearch);
        return new Message<>(search);

    }
}
