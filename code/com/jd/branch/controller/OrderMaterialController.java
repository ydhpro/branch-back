package com.jd.branch.controller;


import com.jd.branch.api.request.ReqOrderMaterialSave;
import com.jd.branch.api.request.ReqOrderMaterialSearch;
import com.jd.branch.api.response.RespOrderMaterialDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.OrderMaterial;
import com.jd.branch.service.OrderMaterialService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class OrderMaterialController extends BaseController {
    @Autowired
    private IOrderMaterialService service;

    @PostMapping("/api/v1/OrderMaterial")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqOrderMaterialSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/OrderMaterial")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/OrderMaterial/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespOrderMaterialDetail>> findList(HttpServletRequest request,@RequestBody ReqOrderMaterialSearch item)throws Exception {
        List<RespOrderMaterialDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/OrderMaterial")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqOrderMaterialSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/OrderMaterial/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespOrderMaterialDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespOrderMaterialDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/OrderMaterial")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespOrderMaterialDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqOrderMaterialSearch reqOrderMaterialSearch) throws Exception {

        PageInfo<RespOrderMaterialDetail> search = service.search(this.getLoginObj(request),reqOrderMaterialSearch);
        return new Message<>(search);

    }
}
