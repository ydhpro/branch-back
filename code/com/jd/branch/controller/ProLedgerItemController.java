package com.jd.branch.controller;


import com.jd.branch.api.request.ReqProLedgerItemSave;
import com.jd.branch.api.request.ReqProLedgerItemSearch;
import com.jd.branch.api.response.RespProLedgerItemDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.ProLedgerItem;
import com.jd.branch.service.ProLedgerItemService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class ProLedgerItemController extends BaseController {
    @Autowired
    private IProLedgerItemService service;

    @PostMapping("/api/v1/ProLedgerItem")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqProLedgerItemSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/ProLedgerItem")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/ProLedgerItem/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespProLedgerItemDetail>> findList(HttpServletRequest request,@RequestBody ReqProLedgerItemSearch item)throws Exception {
        List<RespProLedgerItemDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/ProLedgerItem")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqProLedgerItemSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/ProLedgerItem/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespProLedgerItemDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespProLedgerItemDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/ProLedgerItem")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespProLedgerItemDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqProLedgerItemSearch reqProLedgerItemSearch) throws Exception {

        PageInfo<RespProLedgerItemDetail> search = service.search(this.getLoginObj(request),reqProLedgerItemSearch);
        return new Message<>(search);

    }
}
