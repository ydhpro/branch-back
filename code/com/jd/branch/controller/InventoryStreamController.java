package com.jd.branch.controller;


import com.jd.branch.api.request.ReqInventoryStreamSave;
import com.jd.branch.api.request.ReqInventoryStreamSearch;
import com.jd.branch.api.response.RespInventoryStreamDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.InventoryStream;
import com.jd.branch.service.InventoryStreamService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 库存流水 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class InventoryStreamController extends BaseController {
    @Autowired
    private IInventoryStreamService service;

    @PostMapping("/api/v1/InventoryStream")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqInventoryStreamSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/InventoryStream")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/InventoryStream/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespInventoryStreamDetail>> findList(HttpServletRequest request,@RequestBody ReqInventoryStreamSearch item)throws Exception {
        List<RespInventoryStreamDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/InventoryStream")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqInventoryStreamSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/InventoryStream/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespInventoryStreamDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespInventoryStreamDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/InventoryStream")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespInventoryStreamDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqInventoryStreamSearch reqInventoryStreamSearch) throws Exception {

        PageInfo<RespInventoryStreamDetail> search = service.search(this.getLoginObj(request),reqInventoryStreamSearch);
        return new Message<>(search);

    }
}
