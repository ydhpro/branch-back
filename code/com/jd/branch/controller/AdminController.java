package com.jd.branch.controller;


import com.jd.branch.api.request.ReqAdminSave;
import com.jd.branch.api.request.ReqAdminSearch;
import com.jd.branch.api.response.RespAdminDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.Admin;
import com.jd.branch.service.AdminService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 管理员 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class AdminController extends BaseController {
    @Autowired
    private IAdminService service;

    @PostMapping("/api/v1/Admin")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqAdminSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/Admin")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/Admin/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespAdminDetail>> findList(HttpServletRequest request,@RequestBody ReqAdminSearch item)throws Exception {
        List<RespAdminDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/Admin")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqAdminSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/Admin/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespAdminDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespAdminDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/Admin")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespAdminDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqAdminSearch reqAdminSearch) throws Exception {

        PageInfo<RespAdminDetail> search = service.search(this.getLoginObj(request),reqAdminSearch);
        return new Message<>(search);

    }
}
