package com.jd.branch.controller;


import com.jd.branch.api.request.ReqAddCategorySave;
import com.jd.branch.api.request.ReqAddCategorySearch;
import com.jd.branch.api.response.RespAddCategoryDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.AddCategory;
import com.jd.branch.service.AddCategoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 增项类目 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class AddCategoryController extends BaseController {
    @Autowired
    private IAddCategoryService service;

    @PostMapping("/api/v1/AddCategory")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqAddCategorySave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/AddCategory")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/AddCategory/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespAddCategoryDetail>> findList(HttpServletRequest request,@RequestBody ReqAddCategorySearch item)throws Exception {
        List<RespAddCategoryDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/AddCategory")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqAddCategorySave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/AddCategory/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespAddCategoryDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespAddCategoryDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/AddCategory")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespAddCategoryDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqAddCategorySearch reqAddCategorySearch) throws Exception {

        PageInfo<RespAddCategoryDetail> search = service.search(this.getLoginObj(request),reqAddCategorySearch);
        return new Message<>(search);

    }
}
