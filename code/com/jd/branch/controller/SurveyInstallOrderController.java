package com.jd.branch.controller;


import com.jd.branch.api.request.ReqSurveyInstallOrderSave;
import com.jd.branch.api.request.ReqSurveyInstallOrderSearch;
import com.jd.branch.api.response.RespSurveyInstallOrderDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.SurveyInstallOrder;
import com.jd.branch.service.SurveyInstallOrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 勘安工单 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class SurveyInstallOrderController extends BaseController {
    @Autowired
    private ISurveyInstallOrderService service;

    @PostMapping("/api/v1/SurveyInstallOrder")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/SurveyInstallOrder")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/SurveyInstallOrder/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespSurveyInstallOrderDetail>> findList(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSearch item)throws Exception {
        List<RespSurveyInstallOrderDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/SurveyInstallOrder")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/SurveyInstallOrder/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespSurveyInstallOrderDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespSurveyInstallOrderDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/SurveyInstallOrder")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespSurveyInstallOrderDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqSurveyInstallOrderSearch reqSurveyInstallOrderSearch) throws Exception {

        PageInfo<RespSurveyInstallOrderDetail> search = service.search(this.getLoginObj(request),reqSurveyInstallOrderSearch);
        return new Message<>(search);

    }
}
