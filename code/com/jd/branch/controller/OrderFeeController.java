package com.jd.branch.controller;


import com.jd.branch.api.request.ReqOrderFeeSave;
import com.jd.branch.api.request.ReqOrderFeeSearch;
import com.jd.branch.api.response.RespOrderFeeDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.OrderFee;
import com.jd.branch.service.OrderFeeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 单据费用 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class OrderFeeController extends BaseController {
    @Autowired
    private IOrderFeeService service;

    @PostMapping("/api/v1/OrderFee")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqOrderFeeSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/OrderFee")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/OrderFee/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespOrderFeeDetail>> findList(HttpServletRequest request,@RequestBody ReqOrderFeeSearch item)throws Exception {
        List<RespOrderFeeDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/OrderFee")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqOrderFeeSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/OrderFee/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespOrderFeeDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespOrderFeeDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/OrderFee")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespOrderFeeDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqOrderFeeSearch reqOrderFeeSearch) throws Exception {

        PageInfo<RespOrderFeeDetail> search = service.search(this.getLoginObj(request),reqOrderFeeSearch);
        return new Message<>(search);

    }
}
