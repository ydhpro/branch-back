package com.jd.branch.controller;


import com.jd.branch.api.request.ReqInventorySave;
import com.jd.branch.api.request.ReqInventorySearch;
import com.jd.branch.api.response.RespInventoryDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.Inventory;
import com.jd.branch.service.InventoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 库存 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class InventoryController extends BaseController {
    @Autowired
    private IInventoryService service;

    @PostMapping("/api/v1/Inventory")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqInventorySave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/Inventory")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/Inventory/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespInventoryDetail>> findList(HttpServletRequest request,@RequestBody ReqInventorySearch item)throws Exception {
        List<RespInventoryDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/Inventory")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqInventorySave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/Inventory/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespInventoryDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespInventoryDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/Inventory")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespInventoryDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqInventorySearch reqInventorySearch) throws Exception {

        PageInfo<RespInventoryDetail> search = service.search(this.getLoginObj(request),reqInventorySearch);
        return new Message<>(search);

    }
}
