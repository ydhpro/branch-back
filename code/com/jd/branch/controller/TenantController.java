package com.jd.branch.controller;


import com.jd.branch.api.request.ReqTenantSave;
import com.jd.branch.api.request.ReqTenantSearch;
import com.jd.branch.api.response.RespTenantDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.Tenant;
import com.jd.branch.service.TenantService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 租户 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class TenantController extends BaseController {
    @Autowired
    private ITenantService service;

    @PostMapping("/api/v1/Tenant")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqTenantSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/Tenant")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/Tenant/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespTenantDetail>> findList(HttpServletRequest request,@RequestBody ReqTenantSearch item)throws Exception {
        List<RespTenantDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/Tenant")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqTenantSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/Tenant/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespTenantDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespTenantDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/Tenant")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespTenantDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqTenantSearch reqTenantSearch) throws Exception {

        PageInfo<RespTenantDetail> search = service.search(this.getLoginObj(request),reqTenantSearch);
        return new Message<>(search);

    }
}
