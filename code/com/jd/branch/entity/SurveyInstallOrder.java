package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 勘安工单
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SurveyInstallOrder implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 客户姓名
     */
     @PropertyName(name = "客户姓名")
    private String customerName;

    

    /**
     * 客户手机号码
     */
     @PropertyName(name = "客户手机号码")
    private String customerPhone;

    

    /**
     * 客户外源id
     */
     @PropertyName(name = "客户外源id")
    private String customerOutId;

    

    private String installProvince;

    

    private String installCity;

    

    private String installArea;

    

    private String installAddr;

    

    /**
     * 门牌车位号
     */
     @PropertyName(name = "门牌车位号")
    private String installCarPosiNum;

    

    /**
     * 小区
     */
     @PropertyName(name = "小区")
    private String liveArea;

    

    /**
     * 物业公司
     */
     @PropertyName(name = "物业公司")
    private String proCompany;

    

    /**
     * 安装订单号
     */
     @PropertyName(name = "安装订单号")
    private String installOrderNum;

    

    /**
     * 安装工单号
     */
     @PropertyName(name = "安装工单号")
    private String installServiceNum;

    

    /**
     * 车型：理想one、理想L系列、理想MEGA、极石
     */
     @PropertyName(name = "车型：理想one、理想L系列、理想MEGA、极石")
    private String carType;

    

    /**
     * 充电桩类型：7kw，21kw
     */
     @PropertyName(name = "充电桩类型：7kw，21kw")
    private String chargeType;

    

    /**
     * 服务类型：客户权益，商城付费
     */
     @PropertyName(name = "服务类型：客户权益，商城付费")
    private String serviceType;

    

    /**
     * 工单来源：App
     */
     @PropertyName(name = "工单来源：App")
    private String orderSource;

    

    /**
     * 平台派送至网点时间（客服输入时间）
     */
     @PropertyName(name = "平台派送至网点时间（客服输入时间）")
    private LocalDateTime createTime;

    

    /**
     * 订单创建时间
     */
     @PropertyName(name = "订单创建时间")
    private LocalDateTime orderCreateTime;

    

    /**
     * 待勘测时间（师傅端的勘测接单时间）
     */
     @PropertyName(name = "待勘测时间（师傅端的勘测接单时间）")
    private LocalDateTime waitSurveyTime;

    

    /**
     * 待安装时间（师傅端的安装接单时间）
     */
     @PropertyName(name = "待安装时间（师傅端的安装接单时间）")
    private LocalDateTime waitInstallTime;

    

    private Long surveyWorkerGroupId;

    

    private String surveyWorkerGroupName;

    

    private Long surveyWorker1Id;

    

    private Long surveyWorker2Id;

    

    private String surveyWorker1Name;

    

    private String surveyWorker2Name;

    

    /**
     * 预计勘测开始时间
     */
     @PropertyName(name = "预计勘测开始时间")
    private LocalDateTime surveyPlanStartTime;

    

    /**
     * 预计勘测结束时间
     */
     @PropertyName(name = "预计勘测结束时间")
    private LocalDateTime surveyPlanEndTime;

    

    /**
     * 实际勘测开始时间
     */
     @PropertyName(name = "实际勘测开始时间")
    private LocalDateTime surverActStartTime;

    

    /**
     * 实际勘测结束时间
     */
     @PropertyName(name = "实际勘测结束时间")
    private LocalDateTime surverActEndTime;

    

    /**
     * 预估米数
     */
     @PropertyName(name = "预估米数")
    private BigDecimal meters;

    

    /**
     * 安装方式
     */
     @PropertyName(name = "安装方式")
    private String installWay;

    

    /**
     * 管材类型
     */
     @PropertyName(name = "管材类型")
    private String pipeType;

    

    /**
     * 是否有电源点
     */
     @PropertyName(name = "是否有电源点")
    private Integer isPowerPoint;

    

    /**
     * 电表参数
     */
     @PropertyName(name = "电表参数")
    private String powerPointParam;

    

    /**
     * 电表位置
     */
     @PropertyName(name = "电表位置")
    private String eleTablePosi;

    

    /**
     * 预估电表位置
     */
     @PropertyName(name = "预估电表位置")
    private String estimateEleTablePosi;

    

    /**
     * 其他特殊备注
     */
     @PropertyName(name = "其他特殊备注")
    private String otherRemark;

    

    /**
     * 工单备注
     */
     @PropertyName(name = "工单备注")
    private String orderRemark;

    

    /**
     * 文字信息审核结果
     */
     @PropertyName(name = "文字信息审核结果")
    private String textAuditRes;

    

    /**
     * 勘测审核结果
     */
     @PropertyName(name = "勘测审核结果")
    private Integer surveyAuditResult;

    

    private Long installWorkerGroupId;

    

    private String installWorkerGroupName;

    

    private Long installWorker1Id;

    

    private Long installWorker2Id;

    

    private String installWorker1Name;

    

    private String installWorker2Name;

    

    /**
     * 预计安装开始时间
     */
     @PropertyName(name = "预计安装开始时间")
    private LocalDateTime installPlanStartTime;

    

    /**
     * 预计安装结束时间
     */
     @PropertyName(name = "预计安装结束时间")
    private LocalDateTime installPlanEndTime;

    

    /**
     * 实际安装开始时间
     */
     @PropertyName(name = "实际安装开始时间")
    private LocalDateTime installActStartTime;

    

    /**
     * 实际安装结束时间
     */
     @PropertyName(name = "实际安装结束时间")
    private LocalDateTime installActEndTime;

    

    private Long tenantId;

    

    /**
     * 状态：1勘测待派单 2勘测时间确认 3待勘测，4勘测待审核，5勘测驳回，6安装待派单，7安装时间确认  8待安装，9安装中，10已安装，11已结单，12已结算
     */
     @PropertyName(name = "状态：1勘测待派单 2勘测时间确认 3待勘测，4勘测待审核，5勘测驳回，6安装待派单，7安装时间确认  8待安装，9安装中，10已安装，11已结单，12已结算")
    private Integer state;

    

    /**
     * 品牌id
     */
     @PropertyName(name = "品牌id")
    private Long brandId;

    

    /**
     * 品牌名称
     */
     @PropertyName(name = "品牌名称")
    private String brandName;

    

    /**
     * 归属销售区域
     */
     @PropertyName(name = "归属销售区域")
    private String belongArea;

    

    /**
     * 其他收入
     */
     @PropertyName(name = "其他收入")
    private BigDecimal otherIncome;

    

    /**
     * 其他支出
     */
     @PropertyName(name = "其他支出")
    private BigDecimal otherExpend;

    

    /**
     * 师傅通勤公里数
     */
     @PropertyName(name = "师傅通勤公里数")
    private BigDecimal workerCommuteNum;

    

    /**
     * 单据创建日期
     */
     @PropertyName(name = "单据创建日期")
    private LocalDate createDate;

    

    private Integer deleteFlag;

    


}
