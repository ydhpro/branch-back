package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 师傅组品牌维护
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkerGroupBrand implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 合作品牌名称
     */
     @PropertyName(name = "合作品牌名称")
    private String brandName;

    

    /**
     * 合作品牌id
     */
     @PropertyName(name = "合作品牌id")
    private Long brandId;

    

    /**
     * 师傅组id
     */
     @PropertyName(name = "师傅组id")
    private Long workerGroupId;

    

    /**
     * 半包师傅安装结算价
     */
     @PropertyName(name = "半包师傅安装结算价")
    private BigDecimal halfPackInstallSettlePrice;

    

    /**
     * 全包师傅安装结算价
     */
     @PropertyName(name = "全包师傅安装结算价")
    private BigDecimal allPackInstallSettlePrice;

    

    /**
     * 半包师傅勘测结算价
     */
     @PropertyName(name = "半包师傅勘测结算价")
    private BigDecimal halfPackSurveySettlePrice;

    

    /**
     * 全包师傅勘测结算价
     */
     @PropertyName(name = "全包师傅勘测结算价")
    private BigDecimal allPackSurveySettlePrice;

    

    /**
     * 全包额外抽取比例
     */
     @PropertyName(name = "全包额外抽取比例")
    private BigDecimal allPackAddExtractRate;

    

    /**
     * 套包外每米提成
     */
     @PropertyName(name = "套包外每米提成")
    private BigDecimal packOutMeterCommission;

    

    /**
     * 路程补贴
     */
     @PropertyName(name = "路程补贴")
    private BigDecimal distanceSubsidy;

    

    private Long tenantId;

    

    private Integer deleteFlag;

    


}
