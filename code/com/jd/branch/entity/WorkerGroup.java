package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 师傅组
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkerGroup implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 默认将两个师傅名称拼起来
     */
     @PropertyName(name = "默认将两个师傅名称拼起来")
    private String name;

    

    private Long worker1Id;

    

    private String worker1Name;

    

    private Long worker2Id;

    

    private String worker2Name;

    

    /**
     * 大区
     */
     @PropertyName(name = "大区")
    private String area;

    

    /**
     * 合作方式：1全包，2半包
     */
     @PropertyName(name = "合作方式：1全包，2半包")
    private Integer comType;

    

    private Long tenantId;

    

    private Integer deleteFlag;

    


}
