package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 库存
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Inventory implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 库存类型：1主库，2师傅，3小网点
     */
     @PropertyName(name = "库存类型：1主库，2师傅，3小网点")
    private Integer type;

    

    /**
     * 存储对象id（师傅id/小网点id，如果是主库则为0）
     */
     @PropertyName(name = "存储对象id（师傅id/小网点id，如果是主库则为0）")
    private Long entityId;

    

    /**
     * 存储对象名称：师傅名称，网点名称
     */
     @PropertyName(name = "存储对象名称：师傅名称，网点名称")
    private String entityName;

    

    /**
     * 物料名称
     */
     @PropertyName(name = "物料名称")
    private String materialName;

    

    /**
     * 当前库存数
     */
     @PropertyName(name = "当前库存数")
    private BigDecimal num;

    

    /**
     * 物料编码
     */
     @PropertyName(name = "物料编码")
    private String materialCode;

    

    /**
     * 锁定库存数
     */
     @PropertyName(name = "锁定库存数")
    private BigDecimal lockNum;

    

    /**
     * 首次入库时间
     */
     @PropertyName(name = "首次入库时间")
    private LocalDateTime createTime;

    

    /**
     * 最近一次操作时间
     */
     @PropertyName(name = "最近一次操作时间")
    private LocalDateTime latestTime;

    

    /**
     * 单位（线缆单位为：米）
     */
     @PropertyName(name = "单位（线缆单位为：米）")
    private String unit;

    

    /**
     * 型号
     */
     @PropertyName(name = "型号")
    private String model;

    

    /**
     * 购入价格（成本）
     */
     @PropertyName(name = "购入价格（成本）")
    private BigDecimal price;

    

    /**
     * 销售价（销售给师傅）
     */
     @PropertyName(name = "销售价（销售给师傅）")
    private BigDecimal salePrice;

    

    /**
     * 批次号：格式年月日时分秒
     */
     @PropertyName(name = "批次号：格式年月日时分秒")
    private Long batchNum;

    

    /**
     * 是否电缆：0否1是
     */
     @PropertyName(name = "是否电缆：0否1是")
    private Integer isCable;

    

    /**
     * 来源id
     */
     @PropertyName(name = "来源id")
    private Long sourceId;

    

    /**
     * 每捆米数
     */
     @PropertyName(name = "每捆米数")
    private BigDecimal meterNum;

    

    private Long tenantId;

    

    private Integer deleteFlag;

    


}
