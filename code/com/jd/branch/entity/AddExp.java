package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 增项费用
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddExp implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 收费类别
     */
     @PropertyName(name = "收费类别")
    private String name;

    

    /**
     * 输入类型（1输入框，2下拉：密码锁，机械锁）
     */
     @PropertyName(name = "输入类型（1输入框，2下拉：密码锁，机械锁）")
    private Integer type;

    

    /**
     * 单价
     */
     @PropertyName(name = "单价")
    private BigDecimal price;

    

    /**
     * 总价
     */
     @PropertyName(name = "总价")
    private BigDecimal total;

    

    /**
     * 数量
     */
     @PropertyName(name = "数量")
    private BigDecimal num;

    

    /**
     * 关联单据号
     */
     @PropertyName(name = "关联单据号")
    private Long orderId;

    

    /**
     * 师傅结算价
     */
     @PropertyName(name = "师傅结算价")
    private BigDecimal settlePrice;

    

    private Long tenantId;

    

    private Integer deleteFlag;

    

    /**
     * 增项类型：1服务，2物料
     */
     @PropertyName(name = "增项类型：1服务，2物料")
    private Integer addType;

    


}
