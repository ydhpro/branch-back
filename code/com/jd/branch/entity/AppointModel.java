package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 预约信息模板
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppointModel implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 适用渠道
     */
     @PropertyName(name = "适用渠道")
    private String source;

    

    private String modelText;

    

    /**
     * 租户id
     */
     @PropertyName(name = "租户id")
    private Long tenantId;

    

    private Integer deleteFlag;

    


}
