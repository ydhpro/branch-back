package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 图片附件
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PicAttachment implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    private String picUrl;

    

    /**
     * 通过表关联
     */
     @PropertyName(name = "通过表关联")
    private String type;

    

    /**
     * 关联主题id
     */
     @PropertyName(name = "关联主题id")
    private Long relateId;

    

    private Long tenantId;

    

    /**
     * 评论
     */
     @PropertyName(name = "评论")
    private String comment;

    

    /**
     * 是否审核通过(0否1是）
     */
     @PropertyName(name = "是否审核通过(0否1是）")
    private Integer isAudited;

    

    private Integer deleteFlag;

    


}
