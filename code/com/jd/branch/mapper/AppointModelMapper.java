package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.AppointModel;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * 预约信息模板 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
 @Mapper
public interface AppointModelMapper extends BaseMapper<AppointModel> {

}
