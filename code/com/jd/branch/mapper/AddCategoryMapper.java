package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.AddCategory;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * 增项类目 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
 @Mapper
public interface AddCategoryMapper extends BaseMapper<AddCategory> {

}
