package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.WorkerGroupBrand;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * 师傅组品牌维护 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
 @Mapper
public interface WorkerGroupBrandMapper extends BaseMapper<WorkerGroupBrand> {

}
