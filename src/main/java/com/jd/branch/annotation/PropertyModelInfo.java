package com.jd.branch.annotation;

import lombok.Data;

/**
 * @author ydh
 * @version 1.0
 * @date 2021/4/23 1:54
 * 利用反射机制拿到的字段属性信息
 */
@Data
public class PropertyModelInfo {
    /**
     * 属性名称
     */
    private String propertyName;
    /**
     * 属性注释
     */
    private String propertyComment;
    /**
     * 属性值
     */
    private Object value;
    /**
     * 返回值类型
     */
    private Class<?> returnType;

}
