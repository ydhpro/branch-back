package com.jd.branch.annotation;

import lombok.Data;

/**
 * @author ydh
 * @version 1.0
 * @date 2021/4/23 1:54
 * 用于记录修改后发生变化的字段属性信息
 */
@Data
public class ModifiedPropertyInfo {
    /**
     * 发生变化的属性名称
     */
    private String propertyName;
    /**
     * 发生变化的属性注释
     */
    private String propertyComment;
    /**
     * 修改前的值
     */
    private Object oldValue;
    /**
     * 修改后的值
     */
    private Object newValue;

}
