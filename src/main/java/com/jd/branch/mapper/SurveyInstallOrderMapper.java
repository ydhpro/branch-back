package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.api.response.RespBusinessSitDetail;
import com.jd.branch.entity.SurveyInstallOrder;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 勘安工单 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Mapper
public interface SurveyInstallOrderMapper extends BaseMapper<SurveyInstallOrder> {
    List<RespBusinessSitDetail> selectGroupByDate(Date startDate, Date endDate);
}
