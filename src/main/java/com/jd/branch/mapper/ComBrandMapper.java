package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.ComBrand;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * 合作品牌相关参数 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-08-26
 */
 @Mapper
public interface ComBrandMapper extends BaseMapper<ComBrand> {

}
