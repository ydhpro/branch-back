package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.WorkerGroup;
import com.jd.branch.entity.WorkerGroupBrand;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 师傅组 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Mapper
public interface WorkerGroupBrandMapper extends BaseMapper<WorkerGroupBrand> {

}
