package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.Inventory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 库存 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Mapper
public interface InventoryMapper extends BaseMapper<Inventory> {

}
