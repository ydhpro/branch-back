package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.ChiToKey;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * 中文对应键 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-06-03
 */
 @Mapper
public interface ChiToKeyMapper extends BaseMapper<ChiToKey> {

}
