package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.PicAttachment;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 图片附件 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Mapper
public interface PicAttachmentMapper extends BaseMapper<PicAttachment> {

}
