package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.AddExp;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * 增项费用 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-04-08
 */
 @Mapper
public interface AddExpMapper extends BaseMapper<AddExp> {

}
