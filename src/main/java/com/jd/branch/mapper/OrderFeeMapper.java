package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.OrderFee;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * 单据费用 Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-08-30
 */
 @Mapper
public interface OrderFeeMapper extends BaseMapper<OrderFee> {

}
