package com.jd.branch.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.OrderMaterial;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ydh
 * @since 2024-04-29
 */
 @Mapper
public interface OrderMaterialMapper extends BaseMapper<OrderMaterial> {

}
