package com.jd.branch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;


import java.util.Locale;

/**
 * @author luoqy4
 */

@EnableFeignClients
@EnableDiscoveryClient
@EnableTransactionManagement
@Import(cn.hutool.extra.spring.SpringUtil.class)
@ComponentScan("com.jd")
@SpringBootApplication
public class JdBranchApplication {


	/**
	 * 设置默认语言
	 * @return
	 */
	@Bean
	public SessionLocaleResolver localeResolver() {
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(Locale.CHINA);
		return localeResolver;
	}

	public static void main(String[] args) {
		SpringApplication.run(JdBranchApplication.class, args);
	}

}
