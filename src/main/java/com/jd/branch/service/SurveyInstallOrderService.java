package com.jd.branch.service;


import com.alibaba.fastjson.JSONObject;
import com.jd.branch.api.request.*;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.OrderFee;
import com.jd.branch.entity.SurveyInstallOrder;
import com.jd.branch.api.response.RespSurveyInstallOrderDetail;
import java.util.List;


/**
 * <p>
 * 勘安工单 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
public interface SurveyInstallOrderService {
    PageInfo<RespSurveyInstallOrderDetail> search(LoginRedisObj loginRedisObj,ReqSurveyInstallOrderSearch reqSurveyInstallOrderSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave save) throws Exception;

    RespSurveyInstallOrderDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespSurveyInstallOrderDetail> findList(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch item) throws Exception;

    void surDispatch(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave item) throws Exception;

    void submitSurTime(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave item) throws Exception;

    void submitInstallTime(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave item) throws Exception;

    PageInfo<RespSurveyInstallOrderDetail> workSearch(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch reqSurveyInstallOrderSearch) throws Exception;

    void auditSurvey(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderAuditSurvey reqSurveyInstallOrderAuditSurvey) throws Exception;

    void finishInstall(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderAuditSurvey reqSurveyInstallOrderAuditSurvey) throws Exception;

    void saveMaterialList(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSaveMaterialList item) throws Exception;

    void end(LoginRedisObj loginRedisObj, ReqCommDel reqCommDel) throws Exception;

    JSONObject explain(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderExplain req) throws Exception;

    void changeState(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave reqSurveyInstallOrderSave) throws Exception;

    OrderFee getCalFormaleAndResult(Long id) throws Exception;
}