package com.jd.branch.service;


import com.jd.branch.api.request.ChiToKeyApi;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ChiToKey;

import java.util.List;


/**
 * <p>
 * 中文对应键 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-06-03
 */
public interface ChiToKeyService {
    PageInfo<ChiToKey> search(LoginRedisObj loginRedisObj, ChiToKeyApi reqChiToKeySearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ChiToKey update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ChiToKey save) throws Exception;

    ChiToKey detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<ChiToKey> findList(LoginRedisObj loginRedisObj, ChiToKey item) throws Exception;
}