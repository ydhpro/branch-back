package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.WorkerGroupBrand;
import com.jd.branch.api.request.ReqWorkerGroupBrandSave;
import com.jd.branch.api.request.ReqWorkerGroupBrandSearch;
import com.jd.branch.api.response.RespWorkerGroupBrandDetail;
import java.util.List;


/**
 * <p>
 * 师傅组品牌维护 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-09-07
 */
public interface WorkerGroupBrandService {
    PageInfo<RespWorkerGroupBrandDetail> search(LoginRedisObj loginRedisObj,ReqWorkerGroupBrandSearch reqWorkerGroupBrandSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqWorkerGroupBrandSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqWorkerGroupBrandSave save) throws Exception;

    RespWorkerGroupBrandDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespWorkerGroupBrandDetail> findList(LoginRedisObj loginRedisObj, ReqWorkerGroupBrandSearch item) throws Exception;
}