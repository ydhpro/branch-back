package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.api.request.ReqAddCategorySave;
import com.jd.branch.api.request.ReqAddCategorySearch;
import com.jd.branch.api.response.RespAddCategoryDetail;

import java.util.List;


/**
 * <p>
 * 增项类目 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-09-05
 */
public interface AddCategoryService {
    PageInfo<RespAddCategoryDetail> search(LoginRedisObj loginRedisObj, ReqAddCategorySearch reqAddCategorySearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqAddCategorySave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqAddCategorySave save) throws Exception;

    RespAddCategoryDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespAddCategoryDetail> findList(LoginRedisObj loginRedisObj, ReqAddCategorySearch item) throws Exception;
}