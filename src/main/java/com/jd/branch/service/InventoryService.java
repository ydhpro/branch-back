package com.jd.branch.service;


import com.jd.branch.api.request.ReqTypeSearch;
import com.jd.branch.api.response.RespTypeDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Inventory;
import com.jd.branch.api.request.ReqInventorySave;
import com.jd.branch.api.request.ReqInventorySearch;
import com.jd.branch.api.response.RespInventoryDetail;

import java.math.BigDecimal;
import java.util.List;


/**
 * <p>
 * 库存 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
public interface InventoryService {
    PageInfo<RespInventoryDetail> search(LoginRedisObj loginRedisObj, ReqInventorySearch reqInventorySearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqInventorySave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqInventorySave save) throws Exception;

    RespInventoryDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespInventoryDetail> findList(LoginRedisObj loginRedisObj, ReqInventorySearch item) throws Exception;

    PageInfo<RespInventoryDetail> sonSearch(LoginRedisObj loginRedisObj, ReqInventorySearch param) throws Exception;

    void pubInventoryOperation(Long tenantId, Long materialId, Long sourceId, Integer sourceType, Long targetId, Integer targetType, BigDecimal num, String remark, boolean hasLockInvent);

    PageInfo<RespTypeDetail> findtype(LoginRedisObj loginRedisObj, ReqTypeSearch param) throws Exception;

    String getMaterialCode(String prefix);

    Long getBatchNum();

    void saveSonMaster(LoginRedisObj loginRedisObj, ReqInventorySave save) throws Exception;
}