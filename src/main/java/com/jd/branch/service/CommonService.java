package com.jd.branch.service;


import com.jd.branch.api.request.ReqLogin;
import com.jd.branch.api.response.RespPersonDetail;
import com.jd.branch.common.LoginRedisObj;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ydh
 * @since 2023-04-02
 */
public interface CommonService {
    String getPhoneCode(String phone);
}
