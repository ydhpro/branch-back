package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.api.request.ReqSysDicSave;
import com.jd.branch.api.request.ReqSysDicSearch;
import com.jd.branch.api.response.RespSysDicDetail;
import java.util.List;


/**
 * <p>
 * 系统字典表 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-08-26
 */
public interface SysDicService {
    PageInfo<RespSysDicDetail> search(LoginRedisObj loginRedisObj,ReqSysDicSearch reqSysDicSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqSysDicSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqSysDicSave save) throws Exception;

    RespSysDicDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespSysDicDetail> findList(LoginRedisObj loginRedisObj, ReqSysDicSearch item) throws Exception;
}