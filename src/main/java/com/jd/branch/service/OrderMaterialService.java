package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.api.request.ReqOrderMaterialSave;
import com.jd.branch.api.request.ReqOrderMaterialSearch;
import com.jd.branch.api.response.RespOrderMaterialDetail;
import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ydh
 * @since 2024-04-29
 */
public interface OrderMaterialService {
    PageInfo<RespOrderMaterialDetail> search(LoginRedisObj loginRedisObj,ReqOrderMaterialSearch reqOrderMaterialSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqOrderMaterialSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqOrderMaterialSave save) throws Exception;

    RespOrderMaterialDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespOrderMaterialDetail> findList(LoginRedisObj loginRedisObj, ReqOrderMaterialSearch item) throws Exception;
}