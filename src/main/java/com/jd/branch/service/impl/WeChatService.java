package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.jd.branch.common.LoginRedisObj;

import com.jd.branch.util.AESUtils;
import com.jd.branch.util.GetPostUntil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.util.Base64;


/**
 * @author ydh
 * @version 1.0
 * @date 2021/2/3 23:21
 */
public class WeChatService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeChatService.class);
    //这个时候龙樱小程序的appid
    public static String mpAppid = "wx3d0dd7970aabd966";
    //这是创宁小程序的appsecret密钥
//    public static String mpSecret = "0ad2a040c229cbd71123f007451ed272";
    //这里是龙樱的appsecret
    public static String mpSecret = "0ad2a040c229cbd71123f007451ed272";
    public static String wxLoginUrl = "https://api.weixin.qq.com/sns/jscode2session";

    public static LoginRedisObj getPhoneNum(HttpServletResponse response, String jsonStr) throws Exception {
        response.setHeader("Access-Control-Allow-Origin", "*");
        /*星号表示所有的域都可以接受，*/
        response.setHeader("Access-Control-Allow-Methods", "GET,POST");
//        CommonRsp rsp = new CommonRsp();
//        JDResponseData jdResponseData=new JDResponseData();
        JSONObject jsonStrs = JSONObject.parseObject(jsonStr);
        String iv = (String) jsonStrs.get("iv");
        String encryptedData = (String) jsonStrs.get("encryptedData");
        String code = (String) jsonStrs.get("code");
//        传入获取openid
        String wxLoginUrl = "https://api.weixin.qq.com/sns/jscode2session";
        String param = "appid=" + mpAppid + "&secret=" + mpSecret + "&js_code=" + code + "&grant_type=authorization_code";
        LOGGER.info("param:" + param);
        String jsonString = GetPostUntil.sendGet(wxLoginUrl, param);
        JSONObject json = JSONObject.parseObject(jsonString);
        LOGGER.info("json=" + JSONObject.toJSONString(json));
        String sessionkey = json.getString("session_key");
//        AESUtils aes = new AESUtils();
        LOGGER.info("sessionkey=" + sessionkey);
        Base64.Decoder decoder = Base64.getDecoder();
        LOGGER.info("before result byte" + JSONObject.toJSONString(encryptedData) + "," + JSONObject.toJSONString(sessionkey) + "," + JSONObject.toJSONString(iv));
        byte[] result = AESUtils.decrypt(decoder.decode(encryptedData), decoder.decode(sessionkey),
                AESUtils.generateIV(decoder.decode(iv)));

        //判断返回参数是否为空
        if (null != result && result.length > 0) {
            String phoneNumber = "";
            try {
                String jsons = new String(result, "UTF-8");
                LOGGER.info("jsons" + jsons);
                JSONObject json2 = JSONObject.parseObject(jsons);

                //json解析phoneNumber值
                phoneNumber = (String) json2.get("phoneNumber");
                LoginRedisObj openid = LoginRedisObj.builder().phone(phoneNumber).openId(json.getString("openid")).build();
                return openid;

            } catch (JSONException e) {
                e.printStackTrace();
//                return new JDResponseData(500, "获取手机号失败" + e.getMessage());
                throw new RuntimeException("解析失败" + e.getMessage());
            }
        } else {
//            return new JDResponseData(500, "获取手机号失败");
            throw new RuntimeException("解析失败");
        }
    }


    public static String getOpenidByCode(String code) throws Exception {
        String reqUrl = "appid=" + mpAppid + "&secret=" + mpSecret + "&js_code=" + code + "&grant_type=authorization_code";
        String s = GetPostUntil.sendGet(wxLoginUrl, reqUrl);
        JSONObject jsonResult = JSONObject.parseObject(s);
        LOGGER.info("code=" + code + "," + mpAppid + "," + mpSecret + "返回：" + jsonResult.toJSONString());
        return jsonResult.getString("openid");
    }


}
