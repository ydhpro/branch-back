package com.jd.branch.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqInventorySave;
import com.jd.branch.api.request.ReqInventorySearch;
import com.jd.branch.api.request.ReqTypeSearch;
import com.jd.branch.api.request.ReqWorkerSearch;
import com.jd.branch.api.response.RespInventoryDetail;
import com.jd.branch.api.response.RespTypeDetail;
import com.jd.branch.api.response.RespWorkerDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.constant.CommonEnums;
import com.jd.branch.entity.Inventory;
import com.jd.branch.entity.InventoryStream;
import com.jd.branch.entity.Worker;
import com.jd.branch.mapper.InventoryMapper;
import com.jd.branch.mapper.InventoryStreamMapper;
import com.jd.branch.mapper.WorkerMapper;
import com.jd.branch.service.InventoryService;


import com.jd.branch.service.WorkerService;
import com.jd.branch.util.DefindBeanUtil;
import com.jd.branch.util.DefindUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * <p>
 * 库存 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Service
public class InventoryServiceImpl implements InventoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryServiceImpl.class);
    @Autowired
    private InventoryMapper mapper;

    @Autowired
    private WorkerMapper workerMapper;

    @Autowired
    private InventoryStreamMapper inventoryStreamMapper;

    @Autowired
    private WorkerService workerService;


    @Override
    public PageInfo<RespInventoryDetail> search(LoginRedisObj loginRedisObj, ReqInventorySearch param) {
        //1.构造查询条件
        QueryWrapper<Inventory> wrapper = new QueryWrapper<>(
                Inventory.builder().tenantId(param.getTenantId()).entityId(param.getEntityId()).deleteFlag(0).build());

        if (StrUtil.isNotEmpty(param.getKeyword())) {
            wrapper.and(
                    andWrapper -> andWrapper.like("material_name", param.getKeyword()));
        }


        //2.分页查询
        Page<Inventory> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<Inventory> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespInventoryDetail> details = new ArrayList<>();
        for (Inventory item : iPage.getRecords()) {
            RespInventoryDetail resp = DefindBeanUtil.copyProperties(item, RespInventoryDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespInventoryDetail> pageInfo = new PageInfo<>(
                param.getCurrentPage(),
                iPage.getTotal(),
                param.getPageSize(),
                details
        );


        return pageInfo;
    }

    @Override
    public PageInfo<RespInventoryDetail> sonSearch(LoginRedisObj loginRedisObj, ReqInventorySearch param) {
        //1.构造查询条件
        QueryWrapper<Inventory> wrapper = new QueryWrapper<>(
                Inventory.builder().tenantId(param.getTenantId()).build()).ne("type", 1);

        if (StrUtil.isNotEmpty(param.getKeyword())) {
            wrapper.and(
                    andWrapper -> andWrapper.like("material_name", param.getKeyword()));
        }


        //2.分页查询
        Page<Inventory> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<Inventory> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespInventoryDetail> details = new ArrayList<>();
        for (Inventory item : iPage.getRecords()) {
            RespInventoryDetail resp = DefindBeanUtil.copyProperties(item, RespInventoryDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespInventoryDetail> pageInfo = new PageInfo<>(
                param.getCurrentPage(),
                iPage.getTotal(),
                param.getPageSize(),
                details
        );


        return pageInfo;
    }

    @Override
    public List<RespInventoryDetail> findList(LoginRedisObj loginRedisObj, ReqInventorySearch req) throws Exception {
        Inventory param = JSONObject.parseObject(JSONObject.toJSONString(req), Inventory.class);
        List<Inventory> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespInventoryDetail> details = new ArrayList<>();
        for (Inventory item : list) {
            RespInventoryDetail detail = DefindBeanUtil.copyProperties(item, RespInventoryDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj, Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj, ReqInventorySave item) {
        Inventory update = DefindBeanUtil.copyProperties(item, Inventory.class);
        mapper.updateById(update);
    }

    @Override
    public void saveMaster(LoginRedisObj loginRedisObj, ReqInventorySave add) throws Exception {
        Inventory save = JSONObject.parseObject(JSONObject.toJSONString(add), Inventory.class);


        save.setMaterialCode(getMaterialCode("WL"));

        save.setBatchNum(getBatchNum());
        save.setTenantId(loginRedisObj.getTenantId());
        if (add.getType() == 1) {
            //主库
            save.setEntityId(0L);
        }
        save.setLatestTime(new Date());

        mapper.insert(save);
    }

    @Override
    public void saveSonMaster(LoginRedisObj loginRedisObj, ReqInventorySave add) throws Exception {
        Inventory save = JSONObject.parseObject(JSONObject.toJSONString(add), Inventory.class);


        save.setMaterialCode(getMaterialCode("WL"));

        save.setBatchNum(getBatchNum());
        save.setTenantId(loginRedisObj.getTenantId());
        if (add.getType() == 1) {
            //主库
            save.setEntityId(0L);
        } else {
            save.setEntityId(add.getTargetId());
            save.setEntityName(add.getTargetName());
        }
        save.setLatestTime(new Date());

        mapper.insert(save);
    }

    @Override
    public RespInventoryDetail detail(LoginRedisObj loginRedisObj, Long id) {
        Inventory byId = mapper.selectById(id);
        RespInventoryDetail resp = transferOne(byId);
        return resp;
    }

    private RespInventoryDetail transferOne(Inventory item) {
        RespInventoryDetail resp = DefindBeanUtil.copyProperties(item, RespInventoryDetail.class);
        return resp;
    }

    @Override
    //永远只有正数，由source到target
    //此参数代表该操作是否有锁定库存，如果有，则直接从锁定库存中减，而不从总库存减 hasLockInvent
    @Transactional
    public void pubInventoryOperation(Long tenantId, Long materialId, Long sourceId, Integer sourceType, Long targetId, Integer targetType, BigDecimal num, String remark, boolean hasLockInvent) {
        if (sourceId != null && targetId != null) {
            //源头库存
            List<Inventory> sourceInventories = mapper.selectList(new QueryWrapper<>(Inventory.builder().id(materialId).tenantId(tenantId).build()));
            //目标库存
//            List<Inventory> targetInventories = mapper.selectList(new QueryWrapper<>(Inventory.builder().id(materialId).entityId(targetId).sourceId(materialId).tenantId(tenantId).build()));

            Inventory sourInv = sourceInventories.size() > 0 ? sourceInventories.get(0) : null;


            String sourceName = "主库", targetName = "主库";
            if (sourceType == 2) {
                Worker worker = workerMapper.selectById(sourceId);
                if (worker != null) {
                    sourceName = worker.getName();
                }
            }
            if (targetType == 2) {
                Worker worker = workerMapper.selectById(targetId);
                if (worker != null) {
                    targetName = worker.getName();
                    //如果是分配给师傅的,只要分给他一段电缆,就新增一条激流
                    Inventory tarInv = Inventory.builder().sourceId(materialId)
                            .tenantId(tenantId)
                            .materialCode(getMaterialCode("WL"))
                            .entityId(targetId)
                            .type(targetType)
                            .entityName(targetName)
                            .lockNum(new BigDecimal(0D))
                            .num(num)
                            .latestTime(new Date())
                            .materialName(sourInv.getMaterialName())
                            .model(sourInv.getModel())
                            .unit(sourInv.getUnit())
                            .price(sourInv.getPrice())
                            .batchNum(sourInv.getBatchNum())
                            .meterNum(sourInv.getMeterNum())
                            .isCable(sourInv.getIsCable())
                            .build();

                    mapper.insert(tarInv);


                    InventoryStream build = InventoryStream.builder().tenantId(tenantId).sourceId(sourceId).sourceName(sourceName).sourceCurrentNum(sourInv.getNum()).streamNum(num).targetId(targetId).targetName(targetName).targetCurrentNum(tarInv.getNum()).build();
                    build.setSourceAftNum(build.getSourceCurrentNum().subtract(build.getStreamNum()));
                    build.setTargetAftNum(build.getTargetCurrentNum().add(build.getStreamNum()));
                    build.setMaterialName(sourInv.getMaterialName());
                    build.setMaterialId(sourInv.getId());
                    inventoryStreamMapper.insert(build);

                    if (hasLockInvent) {
                        //如果是true说明有锁库存，则库存结算时直接减锁定库存的数量
                        sourInv.setLockNum(sourInv.getLockNum().subtract(num));
                    } else {
                        //否则直接减总库存数量
                        sourInv.setNum(sourInv.getNum().subtract(num));
                    }

//                    tarInv.setNum(tarInv.getNum().add(num));
                    mapper.updateById(sourInv);
                    mapper.updateById(tarInv);
                }
            }
        } else if (sourceId == null) {
            //源头为空，说明是从外部购买后进入主库
            List<Inventory> inventories = mapper.selectList(new QueryWrapper<>(Inventory.builder().tenantId(tenantId).id(materialId).entityId(0L).build()));
            InventoryStream build = InventoryStream.builder().tenantId(tenantId).sourceName("外部购买").streamNum(num).targetId(targetId).targetName("主库").build();
            if (inventories.size() > 0) {
                build.setTargetCurrentNum(inventories.get(0).getNum());
                build.setTargetAftNum(build.getTargetCurrentNum().add(build.getStreamNum()));
                build.setMaterialName(inventories.get(0).getMaterialName());
                build.setMaterialId(inventories.get(0).getId());
            }

            inventoryStreamMapper.insert(build);

            if (hasLockInvent) {
                //如果是true说明有锁库存，则库存结算时直接减锁定库存的数量
                inventories.get(0).setLockNum(inventories.get(0).getLockNum().subtract(build.getStreamNum()));
            } else {
                //否则直接减总库存数量
                inventories.get(0).setNum(inventories.get(0).getNum().subtract(build.getStreamNum()));
            }

            mapper.updateById(inventories.get(0));
        } else if (targetId == null) {
            //目标为空，说明是从安装/报废/遗失消耗的
            List<Inventory> inventories = mapper.selectList(new QueryWrapper<>(Inventory.builder().tenantId(tenantId).id(materialId).entityId(sourceId).build()));
            InventoryStream build = InventoryStream.builder().tenantId(tenantId).materialId(materialId).sourceId(sourceId).streamNum(num).targetName("消耗").build();
            if (inventories.size() > 0) {
                build.setSourceName(inventories.get(0).getEntityName());
                build.setMaterialName(inventories.get(0).getMaterialName());
                build.setMaterialId(inventories.get(0).getId());
                build.setSourceCurrentNum(inventories.get(0).getNum());
                build.setSourceAftNum(inventories.get(0).getNum().subtract(build.getStreamNum()));

                if (hasLockInvent) {
                    build.setSourceCurrentNum(inventories.get(0).getNum().add(build.getStreamNum()));
                    build.setSourceCurrentNum(inventories.get(0).getNum());
                }


            }

            if (hasLockInvent) {
                //如果是true说明有锁库存，则库存结算时直接减锁定库存的数量
                inventories.get(0).setLockNum(inventories.get(0).getLockNum().subtract(build.getStreamNum()));
            } else {
                //否则直接减总库存数量
                inventories.get(0).setNum(inventories.get(0).getNum().subtract(build.getStreamNum()));
            }
            build.setRemark(remark);
            inventoryStreamMapper.insert(build);

            mapper.updateById(inventories.get(0));
        }

    }

    public String getMaterialCode(String prefix) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddHHmmssSSS");
        String date = simpleDateFormat.format(new Date());
        Random random = new Random();
        int rannum = (int) (random.nextDouble() * (99999 - 10000 + 1)) + 10000;
        String materialCode = "WL" + date + rannum;
        return materialCode;
    }

    public Long getBatchNum() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String date = simpleDateFormat.format(new Date());
        return Long.valueOf(date);
    }


    public PageInfo<RespTypeDetail> findtype(LoginRedisObj loginRedisObj, ReqTypeSearch param) throws Exception {
        if (param.getType().equals(2)) {
            PageInfo<RespWorkerDetail> search = workerService.search(loginRedisObj, ReqWorkerSearch.builder().keyword(param.getKeyword()).build());
            List<RespTypeDetail> retList = new ArrayList<>();
            for (RespWorkerDetail r : search.getList()) {
                RespTypeDetail build = RespTypeDetail.builder().name(r.getName()).id(r.getId()).build();
                retList.add(build);
            }
            PageInfo<RespTypeDetail> pageInfo = new PageInfo<>(
                    param.getCurrentPage(),
                    search.getTotal(),
                    param.getPageSize(),
                    retList
            );
            return pageInfo;
        } else {
            return null;
        }
    }
}
