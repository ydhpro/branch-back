package com.jd.branch.service.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.ShortMsgForBxLogin;
import com.jd.branch.common.ShortMsgForBxLoginItem;
import com.jd.branch.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author yuandh5
 * @date 2024/3/21
 */
@Service
public class CommonServiceImpl implements CommonService {
    public static String msgApi = "https://api-v4.mysubmail.com/sms/send";
    public static String msgServiceUrl = "https://api.mysubmail.com/message/xsend.json";
    public static String msgModel = "Kll0S1";
    public static String appKey = "921aae84345eb19630b00a0b3c8f0c7c";
    public static String appid = "69367";


    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
//    https://www.mysubmail.com/
//    验证码用的是这里的
    public String getPhoneCode(String phone) {
//        phone = "15914592970";
        String s = RandomUtil.randomNumbers(6);
        ShortMsgForBxLogin build = ShortMsgForBxLogin.builder().appid(appid).to(phone).vars(ShortMsgForBxLoginItem.builder().code(s).build()).project(msgModel).signature(appKey).build();
        redisTemplate.opsForValue().set("phonecode:" + phone, s, 30, TimeUnit.MINUTES);
        HttpUtil.post(msgServiceUrl, JSONObject.toJSONString(build));
        return s;
    }
}
