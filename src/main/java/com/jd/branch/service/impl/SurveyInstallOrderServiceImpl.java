package com.jd.branch.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.*;
import com.jd.branch.api.response.RespSurveyInstallOrderDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.constant.CommonEnums;
import com.jd.branch.entity.*;
import com.jd.branch.mapper.*;
import com.jd.branch.service.InventoryService;
import com.jd.branch.service.PicAttachmentService;
import com.jd.branch.service.SurveyInstallOrderService;


import com.jd.branch.util.DefindBeanUtil;
import com.jd.branch.util.DefindUtils;
import org.apache.commons.collections4.queue.PredicatedQueue;
import org.redisson.MapWriterTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * <p>
 * 勘安工单 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Service
public class SurveyInstallOrderServiceImpl implements SurveyInstallOrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyInstallOrderServiceImpl.class);
    @Autowired
    private SurveyInstallOrderMapper mapper;

    @Autowired
    private WorkerGroupMapper workerGroupMapper;

    @Autowired
    private PicAttachmentService picAttachmentService;

    @Autowired
    private OrderMaterialMapper orderMaterialMapper;

    @Autowired
    private InventoryMapper inventoryMapper;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private ChiToKeyMapper chiToKeyMapper;

    @Autowired
    private AddExpMapper addExpMapper;

    @Autowired
    private ComBrandMapper comBrandMapper;

    @Autowired
    private WorkerGroupBrandMapper workerGroupBrandMapper;

    @Autowired
    private OrderFeeMapper orderFeeMapper;


    @Override
    public PageInfo<RespSurveyInstallOrderDetail> search(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch param) {
        //1.构造查询条件
        QueryWrapper<SurveyInstallOrder> wrapper = new QueryWrapper<>(
                SurveyInstallOrder.builder().tenantId(param.getTenantId()).deleteFlag(0).build()).orderByDesc("create_time");


        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<SurveyInstallOrder> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<SurveyInstallOrder> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespSurveyInstallOrderDetail> details = new ArrayList<>();
        for (SurveyInstallOrder item : iPage.getRecords()) {
            RespSurveyInstallOrderDetail resp = transferOne(item);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespSurveyInstallOrderDetail> pageInfo = new PageInfo<>(
                param.getCurrentPage(),
                iPage.getTotal(),
                param.getPageSize(),
                details
        );


        return pageInfo;
    }

    @Override
    public List<RespSurveyInstallOrderDetail> findList(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch req) throws Exception {
        SurveyInstallOrder param = JSONObject.parseObject(JSONObject.toJSONString(req), SurveyInstallOrder.class);
        List<SurveyInstallOrder> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespSurveyInstallOrderDetail> details = new ArrayList<>();
        for (SurveyInstallOrder item : list) {
            RespSurveyInstallOrderDetail detail = DefindBeanUtil.copyProperties(item, RespSurveyInstallOrderDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj, Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave item) {
        SurveyInstallOrder update = DefindBeanUtil.copyProperties(item, SurveyInstallOrder.class);
        if (item.getBrandId() != null) {
            ComBrand comBrand = comBrandMapper.selectById(item.getBrandId());
            if (comBrand != null) {
                update.setBrandName(comBrand.getName());
            }
        }
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave add) {
        SurveyInstallOrder save = JSONObject.parseObject(JSONObject.toJSONString(add), SurveyInstallOrder.class);
        save.setTenantId(loginRedisObj.getTenantId());
        mapper.insert(save);
    }

    @Override
    public RespSurveyInstallOrderDetail detail(LoginRedisObj loginRedisObj, Long id) {
        SurveyInstallOrder byId = mapper.selectById(id);
        RespSurveyInstallOrderDetail resp = transferOne(byId);
        return resp;
    }

    private RespSurveyInstallOrderDetail transferOne(SurveyInstallOrder item) {
        RespSurveyInstallOrderDetail resp = DefindBeanUtil.copyProperties(item, RespSurveyInstallOrderDetail.class);
        resp.setStateName(CommonEnums.SIOrderState.getNameByIndex(resp.getState()));
        return resp;
    }

    @Override
//    勘测派单
    public void surDispatch(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave item) throws Exception {
        SurveyInstallOrder surveyInstallOrder = mapper.selectById(item.getId());
        if (item.getSurveyWorkerGroupId() != null) {
            WorkerGroup workerGroup = workerGroupMapper.selectById(item.getSurveyWorkerGroupId());
            String[] split = workerGroup.getName().split(",");
            surveyInstallOrder.setSurveyWorkerGroupName(workerGroup.getName());
            surveyInstallOrder.setSurveyWorker1Id(workerGroup.getWorker1Id());
            surveyInstallOrder.setSurveyWorker2Id(workerGroup.getWorker2Id());
            surveyInstallOrder.setSurveyWorker1Name(split[0]);
            surveyInstallOrder.setSurveyWorker2Name(split[1]);
            surveyInstallOrder.setState(CommonEnums.SIOrderState.KCSJQR.getIndex());
            surveyInstallOrder.setWaitSurveyTime(new Date());
            surveyInstallOrder.setSurveyWorkerGroupId(item.getSurveyWorkerGroupId());
        }

        if (item.getInstallWorkerGroupId() != null) {
            WorkerGroup workerGroup = workerGroupMapper.selectById(item.getInstallWorkerGroupId());
            String[] split = workerGroup.getName().split(",");
            surveyInstallOrder.setInstallWorkerGroupName(workerGroup.getName());
            surveyInstallOrder.setInstallWorker1Id(workerGroup.getWorker1Id());
            surveyInstallOrder.setInstallWorker2Id(workerGroup.getWorker2Id());
            surveyInstallOrder.setInstallWorker1Name(split[0]);
            surveyInstallOrder.setInstallWorker2Name(split[1]);
            surveyInstallOrder.setState(CommonEnums.SIOrderState.AZSJQR.getIndex());
            surveyInstallOrder.setWaitInstallTime(new Date());
            surveyInstallOrder.setInstallWorkerGroupId(item.getInstallWorkerGroupId());
        }


        mapper.updateById(surveyInstallOrder);
    }

    @Override
    public void submitSurTime(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave item) throws Exception {
        SurveyInstallOrder surveyInstallOrder = mapper.selectById(item.getId());
        surveyInstallOrder.setSurveyPlanStartTime(item.getSurveyPlanStartTime());
        surveyInstallOrder.setSurveyPlanEndTime(item.getSurveyPlanEndTime());
        if (surveyInstallOrder.getSurveyPlanStartTime() != null && surveyInstallOrder.getSurveyPlanEndTime() != null) {
            surveyInstallOrder.setState(CommonEnums.SIOrderState.DKC.getIndex());
            //如果这两个时间都存在，说明现在已经开始勘测了,那现在就是实际开始时间
            surveyInstallOrder.setSurverActStartTime(new Date());
        }

        mapper.updateById(surveyInstallOrder);
    }

    @Override
    public PageInfo<RespSurveyInstallOrderDetail> workSearch(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSearch param) throws Exception {
        //1.构造查询条件
        QueryWrapper<SurveyInstallOrder> wrapper = new QueryWrapper<>(
                SurveyInstallOrder.builder().tenantId(param.getTenantId()).build());

        wrapper.and(
                andWrapper -> andWrapper.eq("survey_worker1_id", loginRedisObj.getId())
                        .or().eq("survey_worker2_id", loginRedisObj.getId())
                        .or().eq("install_worker1_id", loginRedisObj.getId())
                        .or().eq("install_worker2_id", loginRedisObj.getId())
        );

        if (param.getSearchType() != null) {
            if (param.getSearchType() == 0) {
                wrapper.and(andWrapper -> andWrapper.in("state", CollUtil.newArrayList(2, 3, 5, 7, 8)));
            } else if (param.getSearchType() == 1) {
                wrapper.and(andWrapper -> andWrapper.in("state", CollUtil.newArrayList(4, 9)));
            }
        }

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<SurveyInstallOrder> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<SurveyInstallOrder> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespSurveyInstallOrderDetail> details = new ArrayList<>();
        for (SurveyInstallOrder item : iPage.getRecords()) {
            RespSurveyInstallOrderDetail resp = transferOne(item);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespSurveyInstallOrderDetail> pageInfo = new PageInfo<>(
                param.getCurrentPage(),
                iPage.getTotal(),
                param.getPageSize(),
                details
        );


        return pageInfo;
    }

    @Override
    public void auditSurvey(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderAuditSurvey reqSurveyInstallOrderAuditSurvey) throws Exception {
        //处理图片审核结果
        //0不通过1通过
        int auditFlag = 1;
        for (ReqPicAttachmentSave pic : reqSurveyInstallOrderAuditSurvey.getPicList()) {
            if (StrUtil.isNotEmpty(pic.getComment())) {
                pic.setIsAudited(0);
                auditFlag = 0;
            } else {
                pic.setIsAudited(1);
            }
            picAttachmentService.updateById(loginRedisObj, pic);
        }
        //处理单据结果:如果文字和图片有一个审核不通过，整个单都是驳回
        if (StrUtil.isNotEmpty(reqSurveyInstallOrderAuditSurvey.getTextAuditRes())) {
            auditFlag = 0;
        }

        reqSurveyInstallOrderAuditSurvey.setSurveyAuditResult(auditFlag);
        if (auditFlag == 0) {
            reqSurveyInstallOrderAuditSurvey.setState(CommonEnums.SIOrderState.KCBH.getIndex());
        } else {
            reqSurveyInstallOrderAuditSurvey.setState(CommonEnums.SIOrderState.AZDPD.getIndex());
            //如果审核通过了，说明勘测已完成
            reqSurveyInstallOrderAuditSurvey.setSurverActEndTime(new Date());
        }

        SurveyInstallOrder s = JSONObject.parseObject(JSONObject.toJSONString(reqSurveyInstallOrderAuditSurvey), SurveyInstallOrder.class);
        mapper.updateById(s);

    }

    @Override
    @Transactional
    public void finishInstall(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderAuditSurvey reqSurveyInstallOrderAuditSurvey) throws Exception {
        SurveyInstallOrder surveyInstallOrder = mapper.selectById(reqSurveyInstallOrderAuditSurvey.getId());
        surveyInstallOrder.setState(CommonEnums.SIOrderState.AZDSH.getIndex());
        surveyInstallOrder.setInstallActEndTime(new Date());
        mapper.updateById(surveyInstallOrder);

        OrderFee calFormaleAndResult = getCalFormaleAndResult(reqSurveyInstallOrderAuditSurvey.getId());
        orderFeeMapper.insert(calFormaleAndResult);


    }

    @Override
    @Transactional
    public OrderFee getCalFormaleAndResult(Long id) throws Exception {
        SurveyInstallOrder sio = mapper.selectById(id);

        OrderFee orderFee = OrderFee.builder()
                .orderIncome(new BigDecimal(0))
                .addIncome(new BigDecimal(0))
                .commuteSubsidy(new BigDecimal(0))
                .materialExpand(new BigDecimal(0))
                .otherExpand(new BigDecimal(0))
                .workerCommision(new BigDecimal(0))
                .workerExpand(new BigDecimal(0))
                .otherIncome(new BigDecimal(0))
                .workerInstallSettleFee(new BigDecimal(0))
                .workerSurveySettleFee(new BigDecimal(0))
                .build();
        orderFee.setOrderId(id);
        //计算成本
        List<OrderMaterial> orderMaterials = orderMaterialMapper.selectList(new QueryWrapper<>(OrderMaterial.builder().orderId(sio.getId()).build()));
        BigDecimal materialAmount = new BigDecimal(0D);
        BigDecimal addIncome = new BigDecimal(0D);
        for (OrderMaterial om : orderMaterials) {
            if (om.getPrice() == null) {
                Inventory inventory = inventoryMapper.selectById(om.getMaterialId());
                if (inventory != null) {
                    om.setPrice(inventory.getPrice());
                }
            }
            materialAmount = materialAmount.add(om.getPrice().multiply(om.getNum()));
        }
        List<AddExp> addExps = addExpMapper.selectList(new QueryWrapper<>(AddExp.builder().orderId(sio.getId()).build()));
        for (AddExp ae : addExps) {
            if (ae.getNum() != null && ae.getPrice() != null) {
                addIncome = addIncome.add(ae.getNum().multiply(ae.getPrice()));
            }
        }

        //如果勘测安装在同一天，则不结算勘测费用
        WorkerGroupBrand installWorkerGroupBrand = workerGroupBrandMapper.selectOne(new QueryWrapper<>(WorkerGroupBrand.builder().brandId(sio.getBrandId()).workerGroupId(sio.getInstallWorkerGroupId()).build()));
        WorkerGroup installWorkerGroup = workerGroupMapper.selectById(installWorkerGroupBrand.getWorkerGroupId());
        WorkerGroupBrand surveyWorkerGroupBrand = workerGroupBrandMapper.selectOne(new QueryWrapper<>(WorkerGroupBrand.builder().brandId(sio.getBrandId()).workerGroupId(sio.getSurveyWorkerGroupId()).build()));
        WorkerGroup surveyWorkerGroup = workerGroupMapper.selectById(surveyWorkerGroupBrand.getWorkerGroupId());

        ComBrand comBrand = comBrandMapper.selectById(sio.getBrandId());
        orderFee.setOrderIncome(comBrand.getUpSettlePrice());
        DefindUtils.check(comBrand == null, "暂无" + sio.getBrandId() + ":" + sio.getBrandName() + "该品牌信息");
        String formula = "", calresult = "";
        List<BigDecimal> formulaNums = new ArrayList<>();
        if (CommonEnums.WorkerGroupComType.QB.getIndex().equals(surveyWorkerGroup.getComType())) {
            //全包
            orderFee.setWorkerSurveySettleFee(surveyWorkerGroupBrand.getAllPackSurveySettlePrice());
            if (!DateUtil.format(sio.getInstallActEndTime(), "yyyy-MM-dd").equals(DateUtil.format(sio.getSurverActEndTime(), "yyyy-MM-dd"))) {
                //不相等说明不是同一天,需要额外结算勘测费
                orderFee.setWorkerSurveySettleFee(surveyWorkerGroupBrand.getAllPackSurveySettlePrice());
            } else {
                orderFee.setWorkerSurveySettleFee(new BigDecimal(0));
            }
            formulaNums.add(orderFee.getWorkerSurveySettleFee());
            //全包安装结算价
            orderFee.setWorkerInstallSettleFee(installWorkerGroupBrand.getAllPackInstallSettlePrice());
            formulaNums.add(orderFee.getWorkerInstallSettleFee());
            //计算全包师傅提成:工单额外收入*抽取比例
            orderFee.setWorkerCommision(sio.getOtherIncome() == null ? new BigDecimal(0) : sio.getOtherIncome().multiply(installWorkerGroupBrand.getAllPackAddExtractRate() == null ? new BigDecimal(0) : installWorkerGroupBrand.getAllPackAddExtractRate()));
            formulaNums.add(sio.getOtherIncome());
            formulaNums.add(installWorkerGroupBrand.getAllPackAddExtractRate());
            formula = "勘测结算价+安装结算价-套包外收取金额*抽取比例={0}+{1}-({2}*{3})={4}";
        } else if (CommonEnums.WorkerGroupComType.BB.getIndex().equals(surveyWorkerGroup.getComType())) {
            //半包
            if (!DateUtil.format(sio.getInstallActEndTime(), "yyyy-MM-dd").equals(DateUtil.format(sio.getSurverActEndTime(), "yyyy-MM-dd"))) {
                //不相等说明不是同一天,需要额外结算勘测费
                orderFee.setWorkerSurveySettleFee(surveyWorkerGroupBrand.getHalfPackSurveySettlePrice());
            } else {
                orderFee.setWorkerSurveySettleFee(new BigDecimal(0));
            }
            formulaNums.add(orderFee.getWorkerSurveySettleFee());
            orderFee.setWorkerInstallSettleFee(installWorkerGroupBrand.getHalfPackInstallSettlePrice());
            formulaNums.add(orderFee.getWorkerInstallSettleFee());
            BigDecimal workerCommission = new BigDecimal(0);
            //增项费用计算公式
            String zxCalFormula = "";
            //增项费用计算结果
            String zxCalRes = "";
            //计算师傅提成
            if (sio.getMeters().compareTo(comBrand.getPackMeter()) > 0) {
                formulaNums.add(sio.getMeters());
                formulaNums.add(comBrand.getPackMeter());
                //大于套包米数
                formulaNums.add(installWorkerGroupBrand.getPackOutMeterCommission());
                workerCommission = workerCommission.add(sio.getMeters().subtract(comBrand.getPackMeter())).multiply(installWorkerGroupBrand.getPackOutMeterCommission());
                //增项费用提成
                CalAddExpDTO calAddExpDTO = calAddExpCommission(addExps, installWorkerGroupBrand, formulaNums);
                zxCalFormula = calAddExpDTO.getFormula();
                zxCalRes = calAddExpDTO.getRes();
                workerCommission = workerCommission.add(calAddExpDTO.getTotal());
                orderFee.setWorkerCommision(workerCommission);
                //公里数*补贴
                //人工支出=（师傅工单结算价+师傅总提成+通勤补贴）
                formulaNums.add(installWorkerGroupBrand.getDistanceSubsidy());
                formulaNums.add(sio.getWorkerCommuteNum());
                orderFee.setCommuteSubsidy(installWorkerGroupBrand.getDistanceSubsidy().multiply(sio.getWorkerCommuteNum()));
                orderFee.setWorkerExpand(workerCommission.add(orderFee.getCommuteSubsidy()));
                formulaNums.add(sio.getOtherExpend() == null ? new BigDecimal(0) : sio.getOtherExpend());


                String f1 = "{0}+{1}+({2}-{3})*{4}+" + zxCalRes + "{5}*{6}+{7}";
                String aftf1 = replaceFormula(f1, formulaNums);
                ScriptEngineManager manager = new ScriptEngineManager();
                ScriptEngine engine = manager.getEngineByName("JavaScript");
                String string = engine.eval(aftf1).toString();
                formula = "勘测结算价+安装结算价+(总米数-套包米数)*每米提成+" + zxCalFormula + "公里数*补贴+其他开支" + "=" + aftf1 + "=" + string;
            } else {
                //等于或小于套包米数
                workerCommission = workerCommission.add(sio.getMeters().subtract(comBrand.getPackMeter())).multiply(installWorkerGroupBrand.getPackOutMeterCommission());
                //增项费用提成
                CalAddExpDTO calAddExpDTO = calAddExpCommission(addExps, installWorkerGroupBrand, formulaNums);
                zxCalFormula = calAddExpDTO.getFormula();
                zxCalRes = calAddExpDTO.getRes();
                workerCommission = workerCommission.add(calAddExpDTO.getTotal());
                orderFee.setWorkerCommision(workerCommission);
                //公里数*补贴
                //人工支出=（师傅工单结算价+师傅总提成+通勤补贴）
                formulaNums.add(installWorkerGroupBrand.getDistanceSubsidy());
                formulaNums.add(sio.getWorkerCommuteNum());
                orderFee.setCommuteSubsidy(installWorkerGroupBrand.getDistanceSubsidy().multiply(sio.getWorkerCommuteNum()));
                orderFee.setWorkerExpand(workerCommission.add(orderFee.getCommuteSubsidy()));
                formulaNums.add(sio.getOtherExpend() == null ? new BigDecimal(0) : sio.getOtherExpend());


                String f1 = "{0}+{1}+" + zxCalRes + "{2}*{3}+{4}";
                String aftf1 = replaceFormula(f1, formulaNums);
                ScriptEngineManager manager = new ScriptEngineManager();
                ScriptEngine engine = manager.getEngineByName("JavaScript");
                String string = engine.eval(aftf1).toString();
                formula = "勘测结算价+安装结算价+" + zxCalFormula + "公里数*补贴+其他开支" + "=" + aftf1 + "=" + string;
            }

        }

        orderFee.setWorkerIncomeFormula(formula);

        orderFee.setMaterialExpand(materialAmount);
        orderFee.setOtherExpand(sio.getOtherExpend() == null ? new BigDecimal(0) : sio.getOtherExpend());
        orderFee.setOtherIncome(sio.getOtherIncome() == null ? new BigDecimal(0) : sio.getOtherIncome());

        orderFee.setAddIncome(addIncome);
        //计算人工成本

        return orderFee;
    }

    public String replaceFormula(String str, List<BigDecimal> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            str = str.replace("{" + i + "}", numbers.get(i) + "");
        }
        return str;
    }

    public CalAddExpDTO calAddExpCommission(List<AddExp> addExps, WorkerGroupBrand wgb, List<BigDecimal> formulaNums) {
        BigDecimal bd = new BigDecimal(0);
        String ret = "";
        String res = "";
        List<BigDecimal> list = new ArrayList<>();

        //计算增项提成
//        for (AddExp addExp : addExps) {
//            if (addExp.getName().equals(CommonEnums.AddExpName.BHX.getName())) {
//                ret = ret + CommonEnums.AddExpName.BHX.getName() + "*提成金额+";
//                res = res + wgb.getProtectBoxCommission() + "*" + addExp.getNum() + "+";
//                bd.add(wgb.getProtectBoxCommission().multiply(addExp.getNum()));
//            } else if (addExp.getName().equals(CommonEnums.AddExpName.KK.getName())) {
//                ret = ret + CommonEnums.AddExpName.KK.getName() + "*提成金额+";
//                res = res + wgb.getBlankOpenCommission() + "*" + addExp.getNum() + "+";
//                bd.add(wgb.getBlankOpenCommission().multiply(addExp.getNum()));
//            } else if (addExp.getName().equals(CommonEnums.AddExpName.KW.getName())) {
//                ret = ret + CommonEnums.AddExpName.KW.getName() + "*提成金额+";
//                res = res + wgb.getDigCommission() + "*" + addExp.getNum() + "+";
//                bd.add(wgb.getDigCommission().multiply(addExp.getNum()));
//            } else if (addExp.getName().equals(CommonEnums.AddExpName.DK.getName())) {
//                ret = ret + CommonEnums.AddExpName.DK.getName() + "*提成金额+";
//                res = res + wgb.getHoleCommission() + "*" + addExp.getNum() + "+";
//                bd.add(wgb.getHoleCommission().multiply(addExp.getNum()));
//            } else if (addExp.getName().equals(CommonEnums.AddExpName.BHXINS.getName())) {
//                ret = ret + CommonEnums.AddExpName.BHXINS.getName() + "*提成金额+";
//                res = res + wgb.getProtectBoxInstallCommission() + "*" + addExp.getNum() + "+";
//                bd.add(wgb.getProtectBoxInstallCommission().multiply(addExp.getNum()));
//            }
//        }
        for (AddExp addExp : addExps) {
            ret = ret + addExp.getName() + "*提成金额+";
            res = res + addExp.getSettlePrice() + "*" + addExp.getNum() + "+";
            bd.add(addExp.getSettlePrice().multiply(addExp.getNum()));
        }
        CalAddExpDTO build = CalAddExpDTO.builder().total(bd).formula(ret).res(res).build();
        return build;
    }

    @Override
    @Transactional
    public void saveMaterialList(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSaveMaterialList item) throws Exception {
        //查询单据，主要为了获取师傅的id
        SurveyInstallOrder surveyInstallOrder = mapper.selectById(item.getOrderId());
        //删除前要把安装师傅当前锁定的物料库存释放了才行
        List<OrderMaterial> orderMaterials = orderMaterialMapper.selectList(new QueryWrapper<>(OrderMaterial.builder().tenantId(loginRedisObj.getTenantId()).orderId(item.getOrderId()).build()));
        for (OrderMaterial om : orderMaterials) {
            List<Inventory> inventories = inventoryMapper.selectList(new QueryWrapper<>(Inventory.builder().tenantId(loginRedisObj.getTenantId()).id(om.getMaterialId()).entityId(surveyInstallOrder.getInstallWorker1Id()).build()));
            if (inventories.size() > 0) {
                inventories.get(0).setNum(inventories.get(0).getNum().add(om.getNum()));
                inventories.get(0).setLockNum(inventories.get(0).getLockNum().subtract(om.getNum()));
                inventoryMapper.updateById(inventories.get(0));
            }
        }

        orderMaterialMapper.delete(new QueryWrapper<>(OrderMaterial.builder().tenantId(loginRedisObj.getTenantId()).orderId(item.getOrderId()).build()));

        for (ReqOrderMaterialSave req : item.getMaterialList()) {
            OrderMaterial orderMaterial = DefindBeanUtil.copyProperties(req, OrderMaterial.class);
            orderMaterial.setOrderId(item.getOrderId());
            orderMaterial.setTenantId(loginRedisObj.getTenantId());
            orderMaterial.setId(null);
            orderMaterialMapper.insert(orderMaterial);

            List<Inventory> inventories = inventoryMapper.selectList(new QueryWrapper<>(Inventory.builder().tenantId(loginRedisObj.getTenantId()).id(orderMaterial.getMaterialId()).entityId(surveyInstallOrder.getInstallWorker1Id()).build()));
            DefindUtils.check(inventories.size() == 0, "请为" + surveyInstallOrder.getInstallWorker1Name() + "师傅配货：" + orderMaterial.getNum() + "单位的" + orderMaterial.getMaterialName());
            if (inventories.size() > 0) {
                inventories.get(0).setNum(inventories.get(0).getNum().subtract(orderMaterial.getNum()));
                inventories.get(0).setLockNum(inventories.get(0).getLockNum().add(orderMaterial.getNum()));
                inventoryMapper.updateById(inventories.get(0));
            }
        }
    }

    @Override
    @Transactional
    public void end(LoginRedisObj loginRedisObj, ReqCommDel reqCommDel) throws Exception {
        //结单，修改单据状态，库存扣减
        SurveyInstallOrder surveyInstallOrder = mapper.selectById(reqCommDel.getId());

        List<OrderMaterial> orderMaterials = orderMaterialMapper.selectList(new QueryWrapper<>(OrderMaterial.builder().tenantId(loginRedisObj.getTenantId()).orderId(reqCommDel.getId()).build()));
        for (OrderMaterial om : orderMaterials) {
            List<Inventory> inventories = inventoryMapper.selectList(new QueryWrapper<>(Inventory.builder().tenantId(loginRedisObj.getTenantId()).id(om.getMaterialId()).build()));
            if (inventories.size() > 0) {
                inventoryService.pubInventoryOperation(loginRedisObj.getTenantId(), om.getMaterialId(), inventories.get(0).getEntityId(), CommonEnums.InventoryType.SF.getIndex(), null, null, om.getNum(), "安装消耗：" + om.getNum(), true);
            }
        }
        //修改单据状态
        surveyInstallOrder.setState(CommonEnums.SIOrderState.YJD.getIndex());
        mapper.updateById(surveyInstallOrder);
    }

    @Override
    public void submitInstallTime(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave item) throws Exception {
        SurveyInstallOrder surveyInstallOrder = mapper.selectById(item.getId());
        surveyInstallOrder.setInstallPlanStartTime(item.getInstallPlanStartTime());
        surveyInstallOrder.setInstallPlanEndTime(item.getInstallPlanEndTime());

        if (surveyInstallOrder.getInstallPlanStartTime() != null && surveyInstallOrder.getInstallPlanEndTime() != null) {
            surveyInstallOrder.setState(CommonEnums.SIOrderState.DAZ.getIndex());
            //如果这两个时间都存在，说明现在已经开始安装了,那现在就是实际开始时间
            surveyInstallOrder.setInstallActStartTime(new Date());
        }

        mapper.updateById(surveyInstallOrder);
    }

    @Override
    public JSONObject explain(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderExplain req) throws Exception {
        List<ChiToKey> chiToKeys = chiToKeyMapper.selectList(new QueryWrapper<>(ChiToKey.builder().build()));
        Map<String, String> map = chiToKeys.stream().collect(Collectors.toMap(ChiToKey::getChi, ChiToKey::getEngkey, (value1, value2) -> value1));
        String qq = "客户姓名:樊卓豪\n" +
                "客户手机:13826059294\n" +
                "安装城市:广东-广州-黄埔\n" +
                "安装地址:广州市黄埔区林逸街124号保利越秀岭南林语-11栋2801房-负一层592车位\n" +
                "安装工单号:101891347409519342\n" +
                "客户ID:3569601073693392898\n" +
                "车型:理想L8 Pro\n" +
                "服务类型:客户权益\n" +
                "安装订单号:101275758337078733\n" +
                "创建人:理想用户\n" +
                "工单状态:待勘测\n" +
                "工单来源:APP\n" +
                "用户需求:7kW交流充电桩\n" +
                "安装备注:--\n" +
                "工单关键节点\n" +
                "工单创建\n" +
                "2024-05-08 11:59:29";
        String[] split = qq.split("\n");
        JSONObject job = new JSONObject();
        for (String s : split) {
            System.out.println("每行啥内容啊:" + s);

            long count = IntStream.of(s.chars().toArray())
                    .filter(ch -> ch == ':')
                    .count();


            if (count == 1) {
                String[] split1 = s.split(":");
                if (split1.length == 2) {
                    job.put(map.get(split1[0]), split1[1]);
                    try {
                        if (map.get(split1[0]).equals("state")) {
                            job.put(map.get(split1[0]), CommonEnums.SIOrderState.getIndexByName(split1[1]));
                        }
                        if (map.get(split1[0]).equals("installAllAddr")) {
                            String[] split2 = split1[1].split("-");
                            job.put("installProvince", split2[0]);
                            job.put("installCity", split2[1]);
                            job.put("installArea", split2[2]);
                        }
                    } catch (Exception e) {
                        System.out.println("啥报错啊" + s + ",啥内容：" + JSONObject.toJSONString(split1));
                    }

                }
            } else if (count == 2) {
                job.put("orderCreateTime", s);
            }


        }
        System.out.println("结果是" + job.toJSONString());
        return job;
    }


    public void changeState(LoginRedisObj loginRedisObj, ReqSurveyInstallOrderSave reqSurveyInstallOrderSave) throws Exception {
        SurveyInstallOrder surveyInstallOrder = mapper.selectById(reqSurveyInstallOrderSave.getId());
        surveyInstallOrder.setState(reqSurveyInstallOrderSave.getState());

        mapper.updateById(surveyInstallOrder);
    }

}
