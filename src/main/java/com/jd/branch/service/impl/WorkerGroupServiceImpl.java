package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqWorkerGroupSave;
import com.jd.branch.api.request.ReqWorkerGroupSearch;
import com.jd.branch.api.response.RespWorkerGroupDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ComBrand;
import com.jd.branch.entity.Worker;
import com.jd.branch.entity.WorkerGroup;
import com.jd.branch.entity.WorkerGroupBrand;
import com.jd.branch.mapper.ComBrandMapper;
import com.jd.branch.mapper.WorkerGroupBrandMapper;
import com.jd.branch.mapper.WorkerGroupMapper;
import com.jd.branch.mapper.WorkerMapper;
import com.jd.branch.service.WorkerGroupService;


import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 师傅组 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Service
public class WorkerGroupServiceImpl implements WorkerGroupService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkerGroupServiceImpl.class);
    @Autowired
    private WorkerGroupMapper mapper;

    @Autowired
    private WorkerGroupBrandMapper workerGroupBrandMapper;


    @Autowired
    private ComBrandMapper comBrandMapper;

    @Autowired
    private WorkerMapper workerMapper;

    @Override
    public PageInfo<RespWorkerGroupDetail> search(LoginRedisObj loginRedisObj, ReqWorkerGroupSearch param) {
        //1.构造查询条件
        QueryWrapper<WorkerGroup> wrapper = new QueryWrapper<>(
                WorkerGroup.builder().tenantId(param.getTenantId()).deleteFlag(0).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<WorkerGroup> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<WorkerGroup> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespWorkerGroupDetail> details = new ArrayList<>();
        for (WorkerGroup item : iPage.getRecords()) {
            RespWorkerGroupDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerGroupDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespWorkerGroupDetail> pageInfo = new PageInfo<>(
                param.getCurrentPage(),
                iPage.getTotal(),
                param.getPageSize(),
                details
        );


        return pageInfo;
    }

    @Override
    public List<RespWorkerGroupDetail> findList(LoginRedisObj loginRedisObj, ReqWorkerGroupSearch req) throws Exception {
        WorkerGroup param = JSONObject.parseObject(JSONObject.toJSONString(req), WorkerGroup.class);
        List<WorkerGroup> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespWorkerGroupDetail> details = new ArrayList<>();
        for (WorkerGroup item : list) {
            RespWorkerGroupDetail detail = DefindBeanUtil.copyProperties(item, RespWorkerGroupDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj, Long id) {
        mapper.deleteById(id);
        //要顺便把brand也删了
        workerGroupBrandMapper.delete(new QueryWrapper<>(WorkerGroupBrand.builder().workerGroupId(id).build()));
    }

    @Override

    public void updateById(LoginRedisObj loginRedisObj, ReqWorkerGroupSave item) {
        WorkerGroup update = DefindBeanUtil.copyProperties(item, WorkerGroup.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqWorkerGroupSave add) {
        List<WorkerGroup> workerGroups = mapper.selectList(new QueryWrapper<>(WorkerGroup.builder().worker1Id(add.getWorker1Id()).worker2Id(add.getWorker2Id()).deleteFlag(0).build()));
        WorkerGroup save = JSONObject.parseObject(JSONObject.toJSONString(add), WorkerGroup.class);
        if (workerGroups.size() == 0) {
            Worker worker1 = workerMapper.selectById(add.getWorker1Id());
            if (worker1 != null) {
                save.setWorker1Name(worker1.getName());
            }
            Worker worker2 = workerMapper.selectById(add.getWorker2Id());
            if (worker2 != null) {
                save.setWorker2Name(worker2.getName());
            }
            save.setTenantId(loginRedisObj.getTenantId());
            mapper.insert(save);
        } else {
            save.setId(workerGroups.get(0).getId());
        }


        //看看有多少个品牌，每个品牌都要单独弄个
        for (Long id : add.getBrand()) {
            List<WorkerGroupBrand> workerGroupBrands = workerGroupBrandMapper.selectList(new QueryWrapper<>(WorkerGroupBrand.builder().workerGroupId(save.getId()).brandId(id).deleteFlag(0).build()));
            if (workerGroupBrands.size() == 0) {
                WorkerGroupBrand workerGroupBrand = DefindBeanUtil.copyProperties(add, WorkerGroupBrand.class);
                ComBrand comBrand = comBrandMapper.selectById(id);
                if (comBrand != null) {
                    workerGroupBrand.setBrandName(comBrand.getName());
                    workerGroupBrand.setBrandId(id);
                }
                workerGroupBrand.setWorkerGroupId(save.getId());
                workerGroupBrandMapper.insert(workerGroupBrand);
            }
        }
    }

    @Override
    public RespWorkerGroupDetail detail(LoginRedisObj loginRedisObj, Long id) {
        WorkerGroup byId = mapper.selectById(id);
        RespWorkerGroupDetail resp = transferOne(byId);
        return resp;
    }

    private RespWorkerGroupDetail transferOne(WorkerGroup item) {
        RespWorkerGroupDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerGroupDetail.class);

        return resp;
    }
}
