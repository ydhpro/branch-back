package com.jd.branch.service.impl;

import cn.hutool.core.date.DateUtil;
import com.jd.branch.api.request.ReqBranchBusinessSit;
import com.jd.branch.constant.CommonEnums;
import com.jd.branch.mapper.SurveyInstallOrderMapper;
import com.jd.branch.service.ChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ChartServiceImpl implements ChartService {

    @Autowired
    SurveyInstallOrderMapper surveyInstallOrderMapper;
    @Override
    public void branchBusinessSitTotal(ReqBranchBusinessSit reqBranchBusinessSit) {
        Date today=new Date();
        Date startDate= DateUtil.offsetDay(today,-30);
        if(reqBranchBusinessSit.getDimension().equals(CommonEnums.ChartDimension.DAY.getIndex())){
            //暂定统计最近30天的
//            surveyInstallOrderMapper.selectGroupByDate()
        }else if(reqBranchBusinessSit.getDimension().equals(CommonEnums.ChartDimension.MONTH.getIndex())){

        }else if(reqBranchBusinessSit.getDimension().equals(CommonEnums.ChartDimension.SEASON.getIndex())){

        }else if(reqBranchBusinessSit.getDimension().equals(CommonEnums.ChartDimension.YEAR.getIndex())){

        }else if(reqBranchBusinessSit.getDimension().equals(CommonEnums.ChartDimension.AREA.getIndex())){

        }
    }

    @Override
    public void orderArrangeSit() {

    }
}
