package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqProLedgerItemSave;
import com.jd.branch.api.request.ReqProLedgerItemSearch;
import com.jd.branch.api.response.RespProLedgerItemDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ProLedgerItem;
import com.jd.branch.mapper.ProLedgerItemMapper;
import com.jd.branch.service.ProLedgerItemService;


import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Service
public class ProLedgerItemServiceImpl  implements ProLedgerItemService {
    private static final Logger LOGGER = LoggerFactory.getLogger( ProLedgerItemServiceImpl.class);
    @Autowired
    private ProLedgerItemMapper mapper;


    @Override
    public PageInfo<RespProLedgerItemDetail> search(LoginRedisObj loginRedisObj, ReqProLedgerItemSearch param) {
        //1.构造查询条件
        QueryWrapper<ProLedgerItem> wrapper = new QueryWrapper<>(
        ProLedgerItem.builder().tenantId(param.getTenantId()).deleteFlag(0).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<ProLedgerItem> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<ProLedgerItem> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespProLedgerItemDetail> details = new ArrayList<>();
        for(ProLedgerItem item:iPage.getRecords()){
            RespProLedgerItemDetail resp = DefindBeanUtil.copyProperties(item, RespProLedgerItemDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespProLedgerItemDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespProLedgerItemDetail> findList(LoginRedisObj loginRedisObj, ReqProLedgerItemSearch req)throws Exception{
        ProLedgerItem param = JSONObject.parseObject(JSONObject.toJSONString(req), ProLedgerItem.class);
        List<ProLedgerItem> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespProLedgerItemDetail> details = new ArrayList<>();
        for(ProLedgerItem item:list){
            RespProLedgerItemDetail detail = DefindBeanUtil.copyProperties(item, RespProLedgerItemDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqProLedgerItemSave item) {
        ProLedgerItem update = DefindBeanUtil.copyProperties(item, ProLedgerItem.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqProLedgerItemSave add){
        ProLedgerItem save = JSONObject.parseObject(JSONObject.toJSONString(add), ProLedgerItem.class);
        save.setTenantId(loginRedisObj.getTenantId());
        mapper.insert(save);
    }

    @Override
    public RespProLedgerItemDetail detail(LoginRedisObj loginRedisObj, Long id){
        ProLedgerItem byId = mapper.selectById(id);
        RespProLedgerItemDetail resp = transferOne(byId);
        return resp;
    }

    private RespProLedgerItemDetail transferOne(ProLedgerItem item){
        RespProLedgerItemDetail resp = DefindBeanUtil.copyProperties(item, RespProLedgerItemDetail.class);
        return resp;
    }
}
