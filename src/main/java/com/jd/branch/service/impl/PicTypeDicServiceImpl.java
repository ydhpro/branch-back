package com.jd.branch.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqPicType;
import com.jd.branch.api.request.ReqPicTypeDicSave;
import com.jd.branch.api.request.ReqPicTypeDicSearch;
import com.jd.branch.api.response.RespPicType;
import com.jd.branch.api.response.RespPicTypeDicDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.PicAttachment;
import com.jd.branch.entity.PicTypeDic;
import com.jd.branch.mapper.PicAttachmentMapper;
import com.jd.branch.mapper.PicTypeDicMapper;
import com.jd.branch.service.PicTypeDicService;

import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 图片类型字典 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@Service
public class PicTypeDicServiceImpl implements PicTypeDicService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PicTypeDicServiceImpl.class);
    @Autowired
    private PicTypeDicMapper mapper;

    @Autowired
    PicAttachmentMapper picAttachmentMapper;


    @Override
    public PageInfo<RespPicTypeDicDetail> search(LoginRedisObj loginRedisObj, ReqPicTypeDicSearch param) {
        //1.构造查询条件
        QueryWrapper<PicTypeDic> wrapper = new QueryWrapper<>(
                PicTypeDic.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<PicTypeDic> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<PicTypeDic> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespPicTypeDicDetail> details = new ArrayList<>();
        for (PicTypeDic item : iPage.getRecords()) {
            RespPicTypeDicDetail resp = DefindBeanUtil.copyProperties(item, RespPicTypeDicDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespPicTypeDicDetail> pageInfo = new PageInfo<>(
                param.getCurrentPage(),
                iPage.getTotal(),
                param.getPageSize(),
                details
        );


        return pageInfo;
    }

    @Override
    public List<RespPicTypeDicDetail> findList(LoginRedisObj loginRedisObj, ReqPicTypeDicSearch req) throws Exception {
        PicTypeDic param = JSONObject.parseObject(JSONObject.toJSONString(req), PicTypeDic.class);
        //需要加上通用的,比如没有指定品牌的
        List<Long> brandIds = CollUtil.newArrayList(0L);
        if (param.getBrandId() != null && 0 != param.getBrandId()) {
            brandIds.add(param.getBrandId());
            param.setBrandId(null);
        }

        List<PicTypeDic> list = mapper.selectList(new QueryWrapper<>(param).in("brand_id", brandIds));
        List<RespPicTypeDicDetail> details = new ArrayList<>();
        for (PicTypeDic item : list) {
            RespPicTypeDicDetail detail = DefindBeanUtil.copyProperties(item, RespPicTypeDicDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    public List<RespPicType> getPic(LoginRedisObj loginRedisObj, ReqPicType reqPicType) {
        List<RespPicType> retList = new ArrayList<>();
        List<PicAttachment> picAttachments = picAttachmentMapper.selectList(new QueryWrapper<>(PicAttachment.builder().relateId(reqPicType.getRelateId()).build()));
        for (PicAttachment picAttachment : picAttachments) {
            RespPicType respPicType = DefindBeanUtil.copyProperties(picAttachment, RespPicType.class);
            PicTypeDic picTypeDic = mapper.selectById(respPicType.getType());
            if (picTypeDic != null) {
                respPicType.setTypeName(picTypeDic.getName());
            }
            retList.add(respPicType);
        }
        return retList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj, Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj, ReqPicTypeDicSave item) {
        PicTypeDic update = DefindBeanUtil.copyProperties(item, PicTypeDic.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqPicTypeDicSave add) {
        PicTypeDic save = JSONObject.parseObject(JSONObject.toJSONString(add), PicTypeDic.class);
        mapper.insert(save);
    }

    @Override
    public RespPicTypeDicDetail detail(LoginRedisObj loginRedisObj, Long id) {
        PicTypeDic byId = mapper.selectById(id);
        RespPicTypeDicDetail resp = transferOne(byId);
        return resp;
    }

    private RespPicTypeDicDetail transferOne(PicTypeDic item) {
        RespPicTypeDicDetail resp = DefindBeanUtil.copyProperties(item, RespPicTypeDicDetail.class);
        return resp;
    }
}
