package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqComBrandSave;
import com.jd.branch.api.request.ReqComBrandSearch;
import com.jd.branch.api.response.RespComBrandDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ComBrand;
import com.jd.branch.mapper.ComBrandMapper;
import com.jd.branch.service.ComBrandService;

import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 合作品牌相关参数 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-08-26
 */
@Service
public class ComBrandServiceImpl  implements ComBrandService {
    private static final Logger LOGGER = LoggerFactory.getLogger( ComBrandServiceImpl.class);
    @Autowired
    private ComBrandMapper mapper;


    @Override
    public PageInfo<RespComBrandDetail> search(LoginRedisObj loginRedisObj, ReqComBrandSearch param) {
        //1.构造查询条件
        QueryWrapper<ComBrand> wrapper = new QueryWrapper<>(
        ComBrand.builder().tenantId(param.getTenantId()).deleteFlag(0).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<ComBrand> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<ComBrand> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespComBrandDetail> details = new ArrayList<>();
        for(ComBrand item:iPage.getRecords()){
            RespComBrandDetail resp = DefindBeanUtil.copyProperties(item, RespComBrandDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespComBrandDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespComBrandDetail> findList(LoginRedisObj loginRedisObj, ReqComBrandSearch req)throws Exception{
        ComBrand param = JSONObject.parseObject(JSONObject.toJSONString(req), ComBrand.class);
        List<ComBrand> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespComBrandDetail> details = new ArrayList<>();
        for(ComBrand item:list){
            RespComBrandDetail detail = DefindBeanUtil.copyProperties(item, RespComBrandDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqComBrandSave item) {
        ComBrand update = DefindBeanUtil.copyProperties(item, ComBrand.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqComBrandSave add){
        ComBrand save = JSONObject.parseObject(JSONObject.toJSONString(add), ComBrand.class);
        mapper.insert(save);
    }

    @Override
    public RespComBrandDetail detail(LoginRedisObj loginRedisObj, Long id){
        ComBrand byId = mapper.selectById(id);
        RespComBrandDetail resp = transferOne(byId);
        return resp;
    }

    private RespComBrandDetail transferOne(ComBrand item){
        RespComBrandDetail resp = DefindBeanUtil.copyProperties(item, RespComBrandDetail.class);
        return resp;
    }
}
