package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqWorkerGroupBrandSave;
import com.jd.branch.api.request.ReqWorkerGroupBrandSearch;
import com.jd.branch.api.response.RespWorkerGroupBrandDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.WorkerGroupBrand;
import com.jd.branch.mapper.WorkerGroupBrandMapper;
import com.jd.branch.service.WorkerGroupBrandService;

import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 师傅组品牌维护 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-09-07
 */
@Service
public class WorkerGroupBrandServiceImpl implements WorkerGroupBrandService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkerGroupBrandServiceImpl.class);
    @Autowired
    private WorkerGroupBrandMapper mapper;


    @Override
    public PageInfo<RespWorkerGroupBrandDetail> search(LoginRedisObj loginRedisObj, ReqWorkerGroupBrandSearch param) {
        //1.构造查询条件
        QueryWrapper<WorkerGroupBrand> wrapper = new QueryWrapper<>(
                WorkerGroupBrand.builder().workerGroupId(param.getWorkerGroupId()).tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<WorkerGroupBrand> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<WorkerGroupBrand> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespWorkerGroupBrandDetail> details = new ArrayList<>();
        for (WorkerGroupBrand item : iPage.getRecords()) {
            RespWorkerGroupBrandDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerGroupBrandDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespWorkerGroupBrandDetail> pageInfo = new PageInfo<>(
                param.getCurrentPage(),
                iPage.getTotal(),
                param.getPageSize(),
                details
        );


        return pageInfo;
    }

    @Override
    public List<RespWorkerGroupBrandDetail> findList(LoginRedisObj loginRedisObj, ReqWorkerGroupBrandSearch req) throws Exception {
        WorkerGroupBrand param = JSONObject.parseObject(JSONObject.toJSONString(req), WorkerGroupBrand.class);
        List<WorkerGroupBrand> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespWorkerGroupBrandDetail> details = new ArrayList<>();
        for (WorkerGroupBrand item : list) {
            RespWorkerGroupBrandDetail detail = DefindBeanUtil.copyProperties(item, RespWorkerGroupBrandDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    public void removeById(LoginRedisObj loginRedisObj, Long id) {
        mapper.deleteById(id);
    }

    @Override
    public void updateById(LoginRedisObj loginRedisObj, ReqWorkerGroupBrandSave item) {
        WorkerGroupBrand update = DefindBeanUtil.copyProperties(item, WorkerGroupBrand.class);
        mapper.updateById(update);
    }

    @Override
    public void saveMaster(LoginRedisObj loginRedisObj, ReqWorkerGroupBrandSave add) {
        WorkerGroupBrand save = JSONObject.parseObject(JSONObject.toJSONString(add), WorkerGroupBrand.class);
        mapper.insert(save);
    }

    @Override
    public RespWorkerGroupBrandDetail detail(LoginRedisObj loginRedisObj, Long id) {
        WorkerGroupBrand byId = mapper.selectById(id);
        RespWorkerGroupBrandDetail resp = transferOne(byId);
        return resp;
    }

    private RespWorkerGroupBrandDetail transferOne(WorkerGroupBrand item) {
        RespWorkerGroupBrandDetail resp = DefindBeanUtil.copyProperties(item, RespWorkerGroupBrandDetail.class);
        return resp;
    }
}
