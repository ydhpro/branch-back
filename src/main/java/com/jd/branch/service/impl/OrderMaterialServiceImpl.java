package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqOrderMaterialSave;
import com.jd.branch.api.request.ReqOrderMaterialSearch;
import com.jd.branch.api.response.RespOrderMaterialDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.OrderMaterial;
import com.jd.branch.mapper.OrderMaterialMapper;
import com.jd.branch.service.OrderMaterialService;

import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-04-29
 */
@Service
public class OrderMaterialServiceImpl  implements OrderMaterialService {
    private static final Logger LOGGER = LoggerFactory.getLogger( OrderMaterialServiceImpl.class);
    @Autowired
    private OrderMaterialMapper mapper;


    @Override
    public PageInfo<RespOrderMaterialDetail> search(LoginRedisObj loginRedisObj, ReqOrderMaterialSearch param) {
        //1.构造查询条件
        QueryWrapper<OrderMaterial> wrapper = new QueryWrapper<>(
        OrderMaterial.builder().tenantId(param.getTenantId()).deleteFlag(0).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<OrderMaterial> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<OrderMaterial> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespOrderMaterialDetail> details = new ArrayList<>();
        for(OrderMaterial item:iPage.getRecords()){
            RespOrderMaterialDetail resp = DefindBeanUtil.copyProperties(item, RespOrderMaterialDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespOrderMaterialDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespOrderMaterialDetail> findList(LoginRedisObj loginRedisObj, ReqOrderMaterialSearch req)throws Exception{
        OrderMaterial param = JSONObject.parseObject(JSONObject.toJSONString(req), OrderMaterial.class);
        List<OrderMaterial> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespOrderMaterialDetail> details = new ArrayList<>();
        for(OrderMaterial item:list){
            RespOrderMaterialDetail detail = DefindBeanUtil.copyProperties(item, RespOrderMaterialDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ReqOrderMaterialSave item) {
        OrderMaterial update = DefindBeanUtil.copyProperties(item, OrderMaterial.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqOrderMaterialSave add){
        OrderMaterial save = JSONObject.parseObject(JSONObject.toJSONString(add), OrderMaterial.class);
        mapper.insert(save);
    }

    @Override
    public RespOrderMaterialDetail detail(LoginRedisObj loginRedisObj, Long id){
        OrderMaterial byId = mapper.selectById(id);
        RespOrderMaterialDetail resp = transferOne(byId);
        return resp;
    }

    private RespOrderMaterialDetail transferOne(OrderMaterial item){
        RespOrderMaterialDetail resp = DefindBeanUtil.copyProperties(item, RespOrderMaterialDetail.class);
        return resp;
    }
}
