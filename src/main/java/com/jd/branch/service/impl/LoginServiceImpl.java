package com.jd.branch.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.jd.branch.api.request.ReqLogin;
import com.jd.branch.api.response.RespPersonDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.entity.Admin;
import com.jd.branch.entity.Worker;
import com.jd.branch.mapper.AdminMapper;
import com.jd.branch.mapper.WorkerMapper;
import com.jd.branch.service.LoginService;
import com.jd.branch.util.DefindUtils;
import org.apache.kafka.clients.admin.internals.AdminMetadataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import sun.misc.LRUCache;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author luoqy4
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private AdminMapper adminMapper;


    @Autowired
    private StringRedisTemplate redisTemplate;


    @Autowired
    private WorkerMapper workerMapper;


    @Override
    public LoginRedisObj loginyMobile(String phone, String openid) throws Exception {
        String token = IdUtil.simpleUUID();

        Worker worker = workerMapper.selectOne(new QueryWrapper<>(Worker.builder().phone(phone).build()));
        DefindUtils.check(worker==null, "当前人员为外部用户，如有需要，请联系管理员");

        worker.setOpenId(openid);
        workerMapper.updateById(worker);

        final LoginRedisObj obj = JSONObject.parseObject(JSONObject.toJSONString(worker),LoginRedisObj.class);
        obj.setToken(token);

        redisTemplate.opsForValue().set(LoginRedisObj.TOKEN_FIX + token, obj.toJson());


        return obj;

    }

    @Override
    public LoginRedisObj loginByOpenid(String openid,Long tenantId) throws Exception {

        Worker worker = workerMapper.selectOne(new QueryWrapper<>(Worker.builder().openId(openid).tenantId(tenantId).build()));
        DefindUtils.check(worker==null, "当前人员为外部用户，如有需要，请联系管理员");

        final LoginRedisObj obj = JSONObject.parseObject(JSONObject.toJSONString(worker),LoginRedisObj.class);


        redisTemplate.opsForValue().set(LoginRedisObj.TOKEN_FIX + openid, obj.toJson());


        return obj;

    }

    @Override
    public LoginRedisObj Login(ReqLogin reqLogin) throws Exception {

        //1.账号/手机号码/邮箱是否存在
//        User user = userMapper.selectForLogin(reqLogin.getAccount());
        List<Admin> admins = adminMapper.selectList(new QueryWrapper<>(Admin.builder().phone(reqLogin.getAccount()).password(reqLogin.getCertificate()).build()));
        DefindUtils.check(admins.size()<0&&admins.size()>1, "用户密码错误");
        DefindUtils.check(admins.size()>1, "用户密码错误");

        //成功则刷新redis等信息
        LoginRedisObj obj = null;
        obj = solveSuccess(admins.get(0), false);



        return obj;
    }

    private LoginRedisObj solveSuccess(Admin admin, boolean isForever) {
        String token = IdUtil.simpleUUID();



        final LoginRedisObj obj = JSONObject.parseObject(JSONObject.toJSONString(admin),LoginRedisObj.class);
        obj.setTenantId(admin.getTenantId());
        obj.setToken(token);

        if (isForever) {
            redisTemplate.opsForValue().set(LoginRedisObj.TOKEN_FIX + token, obj.toJson(), -1, TimeUnit.MINUTES);
            redisTemplate.opsForValue().set(LoginRedisObj.USER_FIX + admin.getId(), obj.toJson(), -1, TimeUnit.MINUTES);
        } else {
            redisTemplate.opsForValue().set(LoginRedisObj.TOKEN_FIX + token, obj.toJson(), LoginRedisObj.TOKEN_EXPIRE, TimeUnit.MINUTES);
            redisTemplate.opsForValue().set(LoginRedisObj.USER_FIX + admin.getId(), obj.toJson(), LoginRedisObj.TOKEN_EXPIRE, TimeUnit.MINUTES);
        }

        redisTemplate.delete(LoginRedisObj.ACC_ERROR_FIX + admin.getPhone());
        redisTemplate.delete(LoginRedisObj.LOCK_FIX + admin.getPhone());

        return obj;

    }

    @Override
    public RespPersonDetail detailPerson(LoginRedisObj login) throws Exception {
        Admin user = adminMapper.selectById(login.getId());
        RespPersonDetail respPersonDetail = JSONObject.parseObject(JSONObject.toJSONString(user), RespPersonDetail.class);

        return respPersonDetail;
    }


    @Override
    public void logout(LoginRedisObj obj) {
        redisTemplate.delete("branch"+LoginRedisObj.USER_FIX + obj.getId());
    }
}
