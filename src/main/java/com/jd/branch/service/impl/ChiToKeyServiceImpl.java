package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ChiToKeyApi;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.ChiToKey;
import com.jd.branch.mapper.ChiToKeyMapper;
import com.jd.branch.service.ChiToKeyService;
import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 中文对应键 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-06-03
 */
@Service
public class ChiToKeyServiceImpl  implements ChiToKeyService {
    private static final Logger LOGGER = LoggerFactory.getLogger( ChiToKeyServiceImpl.class);
    @Autowired
    private ChiToKeyMapper mapper;


    @Override
    public PageInfo<ChiToKey> search(LoginRedisObj loginRedisObj, ChiToKeyApi param) {
        //1.构造查询条件
        QueryWrapper<ChiToKey> wrapper = new QueryWrapper<>(
        ChiToKey.builder().tenantId(param.getTenantId()).deleteFlag(0).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<ChiToKey> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<ChiToKey> iPage = mapper.selectPage(page, wrapper);


        //4.适配分页对象
        PageInfo<ChiToKey> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            iPage.getRecords()
        );


        return pageInfo;
    }

    @Override
    public List<ChiToKey> findList(LoginRedisObj loginRedisObj, ChiToKey req)throws Exception{
        ChiToKey param = JSONObject.parseObject(JSONObject.toJSONString(req), ChiToKey.class);
        List<ChiToKey> list = mapper.selectList(new QueryWrapper<>(param));

        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,ChiToKey item) {
        ChiToKey update = DefindBeanUtil.copyProperties(item, ChiToKey.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ChiToKey add){
        ChiToKey save = JSONObject.parseObject(JSONObject.toJSONString(add), ChiToKey.class);
        mapper.insert(save);
    }

    @Override
    public ChiToKey detail(LoginRedisObj loginRedisObj, Long id){
        ChiToKey byId = mapper.selectById(id);
        return byId;
    }


}
