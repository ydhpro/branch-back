package com.jd.branch.service.impl;



import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.GetObjectRequest;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.Download;
import com.qcloud.cos.transfer.TransferManager;
import com.qcloud.cos.transfer.Upload;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ydh
 * @version 1.0
 * @date 2021/1/6 23:13
 */
public class TencentCOS {
//    private static TencentCOS tencentCOS = null;
//    private static COSClient cosClient;
//
//    public static synchronized TencentCOS getInstance() {
//        if (tencentCOS == null) {
//            tencentCOS = new TencentCOS();
//        }
//        return tencentCOS;
//    }
//
//    private TencentCOS(){
//                // 1 初始化用户身份信息（secretId, secretKey）。
//        String secretId = "AKIDUcwFPOFkZjesslx8vCH6q27vTr0Fm4fy";
//        String secretKey = "BFqFTSXOIwJkv5yc6N1SxEvPiU5xawnP";
//        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
//        // 2 设置 bucket 的区域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
//        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
//        Region region = new Region("ap-guangzhou");
//        ClientConfig clientConfig = new ClientConfig(region);
//        // 3 生成 cos 客户端。
//        cosClient = new COSClient(cred, clientConfig);
////        return cosClient;
//    }
//    public static synchronized COSClient initCOS() {
//        // 1 初始化用户身份信息（secretId, secretKey）。
//        String secretId = "AKIDUcwFPOFkZjesslx8vCH6q27vTr0Fm4fy";
//        String secretKey = "BFqFTSXOIwJkv5yc6N1SxEvPiU5xawnP";
//        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
//        // 2 设置 bucket 的区域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
//        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
//        Region region = new Region("ap-guangzhou");
//        ClientConfig clientConfig = new ClientConfig(region);
//        // 3 生成 cos 客户端。
//        cosClient = new COSClient(cred, clientConfig);
////        return cosClient;
//    }

    //    public static void uploadFile(String objectKey) {
//        String localFilePath = "qaq";
//        // 指定要上传的文件
//        File localFile = new File(localFilePath);
//        // 指定要上传到的存储桶
//        String bucketName = "jypx-1257310092";
//        // 指定要上传到 COS 上对象键
//        String key = objectKey;
//        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
//        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
//    }

    static String bucketName = "branch"; //桶的名称
    //这个是上传到cos上的文件名，肯定要变的啊
//    static String key = "";         //上传到云上路径
    static String region = "ap-guangzhou";//区域广州  广州
    static String appId = "1257310092"; //APPID
    static COSCredentials cred = null;
    static TransferManager transferManager = null;
    static COSClient cosClient = null;

    static {
        // 1 初始化用户身份信息(secretId, secretKey)
        //SecretId 是用于标识 API 调用者的身份
        String SecretId = "AKIDUcwFPOFkZjesslx8vCH6q27vTr0Fm4fy";
        //SecretKey是用于加密签名字符串和服务器端验证签名字符串的密钥
        String SecretKey = "BFqFTSXOIwJkv5yc6N1SxEvPiU5xawnP";
        cred = new BasicCOSCredentials(SecretId, SecretKey);

        // 2 设置bucket的区域,
        ClientConfig clientConfig = new ClientConfig(new Region(region));
        // 3 生成cos客户端
        cosClient = new COSClient(cred, clientConfig);
        // 指定要上传到 COS 上的路径
        ExecutorService threadPool = Executors.newFixedThreadPool(32);
        // 传入一个 threadpool, 若不传入线程池, 默认 TransferManager 中会生成一个单线程的线程池。
        transferManager = new TransferManager(cosClient, threadPool);
    }

    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm ss");

    /**
     * 上传
     */
    public static StringBuffer upload(MultipartFile multipartFile, String modelName) {
        final StringBuffer  fileNameReturn = new StringBuffer("");
        final boolean endflag = false;
        Thread t=new Thread(new Runnable() {
            public void run() {
                try {
                    // 获取文件名
                    String befFileName = multipartFile.getOriginalFilename();
                    // 获取文件后缀
                    String prefix = befFileName.substring(befFileName.lastIndexOf("."));
                    // 若需要防止生成的临时文件重复,可以在文件名后添加随机码
                    //完整日期加上5位随机码做文件名
                    SimpleDateFormat simpleDateFormat;
                    simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmmssSSS");
                    String date=simpleDateFormat.format(new Date());
                    Random random=new Random();
                    int rannum= (int)(random.nextDouble()*(99999-10000 + 1))+ 10000;
                    String fileName=date+rannum;

                    File file = File.createTempFile(befFileName, prefix);
                    multipartFile.transferTo(file);

                    System.out.println("上传开始时间:" + sdf.format(new Date()));
                    // .....(提交上传下载请求, 如下文所属)
                    // bucket 的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
                    String bucket = bucketName + "-" + appId;
                    //本地文件路径
//                    File localFile = new File("src/main/resources/qaq.txt");
                    PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, modelName+"/"+fileName+prefix, file);
                    // 本地文件上传
                    Upload upload = transferManager.upload(putObjectRequest);
                    // 异步（如果想等待传输结束，则调用 waitForUploadResult）
//                    UploadResult uploadResult = upload.waitForUploadResult();
                    //同步的等待上传结束waitForCompletion
                    upload.waitForCompletion();

                    System.out.println("上传结束时间:" + sdf.format(new Date()));
                    System.out.println("上传成功");
                    //获取上传成功之后文件的下载地址
                    //这个下载地址是有时间限制的一个小时有效期
                    //所以下面自己拼一个下载地址
//            URL url = cosClient.generatePresignedUrl(bucketName + "-" + appId, fileName, new Date(new Date().getTime() + 5 * 60 * 10000));
                    //返回一个永久的名称
//            return "https://"+bucketName + "-" + appId+".cos."+region+".myqcloud.com/"+fileName;
                    fileNameReturn .append("https://" + bucketName + "-" + appId + ".cos." + region + ".myqcloud.com/"+modelName+"/" + fileName+prefix) ;
                    System.out.println("上传成功"+fileNameReturn.toString());
                } catch (Throwable tb) {
                    System.out.println("上传失败");
                    tb.printStackTrace();
//            return null;
                } finally {
                    // 关闭 TransferManger
//                    transferManager.shutdownNow();
                }
            }
        });
        t.start();
        while(t.isAlive()==true){
            //这里搞个循环把他兜住不让他往下走
//            System.out.println("判断成功"+fileNameReturn.toString());

//            return fileNameReturn;
        }
        System.out.println("直接返回"+fileNameReturn.toString());
        return fileNameReturn;
//                .start();
//        System.out.println("上传wancheng"+fileNameReturn.toString());
    }

    /**
     * 下载
     */
    public static void download(String fileName) {
        try {
            //下载到本地指定路径
            File localDownFile = new File("src/main/resources/download.pdf");
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName + "-" + appId, fileName);
            // 下载文件
            Download download = transferManager.download(getObjectRequest, localDownFile);
            // 等待传输结束（如果想同步的等待上传结束，则调用 waitForCompletion）
            download.waitForCompletion();
            System.out.println("下载成功");
        } catch (Throwable tb) {
            System.out.println("下载失败");
            tb.printStackTrace();
        } finally {
            // 关闭 TransferManger
            transferManager.shutdownNow();
        }
    }

    /**
     * 删除
     */
    public static void delete(String fileName) {

        // 指定要删除的 bucket 和路径
        try {
            cosClient.deleteObject(bucketName + "-" + appId, "branch/"+fileName);
//            cosClient.deleteObject();
            System.out.println("删除成功");
        } catch (Throwable tb) {
            System.out.println("删除文件失败");
            tb.printStackTrace();
        }

    }


}
