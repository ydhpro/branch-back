package com.jd.branch.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.ReqAdminSave;
import com.jd.branch.api.request.ReqAdminSearch;
import com.jd.branch.api.response.RespAdminDetail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Admin;
import com.jd.branch.mapper.AdminMapper;
import com.jd.branch.service.AdminService;


import com.jd.branch.util.DefindBeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 管理员 服务实现类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Service
public class AdminServiceImpl  implements AdminService {
    private static final Logger LOGGER = LoggerFactory.getLogger( AdminServiceImpl.class);
    @Autowired
    private AdminMapper mapper;


    @Override
    public PageInfo<RespAdminDetail> search(LoginRedisObj loginRedisObj, ReqAdminSearch param) {
        //1.构造查询条件
        QueryWrapper<Admin> wrapper = new QueryWrapper<>(
        Admin.builder().tenantId(param.getTenantId()).deleteFlag(0).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<Admin> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<Admin> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<RespAdminDetail> details = new ArrayList<>();
        for(Admin item:iPage.getRecords()){
            RespAdminDetail resp = DefindBeanUtil.copyProperties(item, RespAdminDetail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<RespAdminDetail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<RespAdminDetail> findList(LoginRedisObj loginRedisObj, ReqAdminSearch req)throws Exception{
        Admin param = JSONObject.parseObject(JSONObject.toJSONString(req), Admin.class);
        List<Admin> list = mapper.selectList(new QueryWrapper<>(param));
        List<RespAdminDetail> details = new ArrayList<>();
        for(Admin item:list){
            RespAdminDetail detail = DefindBeanUtil.copyProperties(item, RespAdminDetail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj, ReqAdminSave item) {
        Admin update = DefindBeanUtil.copyProperties(item, Admin.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, ReqAdminSave add){
        Admin save = JSONObject.parseObject(JSONObject.toJSONString(add), Admin.class);
        save.setTenantId(loginRedisObj.getTenantId());
        mapper.insert(save);
    }

    @Override
    public RespAdminDetail detail(LoginRedisObj loginRedisObj, Long id){
        Admin byId = mapper.selectById(id);
        RespAdminDetail resp = transferOne(byId);
        return resp;
    }

    private RespAdminDetail transferOne(Admin item){
        RespAdminDetail resp = DefindBeanUtil.copyProperties(item, RespAdminDetail.class);
        return resp;
    }
}
