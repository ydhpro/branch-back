package com.jd.branch.service;


import com.jd.branch.api.request.ReqLogin;
import com.jd.branch.api.response.RespPersonDetail;
import com.jd.branch.common.LoginRedisObj;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ydh
 * @since 2023-04-02
 */
public interface LoginService {
    LoginRedisObj loginyMobile(String mobile, String openid) throws Exception;

    LoginRedisObj loginByOpenid(String openid, Long tenantId) throws Exception;

    /**
     * 登录
     * @param reqLogin
     */
    LoginRedisObj Login(ReqLogin reqLogin) throws Exception;

    /**
     * 获取当前用户信息
     * @param login
     * @return
     * @throws Exception
     */
    RespPersonDetail detailPerson(LoginRedisObj login) throws Exception;

    /**
     * 退出当前登录
     *
     * @param obj
     */
    void logout(LoginRedisObj obj);
}
