package com.jd.branch.service;


import com.jd.branch.api.request.ReqAdminSave;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.api.request.ReqAdminSearch;
import com.jd.branch.api.response.RespAdminDetail;
import java.util.List;


/**
 * <p>
 * 管理员 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
public interface AdminService {
    PageInfo<RespAdminDetail> search(LoginRedisObj loginRedisObj, ReqAdminSearch reqAdminSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqAdminSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqAdminSave save) throws Exception;

    RespAdminDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespAdminDetail> findList(LoginRedisObj loginRedisObj, ReqAdminSearch item) throws Exception;
}