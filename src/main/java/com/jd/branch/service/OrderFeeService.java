package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.api.request.ReqOrderFeeSave;
import com.jd.branch.api.request.ReqOrderFeeSearch;
import com.jd.branch.api.response.RespOrderFeeDetail;
import java.util.List;


/**
 * <p>
 * 单据费用 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-09-02
 */
public interface OrderFeeService {
    PageInfo<RespOrderFeeDetail> search(LoginRedisObj loginRedisObj,ReqOrderFeeSearch reqOrderFeeSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqOrderFeeSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqOrderFeeSave save) throws Exception;

    RespOrderFeeDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespOrderFeeDetail> findList(LoginRedisObj loginRedisObj, ReqOrderFeeSearch item) throws Exception;
}