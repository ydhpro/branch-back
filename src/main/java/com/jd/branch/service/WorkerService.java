package com.jd.branch.service;


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.Worker;
import com.jd.branch.api.request.ReqWorkerSave;
import com.jd.branch.api.request.ReqWorkerSearch;
import com.jd.branch.api.response.RespWorkerDetail;
import java.util.List;


/**
 * <p>
 * 师傅信息 服务类
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
public interface WorkerService {
    PageInfo<RespWorkerDetail> search(LoginRedisObj loginRedisObj,ReqWorkerSearch reqWorkerSearch) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, ReqWorkerSave update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, ReqWorkerSave save) throws Exception;

    RespWorkerDetail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<RespWorkerDetail> findList(LoginRedisObj loginRedisObj, ReqWorkerSearch item) throws Exception;
}