package com.jd.branch.common;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginRedisObj {

    private Long id;

    private String name;

    private Long tenantId;


    private String phone;

    private String openId;


    private Integer isSystem;

    private String authorization;

    private String token;


    public String toJson() {
        return JSONObject.toJSONString(this, true);
    }

    /**
     * AES 秘钥
     */
    public static final String TENANT_PREFIX = "tenant:";

    /**
     * 连续错误最大次数前缀
     */
    public static final String ACC_ERROR_FIX = "error:";

    /**
     * 锁定：是否被锁定
     */
    public static final String LOCK_FIX = "lock:";
    /**
     * 锁定：最大锁定时间
     */
    public static final Integer LOCK_TIME_MAX = 10;
    /**
     * 锁定：连续错误最大次数需要锁定
     */
    public static final Integer LOCK_MAX = 20;

    /**
     * ：验证码出现最大次数
     */
    public static final Integer CAPTCHA_MAX = 6;


    /**
     * TOKEN
     */
    public static final String TOKEN_FIX = "branch-token:";
    public static final String USER_FIX = "branch-user:";


    /**
     * TOKEN 过期时间
     */
    public static final Integer TOKEN_EXPIRE = 120;



    public static LoginRedisObj getLoginObj(HttpServletRequest http) throws Exception {
        String authorization = http.getHeader("authorization");
        StringRedisTemplate stringRedisTemplate = SpringUtil.getBean("stringRedisTemplate");
        String jsonObj = stringRedisTemplate.opsForValue().get(TOKEN_FIX + authorization);
        LoginRedisObj obj = JSONObject.parseObject(jsonObj, new TypeReference<LoginRedisObj>() {
        });
        return obj;
    }

    public static LoginRedisObj getLoginObj(Long tenantId) throws Exception {
        LoginRedisObj obj = new LoginRedisObj();
        obj.setTenantId(tenantId);
        return obj;
    }
}
