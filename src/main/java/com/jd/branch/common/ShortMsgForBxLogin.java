package com.jd.branch.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yuandh5
 * @date 2021/11/15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShortMsgForBxLogin {
    private String appid;

    private String project;

    private String signature;

    private String to;

    private ShortMsgForBxLoginItem vars;
}
