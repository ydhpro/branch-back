package com.jd.branch.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jd.branch.api.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@ApiModel(value = "分页信息")
public class PageInfo<T> {
    @ApiModelProperty(value = "当前需要展示的页面，默认为1")
    private Integer currentPage = 1;

    @ApiModelProperty(value = "查询一共得到多少页")
    private Integer pages = 0;

	@ApiModelProperty(value = "查询的得到的总记录数")
	private Integer total = 0;

    @ApiModelProperty(value = "当前页的数据")
    private List<T> list=new ArrayList<>();

    @JsonIgnore
	private Map<String, Object> conditions;

	@JsonIgnore
	private int fromRecord;

	@JsonIgnore
	private int toRecord;

    public PageInfo(List<T> list) {
        this.list = list;
    }
	
	public PageInfo(Integer currentPage, Long total, Integer pages, List<T> list) {
		super();
		this.currentPage = currentPage;
		this.total=total.intValue();
		this.pages = pages;
		this.list=list;
	}

	public PageInfo(Integer currentPage, Integer total, Integer pages, List<T> list) {
		super();
		this.currentPage = currentPage;
		this.total=total;
		this.pages = pages;
		this.list=list;
	}

	/**
	 * 用於自定sql語句
	 * @param currentPage
	 * @param pages
	 * @param conditions
	 */
	public PageInfo(Integer currentPage, Integer pages, Map<String, Object> conditions) {
		super();
		this.currentPage = currentPage;
		this.pages = pages;
		this.conditions=conditions;
		toPage();
	}

	public PageInfo(Integer currentPage, Integer pages) {
		super();
		this.currentPage = currentPage;
		this.pages = pages;
		this.conditions=conditions;
		toPage();
	}

	public PageInfo(PageRequest rageRequest) {
		super();
		this.currentPage = rageRequest.getCurrentPage();
		this.pages = rageRequest.getPageSize();
		this.conditions=conditions;
		toPage();
	}

	public PageInfo() {
	}

	public void toPage() {
		this.setTotal((this.getTotal() / this.getPages()) + (this.getTotal() % this.getPages() > 0 ? 1 : 0));
		this.setFromRecord(this.getPages() * this.getCurrentPage() - this.getPages());
		this.setToRecord(this.getPages());
	}
    
}