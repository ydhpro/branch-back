package com.jd.branch.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luoqy4
 */
@Data
@NoArgsConstructor
@ApiModel(value = "返回信息实体类")
public class Message<T> {

    public static final int CODE_100=100;
    public static final int CODE_101=101;
    public static final int CODE_102=102;


	public static Message<String> success=new Message<>();

    @ApiModelProperty(value = "返回状态码")
    private Integer code = CODE_100;

    @ApiModelProperty(value = "返回信息")
    private String message = "success";
    
    private String messageDetail = "";
    
    @ApiModelProperty(value = "返回的数据，是一个泛型，无具体类型，根据实际判断")
    private T data = null;

    public Message(T t){
        this.data = t;
    }
    public Message(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public Message(Integer code, String message, T data){
        this.code = code;
        this.message = message;
        this.data = data;
    }
    
    public static Message<String> getSuccess(){
        return success; 
    }
    
    public Message(Integer code, String message, String messageDetail){
        this.code = code;
        this.message = message;
        this.messageDetail=messageDetail;
    }

    public static boolean isSuccess(Message message){
        return message.getCode().intValue() == 100;
    }




}