package com.jd.branch.common;

import cn.hutool.core.util.StrUtil;
import com.jd.branch.common.LoginRedisObj;

import javax.servlet.http.HttpServletRequest;


public class BaseController {

	/**
	 * 获取登录用户信息
	 * @return
	 * @throws Exception
	 */
	public LoginRedisObj getLoginObj(HttpServletRequest http) throws Exception {
		String authorization = http.getHeader("Authorization");

		if (StrUtil.isNotEmpty(authorization)) {
			LoginRedisObj obj = LoginRedisObj.getLoginObj(http);
			return obj;
		}

		LoginRedisObj sys = LoginRedisObj.builder().id(1L).name("sys").build();
		return sys;
	}

	
}
