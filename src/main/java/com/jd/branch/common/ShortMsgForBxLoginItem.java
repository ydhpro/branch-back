package com.jd.branch.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yuandh5
 * @date 2021/11/15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShortMsgForBxLoginItem {
    private String code;
}
