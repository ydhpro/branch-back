package com.jd.branch.common;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author luoqy4
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class ReqCommDel {

    @ApiModelProperty(value = "ID",example = "ID", required = true)
    @NotNull(message="id can be null")
	private Long id;

    public String toJsonStr() {
        return JSON.toJSONString(this);
    }
	
}