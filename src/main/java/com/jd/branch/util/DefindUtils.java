package com.jd.branch.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.jd.branch.exception.BizException;
import com.jd.branch.exception.BizReturnException;


import java.util.function.Supplier;

/**
 * @author luoqy4
 */
public class DefindUtils {


    public static <X extends Object> X getCheck(Boolean result, Exception biz, Supplier<? extends X> exceptionSupplier) throws Exception {
        if (result) {
            return exceptionSupplier.get();
        }
        throw biz;
    }

    public static <X extends Object> X getIfPresent(Object value, Supplier<? extends X> exceptionSupplier) {
        if (ObjectUtil.isNotEmpty(value)) {
            return exceptionSupplier.get();
        }
        return null;
    }


    /**
     * 自定义判断
     *
     * @param <X>
     * @param result
     * @param exceptionSupplier
     * @throws X
     */
    public static <X extends Throwable> void check(Boolean result, Supplier<? extends X> exceptionSupplier) throws X {
        if (result) {
            throw exceptionSupplier.get();
        }
    }

    /**
     * 自定义判断
     *
     * @param result
     * @param code
     * @throws Exception
     */
    public static void check(Boolean result, String code) throws Exception {
        if (result) {
            throw new BizException(code);
        }
    }

    /**
     * 自定义判断并返回数据
     *
     * @param result
     * @param code
     * @throws Exception
     */
    public static void checkReturn(Boolean result, String code,Object object) throws Exception {
        if (result) {
            throw new BizReturnException(code,object);
        }
    }


    /**
     * 判断对象是否为空
     *
     * @param <X>
     * @param value
     * @param exceptionSupplier
     * @throws X
     */
    public static <X extends Throwable> void checkObjNull(Object value, Supplier<? extends X> exceptionSupplier) throws X {
        if (ObjectUtil.isEmpty(value)) {
            throw exceptionSupplier.get();
        }
    }

    /**
     * @param <X>
     * @param value
     * @param code
     * @throws X
     * @throws Exception
     */
    public static <X extends Throwable> void checkObjNull(Object value, String code) throws X, Exception {
        if (ObjectUtil.isEmpty(value)) {
            throw new BizException(code);
        }
    }


    /**
     * 检查对象是否已经存在
     *
     * @param <X>
     * @param value
     * @param exceptionSupplier
     * @throws X
     */
    public static <X extends Throwable> void checkObjExist(Object value, Supplier<? extends X> exceptionSupplier) throws X {
        if (ObjectUtil.isNotEmpty(value)) {
            throw exceptionSupplier.get();
        }
    }

    /**
     * @param <X>
     * @param value
     * @param code
     * @throws X
     * @throws BizException
     */
    public static <X extends Throwable> void checkObjExist(Object value, String code) throws X, BizException {
        if (ObjectUtil.isNotEmpty(value)) {
            throw new BizException(code);
        }
    }


    /**
     * 检查字符串是否为空
     *
     * @param <X>
     * @param value
     * @param exceptionSupplier
     * @throws X
     */
    public static <X extends Throwable> void checkStrEmpty(String value, Supplier<? extends X> exceptionSupplier) throws X {
        if (StrUtil.isEmpty(value)) {
            throw exceptionSupplier.get();
        }
    }


}
