package com.jd.branch.util;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;


@Data
@Component
public class I18nUtil {
	
    @Autowired
    private HttpServletRequest request;
	
	private static MessageSource messageSource;
	
	
	static {		
        ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        //国际化报错信息参数文件地址
        reloadableResourceBundleMessageSource.setBasename("i18n/global");
        reloadableResourceBundleMessageSource.setDefaultEncoding("UTF-8");
        reloadableResourceBundleMessageSource.setCacheMillis(-1);
        reloadableResourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
        messageSource=reloadableResourceBundleMessageSource;       
	}
    	
		
    public String getI18nMessage(String code, String... params) {

        String language = request.getHeader("Accept-Language");

        Locale locale = null;
        if (language == null) {
            locale = request.getLocale();
        } else if ("en".equals(language)) {
            locale = Locale.ENGLISH;
        } else if ("zh-CN".equals(language)) {
            locale = Locale.CHINA;
        }else {
        	locale = Locale.CHINA;
        }

        String result = messageSource.getMessage(code, params, locale);

        return result;
    }
  
}
