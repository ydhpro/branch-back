package com.jd.branch.util;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * BeanUtil 对象属性复制
 *

 *  支持单个对象属性复制、列表对象的属性复制
 */
public class DefindBeanUtil {

    private static Logger logger = LoggerFactory.getLogger(DefindBeanUtil.class);

    /**
     * 单个对象属性复制
     *
     * @param source 复制源
     * @param clazz  目标对象class
     * @param <T>    目标对象类型
     * @param <M>    源对象类型
     * @return 目标对象
     */
    public static <T, M> T copyProperties(M source, Class<T> clazz) {
        if (Objects.isNull(source) || Objects.isNull(clazz)){
            return null;
        }
        T t = null;
        try {
            t = clazz.newInstance();
            BeanUtils.copyProperties(source, t);
        } catch (Exception e) {
            logger.error("",e);
            return null;
        }
        return t;
    }

    /**
     * 列表对象属性复制
     *
     * @param sources 源对象列表
     * @param clazz   目标对象class
     * @param <T>     目标对象类型
     * @param <M>     源对象类型
     * @return 目标对象列表
     */
    public static <T, M> List<T> copyObjects(List<M> sources, Class<T> clazz) {
        if (Objects.isNull(sources) || Objects.isNull(clazz)){
            return null;
        }
        return Optional.of(sources)
                .orElse(Lists.newArrayList())
                .stream().map(m -> copyProperties(m, clazz))
                .collect(Collectors.toList());
    }
}
