package com.jd.branch.config;

import cn.hutool.core.util.StrUtil;
import com.jd.branch.common.Message;

import com.jd.branch.exception.BizException;
import com.jd.branch.exception.BizReturnException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author luoqy4
 */
@Slf4j
@ControllerAdvice(basePackages= {"com.jd.yb.controller"})
public class ExceptionHandle {

    /**
     * @ModelAttribute
     * form
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    @ResponseBody
    public Message bindException(BindException ex) {
        BindingResult result=ex.getBindingResult();
        List<String> messList=new ArrayList<String>();
        if(result.hasErrors()){
            List<FieldError> fieldErrors = result.getFieldErrors();
            for (FieldError fileError : fieldErrors) {
                messList.add(StrUtil.concat(true,fileError.getField(),":",fileError.getDefaultMessage()));
            }
        }
        String message=messList.stream().collect(Collectors.joining(","));
        log.error(ex.getMessage(),ex);
        return new Message(Message.CODE_101,message,message);
    }

    /**
     * request body
     * @param httpServletRequest
     * @param ex
     * @return
     * @throws Exception
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Message setMethodArgumentNotValidException(HttpServletRequest httpServletRequest,
                                                      MethodArgumentNotValidException ex) throws Exception {
    	BindingResult result=ex.getBindingResult();
    	List<String> messList=new ArrayList<String>();
        if(result.hasErrors()){
            List<FieldError> fieldErrors = result.getFieldErrors();
            for (FieldError fileError : fieldErrors) {
                messList.add(StrUtil.concat(true,fileError.getField(),":",fileError.getDefaultMessage()));
            }
        }
        String message=messList.stream().collect(Collectors.joining(","));
        log.error(ex.getMessage(),ex);
        return new Message(Message.CODE_101,message,message);
    }

    @ExceptionHandler(BizException.class)
    @ResponseBody
    public Message BizException(BizException e){
        log.info("server error:{}",e);
        return new Message(Message.CODE_101,e.getMessage());
    }

    @ExceptionHandler(BizReturnException.class)
    @ResponseBody
    public Message BizReturnException(BizReturnException e){
        log.info("server error:{}",e);
        return new Message(Message.CODE_102,e.getMessage(),e.getData());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Message allException(Exception e){
        log.error("server error:{}",e);
        return new Message(Message.CODE_101,"server error",e.getMessage());
    }
    

    
}
