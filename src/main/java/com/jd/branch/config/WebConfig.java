package com.jd.branch.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;  
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;  
  
@Configuration  
public class WebConfig implements WebMvcConfigurer {  
  
    @Override  
    public void addCorsMappings(CorsRegistry registry) {  
        registry.addMapping("/**") // 匹配所有路径  
                .allowedOrigins("*") // 允许特定的域名进行跨域请求
                .allowedMethods("GET", "POST", "PUT", "DELETE") // 允许的请求方法  
                .allowedHeaders("*") // 允许的头信息  
                .allowCredentials(true); // 是否发送 cookie  
    }  
}