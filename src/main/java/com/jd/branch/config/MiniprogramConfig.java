package com.jd.branch.config;

import com.github.wxpay.sdk.WXPayConfig;

import org.apache.poi.util.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;


/**
 * @author yuandh5
 * @date 2021/11/23
 */
public class MiniprogramConfig implements WXPayConfig {


    private byte[] certData;

    public MiniprogramConfig() throws Exception {
        InputStream certStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("apiclient_cert.p12");
        this.certData = IOUtils.toByteArray(certStream);
        certStream.close();
    }

    @Override
    public String getAppID() { //小程序appid
        return "wx2787e8a76d0de730";
    }
    @Override
    public String getMchID() {//商户号
        return "1645764760";
    }
    @Override
    public String getKey() {
        /** 商户平台-账户中心-API安全中的密钥 */
        return "LongYing20232023LongYing20232023";
    }
    @Override
    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 10000;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 10000;
    }

}
