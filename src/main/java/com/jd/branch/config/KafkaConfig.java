package com.jd.branch.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;

/**
 * @author yuandh5
 * @date 2023/5/18
 */
@Configuration
public class KafkaConfig {
    Logger logger = LoggerFactory.getLogger(KafkaConfig.class);
    private final String error_topic="error_topic";

    @Bean
    public ConcurrentKafkaListenerContainerFactory listenerContainerFactory(ConsumerFactory consumerFactory, KafkaTemplate<String,Object> template) {
        ConcurrentKafkaListenerContainerFactory factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(consumerFactory);
        // 最大重试次数5次，每次间隔5s(该配置需要参考 Broker.leader 切换的平均时间去设置)
        SeekToCurrentErrorHandler seekToCurrentErrorHandler = new SeekToCurrentErrorHandler((consumerRecord, e) -> {
            logger.error("重试机制后异常，consumerRecord：{}", consumerRecord.toString(), e);
            //做其他业务操作，如记录异常信息到表，发送信息到其他的队列人工核对处理等
            template.send(error_topic,consumerRecord.toString());
        }, 5);

//       批量获取消息的时候，使用该方式
//        SeekToCurrentBatchErrorHandler batchErrorHandler = new SeekToCurrentBatchErrorHandler();

        factory.setErrorHandler(seekToCurrentErrorHandler);
        //设置提交偏移量的方式 ,否则出现异常的时候, 会报错No Acknowledgment available as an argument, the listener container must have a MANUAL AckMode to populate the Acknowledgment.
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL_IMMEDIATE);
        return factory;
    }

}
