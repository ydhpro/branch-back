package com.jd.branch.api.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 库存流水
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespInventoryStreamDetail implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;



    /**
     * 流水数量
     */
    @PropertyName(name = "流水数量")
    private BigDecimal streamNum;



    private String materialName;



    private Long materialId;



    /**
     * 源头id，如果主库从外部进货，则id为null
     */
    @PropertyName(name = "源头id，如果主库从外部进货，则id为null")
    private Long sourceId;



    private String sourceName;



    private BigDecimal sourceCurrentNum;



    private BigDecimal sourceAftNum;



    /**
     * 目标id，如果是安装消耗，则没有id，主库外部进货，为主库id：0
     */
    @PropertyName(name = "目标id，如果是安装消耗，则没有id，主库外部进货，为主库id：0")
    private Long targetId;



    private String targetName;



    private BigDecimal targetCurrentNum;



    private BigDecimal targetAftNum;



    /**
     * 通过备注区分是安装消耗还是报废
     */
    @PropertyName(name = "通过备注区分是安装消耗还是报废")
    private String remark;



    private Long tenantId;



    private Date createTime;



    @TableLogic
    private Integer deleteFlag;



}
