package com.jd.branch.api.response;

import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 增项费用
 * </p>
 *
 * @author ydh
 * @since 2024-04-08
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespBusinessSitDetail implements Serializable {


    private String dimension;

    private Double income;

    private Double expend;

}
