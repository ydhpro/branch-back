package com.jd.branch.api.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 安装工单
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespInstallOrderDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    private String customerName;

    

    private String customerPhone;

    

    private String installProvince;

    

    private String installCity;

    

    private String installArea;

    

    private String installAddr;

    

    /**
     * 安装工单号
     */
     @PropertyName(name = "安装工单号")
    private String installOrderNum;

    

    /**
     * 平台工单号
     */
     @PropertyName(name = "平台工单号")
    private String platformOrderNum;

    

    /**
     * 车型
     */
     @PropertyName(name = "车型")
    private String carType;

    

    /**
     * 充电桩照片
     */
     @PropertyName(name = "充电桩照片")
    private String stationPhoto;

    

    /**
     * 创建时间（入库时间）
     */
     @PropertyName(name = "创建时间（入库时间）")
    private Date createTime;

    

    /**
     * 工单接收时间（从平台/来源接收的时间）平台的派单时间
     */
     @PropertyName(name = "工单接收时间（从平台/来源接收的时间）平台的派单时间")
    private Date reviceTime;

    

    /**
     * 派发给网点师傅时间
     */
     @PropertyName(name = "派发给网点师傅时间")
    private Date distributeTime;

    

    /**
     * 完成时间
     */
     @PropertyName(name = "完成时间")
    private Date finishTime;

    

    private Long installWorker1Id;

    

    private Long installWorker2Id;

    

    private String installWorker1Name;

    

    private String installWorker2Name;

    

    private Long tenantId;

    


}
