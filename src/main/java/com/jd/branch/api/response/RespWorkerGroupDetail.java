package com.jd.branch.api.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 师傅组
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespWorkerGroupDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 默认将两个师傅名称拼起来
     */
    @PropertyName(name = "默认将两个师傅名称拼起来")
    private String name;


    private Long worker1Id;


    private Long worker2Id;


    private Long tenantId;


    private String brand;

    private String area;

    //    合作方式：1全包，2半包
    private Integer comType;

    private String worker1Name;

    private String worker2Name;


}
