package com.jd.branch.api.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 合作品牌相关参数
 * </p>
 *
 * @author ydh
 * @since 2024-08-20
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespComBrandDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    private String name;


    /**
     * 上游结算价（主机厂结算给网点）
     */
    @PropertyName(name = "上游结算价（主机厂结算给网点）")
    private BigDecimal upSettlePrice;


    /**
     * 所属区域名称
     */
    @PropertyName(name = "所属区域名称")
    private String belongAreaName;


    /**
     * 套包米数
     */
    @PropertyName(name = "套包米数")
    private BigDecimal packMeter;


    /**
     * 电缆使用品牌
     */
    @PropertyName(name = "电缆使用品牌")
    private String cableMaterialBrand;


    /**
     * 漏保品牌
     */
    @PropertyName(name = "漏保品牌")
    private String protectBrand;


    @TableLogic
    private Integer deleteFlag;


    private Long tenantId;
    private BigDecimal allPackOrderPrice;
    private BigDecimal halfPackOrderPrice;

}
