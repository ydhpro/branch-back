package com.jd.branch.api.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author luoqy4
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RespPersonDetail {

    @ApiModelProperty(value = "主键", example = "主键")
    private Long id;

    @ApiModelProperty(value = "租户主键", example = "租户主键")
    private Long tenantId;

    @ApiModelProperty(value = "租户编码", example = "租户编码")
    private String tenantCode;

    @ApiModelProperty(value = "组织主键", example = "组织主键主键")
    private Long sysOrgId;

    @ApiModelProperty(value = "组织名字", example = "组织名字")
    private String sysOrgName;

    @ApiModelProperty(value = "组织类型:1:内部-生产区域,2:内部-产线,3:内部-车间,4:外部-制造商,5:外部-代理商,6:外部-运维商,7:外部-设备用户", example = "组织类型:1:内部-生产区域,2:内部-产线,3:内部-车间,4:外部-制造商,5:外部-代理商,6:外部-运维商,7:外部-设备用户")
    private Integer sysOrgType;

    @ApiModelProperty(value = "组织区域:1:华东地区2:华西地区3:华南地区,4:华北地区,5:华中地区,6:其他", example = "组织区域:1:华东地区2:华西地区3:华南地区,4:华北地区,5:华中地区,6:其他")
    private Integer sysOrgArea;

    @ApiModelProperty(value = "用户账号", example = "用户账号")
    private String account;

    @ApiModelProperty(value = "用户名", example = "用户名")
    private String userName;

    @ApiModelProperty(value = "手机号码", example = "手机号码")
    private String phone;

    @ApiModelProperty(value = "手机号码验证状态,0:未验证,1:已经验证", example = "手机号码验证状态,0:未验证,1:已经验证")
    private Integer phoneAct;

    @ApiModelProperty(value = "邮箱", example = "邮箱")
    private String email;

    @ApiModelProperty(value = "邮箱验证状态,0:未验证,1:已经验证", example = "邮箱验证状态,0:未验证,1:已经验证")
    private Integer emailAct;

    @ApiModelProperty(value = "头像", example = "头像")
    private String photo;

    @ApiModelProperty(value = "性别,0:男,1:女", example = "性别,0:男,1:女")
    private Integer sex;

    @ApiModelProperty(value = "生日", example = "生日")
    private String birthday;

    @ApiModelProperty(value = "地址", example = "地址")
    private String address;

    @ApiModelProperty(value = "是否管理员,0:否,1:是", example = "是否管理员,0:否,1:是")
    private Integer isSystem;

    @ApiModelProperty(value = "用户状态,0:禁用,1:启用", example = "用户状态,0:禁用,1:启用")
    private Integer state;

    @ApiModelProperty(value = "来源,1:后台添加,2:前台注册", example = "来源,1:后台添加,2:前台注册")
    private Integer source;

    @ApiModelProperty(value = "最后更新时间", example = "最后更新时间")
    private Date updateTime;

    private String roles;



}
