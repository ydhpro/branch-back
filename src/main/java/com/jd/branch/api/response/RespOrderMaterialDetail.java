package com.jd.branch.api.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author ydh
 * @since 2024-04-29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespOrderMaterialDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    private String materialName;

    private Long tenantId;


    private Long materialId;

    

    private BigDecimal num;

    

    private Long orderId;

    


}
