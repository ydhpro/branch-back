package com.jd.branch.api.response;

import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 预约信息模板
 * </p>
 *
 * @author ydh
 * @since 2024-03-25
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespAppointModelDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    private Long id;

    

    /**
     * 适用渠道
     */
     @PropertyName(name = "适用渠道")
    private String source;

    

    /**
     * 租户id
     */
     @PropertyName(name = "租户id")
    private Long tenantId;

    

    private String modelText;

    


}
