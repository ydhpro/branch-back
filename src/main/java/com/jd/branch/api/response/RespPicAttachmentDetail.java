package com.jd.branch.api.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 图片附件
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespPicAttachmentDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    private String picUrl;


    /**
     * 关联类型：1车位，2走线，3人车位图，4电源点，5勘测单，6电源点参数
     */
    @PropertyName(name = "关联类型：1车位，2走线，3人车位图，4电源点，5勘测单，6电源点参数")
    private Long type;


    /**
     * 关联主题id
     */
    @PropertyName(name = "关联主题id")
    private Long relateId;


    private Long tenantId;


    @PropertyName(name = "图片评论")
    private String comment;

    @PropertyName(name = "是否审核通过（只有部分图片需要）")
    private Integer isAudited;


}
