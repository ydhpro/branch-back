package com.jd.branch.api.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 增项类目
 * </p>
 *
 * @author ydh
 * @since 2024-09-05
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RespAddCategoryDetail implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    private String name;


    private BigDecimal settlePrice;


    private String unit;


    private Integer deleteFlag;


    private Long tenantId;


    private Long brandId;


    private String brandName;


}
