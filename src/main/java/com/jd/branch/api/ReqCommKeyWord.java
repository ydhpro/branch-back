package com.jd.branch.api;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luoqy4
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class ReqCommKeyWord extends PageRequest{

    @ApiModelProperty(value = "关键字", example = "关键字", required = false)
    private String keyWord;

    public String toJsonStr() {
        return JSON.toJSONString(this);
    }

}