package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.api.PageRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author ydh
 * @since 2024-04-29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqOrderMaterialSearch extends PageRequest {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    private String materialName;

    

    private Long materialId;

    

    private BigDecimal num;

    

    private Long orderId;

    private Long tenantId;



}
