package com.jd.branch.api.request;

import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 增项费用
 * </p>
 *
 * @author ydh
 * @since 2024-04-08
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqAddExpBatchSave implements Serializable {

    /**
     * 关联单据号
     */
     @PropertyName(name = "关联单据号")
    private Long orderId;


    private List<ReqAddExpSave> itemList;

}
