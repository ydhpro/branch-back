package com.jd.branch.api.request;

import com.jd.branch.api.PageRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 中文对应键
 * </p>
 *
 * @author ydh
 * @since 2024-06-03
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ChiToKeyApi extends PageRequest {


    private static final long serialVersionUID = 1L;

    private String engkey;

    

    private String chi;

    private Long tenantId;
    


}
