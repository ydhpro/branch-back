package com.jd.branch.api.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 系统字典表
 * </p>
 *
 * @author ydh
 * @since 2024-08-26
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqSysDicSave implements Serializable {


    private static final long serialVersionUID = 1L;

    private Long id;

    

    private String dicKey;

    

    private String dicVal;

    

    private Long tenantId;

    


}
