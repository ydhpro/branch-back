package com.jd.branch.api.request;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


/**
 * @author luoqy4
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReqLogin {

    @ApiModelProperty(value = "登录方式：1.密码,2手机短信",required = true)
    private Integer loginType;

    @ApiModelProperty(value = "账号/手机号码/邮箱",required = true)
    @NotBlank
    @Size(max = 20)
    private String account;

    @ApiModelProperty(value = "凭证")
    @Length(max=50)
    private String certificate;

    @ApiModelProperty(value = "验证码token")
    @Length(max=20)
    private String token;

    @ApiModelProperty(value = "验证码内容")
    @Length(max=6)
    private String text;

    @ApiModelProperty(value = "登录方式")
    private String loginMethod;

    private Long tenantId;

    private String openid;

    public String toJsonStr() {
        return JSON.toJSONString(this);
    }

}
