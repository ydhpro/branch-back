package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 师傅组
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqWorkerGroupSave implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    
    private List<Long> brand;
    /**
     * 默认将两个师傅名称拼起来
     */
     @PropertyName(name = "默认将两个师傅名称拼起来")
    private String name;

    

    private Long worker1Id;

    

    private Long worker2Id;

    

    private Long tenantId;


    private String area;

//    合作方式：1全包，2半包
    private Integer comType;



    /**
     * 半包师傅结算价
     */
    @PropertyName(name = "半包师傅安装结算价")
    private BigDecimal halfPackInstallSettlePrice;



    /**
     * 全包师傅结算价
     */
    @PropertyName(name = "全包师傅安装结算价")
    private BigDecimal allPackInstallSettlePrice;


    /**
     * 半包师傅结算价
     */
    @PropertyName(name = "半包师傅勘测结算价")
    private BigDecimal halfPackSurveySettlePrice;



    /**
     * 全包师傅结算价
     */
    @PropertyName(name = "全包师傅勘测结算价")
    private BigDecimal allPackSurveySettlePrice;



    /**
     * 增项抽取比例
     */
    @PropertyName(name = "全包增项抽取比例")
    private BigDecimal allPackAddExtractRate;



    /**
     * 套包外每米提成
     */
    @PropertyName(name = "套包外每米提成")
    private BigDecimal packOutMeterCommission;



    /**
     * 打孔提成
     */
    @PropertyName(name = "打孔提成")
    private BigDecimal holeCommission;



    /**
     * 保护箱仅安装提成
     */
    @PropertyName(name = "保护箱仅安装提成")
    private BigDecimal protectBoxInstallCommission;

    @PropertyName(name = "空开提成")
    private BigDecimal blankOpenCommission;

    /**
     * 开挖提成
     */
    @PropertyName(name = "开挖提成")
    private BigDecimal digCommission;



    /**
     * 保护箱提成
     */
    @PropertyName(name = "保护箱提成")
    private BigDecimal protectBoxCommission;



    /**
     * 路程补贴
     */
    @PropertyName(name = "路程补贴")
    private BigDecimal distanceSubsidy;



}
