package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.jd.branch.annotation.PropertyName;
import com.jd.branch.api.PageRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 师傅组品牌维护
 * </p>
 *
 * @author ydh
 * @since 2024-08-29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqWorkerGroupBrandSearch extends PageRequest {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 合作品牌名称
     */
    @PropertyName(name = "合作品牌名称")
    private String brandName;


    /**
     * 合作品牌id
     */
    @PropertyName(name = "合作品牌id")
    private Long brandId;


    /**
     * 师傅组id
     */
    @PropertyName(name = "师傅组id")
    private Long workerGroupId;


    /**
     * 半包师傅结算价
     */
    @PropertyName(name = "半包师傅安装结算价")
    private BigDecimal halfPackInstallSettlePrice;


    /**
     * 全包师傅结算价
     */
    @PropertyName(name = "全包师傅安装结算价")
    private BigDecimal allPackInstallSettlePrice;


    /**
     * 半包师傅结算价
     */
    @PropertyName(name = "半包师傅勘测结算价")
    private BigDecimal halfPackSurveySettlePrice;


    /**
     * 全包师傅结算价
     */
    @PropertyName(name = "全包师傅勘测结算价")
    private BigDecimal allPackSurveySettlePrice;


    /**
     * 增项抽取比例
     */
    @PropertyName(name = "全包增项抽取比例")
    private BigDecimal allPackAddExtractRate;


    /**
     * 套包外每米提成
     */
    @PropertyName(name = "套包外每米提成")
    private BigDecimal packOutMeterCommission;


    /**
     * 打孔提成
     */
//     @PropertyName(name = "打孔提成")
//    private BigDecimal holeCommission;


    /**
     * 保护箱仅安装提成
     */
//     @PropertyName(name = "保护箱仅安装提成")
//    private BigDecimal protectBoxInstallCommission;

//    @PropertyName(name = "空开提成")
//    private BigDecimal blankOpenCommission;

    /**
     * 开挖提成
     */
//     @PropertyName(name = "开挖提成")
//    private BigDecimal digCommission;


    /**
     * 保护箱提成
     */
//     @PropertyName(name = "保护箱提成")
//    private BigDecimal protectBoxCommission;


    /**
     * 路程补贴
     */
    @PropertyName(name = "路程补贴")
    private BigDecimal distanceSubsidy;


    private Long tenantId;

    @TableLogic
    private Integer deleteFlag;


}
