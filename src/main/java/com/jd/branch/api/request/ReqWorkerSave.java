package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 师傅信息
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqWorkerSave implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 姓名
     */
     @PropertyName(name = "姓名")
    private String name;

    

    /**
     * 手机号码
     */
     @PropertyName(name = "手机号码")
    private String phone;

    

    /**
     * 小程序openid
     */
     @PropertyName(name = "小程序openid")
    private String openId;

    

    private Date createTime;

    

    private Long tenantId;


    private Integer state;

    private String carNum;
}
