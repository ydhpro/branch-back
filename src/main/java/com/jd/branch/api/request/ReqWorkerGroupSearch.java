package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import com.jd.branch.api.PageRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 师傅组
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqWorkerGroupSearch extends PageRequest {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 默认将两个师傅名称拼起来
     */
     @PropertyName(name = "默认将两个师傅名称拼起来")
    private String name;

    

    private Long worker1Id;

    

    private Long worker2Id;

    

    private Long tenantId;

    


}
