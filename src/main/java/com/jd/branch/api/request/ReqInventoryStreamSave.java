package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 库存流水
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqInventoryStreamSave implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long materialId;

    /**
     * 流水数量
     */
     @PropertyName(name = "流水数量")
    private BigDecimal streamNum;
    private String materialName;

    private String remark;

    /**
     * 源头id，如果是从主库外部进货，则id为空
     */
     @PropertyName(name = "源头id，如果是从主库外部进货，则id为空")
    private Long sourceId;

    

    private String sourceName;

    

    private BigDecimal sourceCurrentNum;

    

    private BigDecimal sourceAftNum;

    

    /**
     * 目标id，如果是从主库外部进货，则id为0（主库id）
     */
     @PropertyName(name = "目标id，如果是从主库外部进货，则id为0（主库id）")
     private Long targetId;



    private String targetName;

    

    private BigDecimal targetCurrentNum;

    

    private BigDecimal targetAftNum;

    

    private Long tenantId;

    


}
