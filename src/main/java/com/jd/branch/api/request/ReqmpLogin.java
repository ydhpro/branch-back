package com.jd.branch.api.request;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author luoqy4
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReqmpLogin {

    private String openid;

    private String token;

    private Long tenantId;

    private String phone;

    public String toJsonStr() {
        return JSON.toJSONString(this);
    }

}
