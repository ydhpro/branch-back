package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 库存
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqInventorySave implements Serializable {


    private static final long serialVersionUID = 1L;


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 库存类型：1主库，2师傅，3小网点
     */
    @PropertyName(name = "库存类型：1主库，2师傅，3小网点")
    private Integer type;


    /**
     * 存储对象id（师傅id/小网点id，如果是主库则为0）
     */
    @PropertyName(name = "存储对象id（师傅id/小网点id，如果是主库则为0）")
    private Long entityId;


    /**
     * 存储对象名称：师傅名称，网点名称
     */
    @PropertyName(name = "存储对象名称：师傅名称，网点名称")
    private String entityName;


    /**
     * 物料名称
     */
    @PropertyName(name = "物料名称")
    private String materialName;

    @PropertyName(name = "物料名称")
    private String materialCode;

    /**
     * 当前库存数
     */
    @PropertyName(name = "当前库存数")
    private BigDecimal num;


    /**
     * 锁定库存数
     */
    @PropertyName(name = "锁定库存数")
    private BigDecimal lockNum;


    /**
     * 首次入库时间
     */
    @PropertyName(name = "首次入库时间")
    private Date createTime;


    /**
     * 最近一次操作时间
     */
    @PropertyName(name = "最近一次操作时间")
    private Date latestTime;


    /**
     * 单位	单位
     */
    @PropertyName(name = "单位	单位")
    private String unit;


    private Long tenantId;


    @TableLogic
    private Integer deleteFlag;


    private BigDecimal price;

    private Long batchNum;

    private BigDecimal meterNum;

    private Integer isCable;

    private String model;

    private Long sourceId;
    private BigDecimal salePrice;

    private Long targetId;

    private String targetName;
}
