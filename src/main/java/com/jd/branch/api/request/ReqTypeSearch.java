package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import com.jd.branch.api.PageRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * <p>
 * 库存
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqTypeSearch extends PageRequest {


    private static final long serialVersionUID = 1L;



    /**
     * 库存类型：1主库，2师傅，3小网点
     */
     @PropertyName(name = "库存类型：1主库，2师傅，3小网点")
    private Integer type;

     private String keyword;

}
