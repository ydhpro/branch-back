package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import com.jd.branch.api.PageRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 库存流水
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqInventoryStreamSearch extends PageRequest {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 流水数量
     */
     @PropertyName(name = "流水数量")
    private BigDecimal streamNum;

    

    /**
     * 源头id，如果是从主库外部进货，则id为空
     */
     @PropertyName(name = "源头id，如果是从主库外部进货，则id为空")
    private Long sourceId;

    

    private String sourceName;

    

    private BigDecimal sourceCurrentNum;

    

    private BigDecimal sourceAftNum;

    

    /**
     * 目标id，如果是从主库外部进货，则id为0（主库id）
     */
     @PropertyName(name = "目标id，如果是从主库外部进货，则id为0（主库id）")
     private Long targetId;
    private String materialName;
    private Long materialId;
    private String remark;

    private String targetName;

    

    private Double targetCurrentNum;

    

    private Double targetAftNum;

    

    private Long tenantId;

    


}
