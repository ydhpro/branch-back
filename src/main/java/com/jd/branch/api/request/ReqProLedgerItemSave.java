package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqProLedgerItemSave implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 凭证类型
     */
     @PropertyName(name = "凭证类型")
    private String type;

    

    /**
     * 文档地址
     */
     @PropertyName(name = "文档地址")
    private String attachUrl;

    

    private Long proLedgerId;

    

    private Long tenantId;

    


}
