package com.jd.branch.api.request;

import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 勘安工单
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqSurveyInstallOrderSaveMaterialList implements Serializable {


    private static final long serialVersionUID = 1L;

    private Long orderId;

    private List<ReqOrderMaterialSave> materialList;


}
