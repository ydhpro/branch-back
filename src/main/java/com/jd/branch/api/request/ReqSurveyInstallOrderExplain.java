package com.jd.branch.api.request;

import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 勘安工单
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqSurveyInstallOrderExplain implements Serializable {


    private static final long serialVersionUID = 1L;

    private String content;
}
