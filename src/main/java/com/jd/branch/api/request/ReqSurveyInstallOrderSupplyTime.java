package com.jd.branch.api.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 勘安工单
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqSurveyInstallOrderSupplyTime implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;




    /**
     * 预计勘测开始时间
     */
    @PropertyName(name = "预计勘测开始时间")
    private Date surveyPlanStartTime;



    /**
     * 预计勘测结束时间
     */
    @PropertyName(name = "预计勘测结束时间")
    private Date surveyPlanEndTime;




}
