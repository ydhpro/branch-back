package com.jd.branch.api.request;

import com.jd.branch.annotation.PropertyName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 增项费用
 * </p>
 *
 * @author ydh
 * @since 2024-04-08
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqAddExpSave implements Serializable {


    private static final long serialVersionUID = 1L;

    private Long id;


    /**
     * 收费类别
     */
    @PropertyName(name = "收费类别")
    private String name;


    /**
     * 输入类型（1输入框，2下拉：密码锁，机械锁）
     */
    @PropertyName(name = "输入类型（1输入框，2下拉：密码锁，机械锁）")
    private Integer type;


    /**
     * 单价
     */
    @PropertyName(name = "单价")
    private BigDecimal price;


    @PropertyName(name = "师傅结算价")
    private BigDecimal settlePrice;
    /**
     * 总价
     */
    @PropertyName(name = "总价")
    private BigDecimal total;


    /**
     * 数量
     */
    @PropertyName(name = "数量")
    private BigDecimal num;


    /**
     * 关联单据号
     */
    @PropertyName(name = "关联单据号")
    private Long orderId;


    private Long tenantId;

}
