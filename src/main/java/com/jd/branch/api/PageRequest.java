package com.jd.branch.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author luoqy4
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "分页请求参数")
public class PageRequest {

    @ApiModelProperty(value = "当前页",example = "当前页", required = true)
    @NotNull(message="currentPage can be null")
    @Min(value = 0, message = "currentPage greater than 0")
	private Integer currentPage=1;
    
    @ApiModelProperty(value = "每页大小",example = "每页大小", required = true)
    @NotNull(message="pageSize can be null")
    @Min(value = 0, message = "pageSize must greater than 0")
	private Integer pageSize=10;

    private String orderBy;

    private String orderAs;

    private String keyword;
	
}