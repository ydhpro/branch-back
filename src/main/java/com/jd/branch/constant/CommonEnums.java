package com.jd.branch.constant;

/**
 * @author yuandh5
 * @date 2023/4/7
 */
public class CommonEnums {

    public enum SIOrderState {
        KCDPD(1, "勘测待派单"),
        KCSJQR(2, "勘测时间确认"),
        DKC(3, "待勘测"),
        KCDSH(4, "勘测待审核"),
        KCBH(5, "勘测驳回"),
        AZDPD(6, "安装待派单"),
        AZSJQR(7, "安装时间确认"),
        DAZ(8, "待安装"),
        AZDSH(9, "安装待审核"),
        AZBH(10, "安装驳回"),
        YAZ(11, "已安装"),
        YJD(12, "已结单"),
        YJS(13, "已结算"),


        ;
        private final Integer index;
        private final String name;

        SIOrderState(Integer index, String name) {
            this.index = index;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Integer getIndex() {
            return index;
        }

        public static String getNameByIndex(Integer index) {
            for (SIOrderState t : SIOrderState.values()) {
                if (t.getIndex().equals(index)) {
                    return t.getName();
                }
            }
            return null;
        }

        public static Integer getIndexByName(String name) {
            for (SIOrderState t : SIOrderState.values()) {
                if (t.getName().equals(name)) {
                    return t.getIndex();
                }
            }
            return null;
        }
    }

    public enum InventoryType {
        ZK(1, "主库"),
        SF(2, "师傅"),
        XWD(3, "小网点"),


        ;
        private final Integer index;
        private final String name;

        InventoryType(Integer index, String name) {
            this.index = index;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Integer getIndex() {
            return index;
        }

        public static String getNameByIndex(Integer index) {
            for (SIOrderState t : SIOrderState.values()) {
                if (t.getIndex().equals(index)) {
                    return t.getName();
                }
            }
            return null;
        }

        public static Integer getIndexByName(String name) {
            for (SIOrderState t : SIOrderState.values()) {
                if (t.getName().equals(name)) {
                    return t.getIndex();
                }
            }
            return null;
        }
    }

    public enum ChartDimension {
        DAY(1, "日"),
        MONTH(2, "月"),

        SEASON(3, "季度"),
        YEAR(4, "年"),

        AREA(5, "区域"),


        ;
        private final Integer index;
        private final String name;

        ChartDimension(Integer index, String name) {
            this.index = index;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Integer getIndex() {
            return index;
        }

        public static String getNameByIndex(Integer index) {
            for (ChartDimension t : ChartDimension.values()) {
                if (t.getIndex().equals(index)) {
                    return t.getName();
                }
            }
            return null;
        }

        public static Integer getIndexByName(String name) {
            for (ChartDimension t : ChartDimension.values()) {
                if (t.getName().equals(name)) {
                    return t.getIndex();
                }
            }
            return null;
        }
    }

    public enum WorkerGroupComType {
        QB(1, "全包"),
        BB(2, "半包"),


        ;
        private final Integer index;
        private final String name;

        WorkerGroupComType(Integer index, String name) {
            this.index = index;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Integer getIndex() {
            return index;
        }

        public static String getNameByIndex(Integer index) {
            for (WorkerGroupComType t : WorkerGroupComType.values()) {
                if (t.getIndex().equals(index)) {
                    return t.getName();
                }
            }
            return null;
        }

        public static Integer getIndexByName(String name) {
            for (WorkerGroupComType t : WorkerGroupComType.values()) {
                if (t.getName().equals(name)) {
                    return t.getIndex();
                }
            }
            return null;
        }
    }

    public enum AddExpName {
        KK("空开"),
        DK("打孔"),
        KW("开挖"),

        BHX("保护箱"),
        BHXINS("保护箱仅安装"),


        ;

        private final String name;

        AddExpName(String name) {

            this.name = name;
        }

        public String getName() {
            return name;
        }


    }

    public enum SIOrderPipeType {
        PVC(1, "pvc管材"),
        DUXIN(2, "镀锌"),


        ;
        private final Integer index;
        private final String name;

        SIOrderPipeType(Integer index, String name) {
            this.index = index;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Integer getIndex() {
            return index;
        }

        public static String getNameByIndex(Integer index) {
            for (SIOrderPipeType t : SIOrderPipeType.values()) {
                if (t.getIndex().equals(index)) {
                    return t.getName();
                }
            }
            return null;
        }

        public static Integer getIndexByName(String name) {
            for (SIOrderPipeType t : SIOrderPipeType.values()) {
                if (t.getName().equals(name)) {
                    return t.getIndex();
                }
            }
            return null;
        }
    }

}
