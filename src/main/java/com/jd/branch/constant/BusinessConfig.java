package com.jd.branch.constant;

/**
 * @author yuandh5
 * @date 2023/4/25
 */
public class BusinessConfig {
    //签到获得的樱叶
    public static final Long SIGNIN_POINT = 1L;

    //阶段赏数字
    public static final Integer RR_PRICE_NUM_LEVEL = 999;
    //w赏数字
    public static final Integer W_PRICE_NUM_LEVEL = 998;
    //冲冲赏数字
    public static final Integer RURU_PRICE_NUM_LEVEL = 997;
    //只有参与奖池抽奖才加经验

    //分组id
    public final static String StaticGroupId = "test-longying";
}
