package com.jd.branch.exception;

import cn.hutool.extra.spring.SpringUtil;
import com.jd.branch.util.I18nUtil;


public class BizReturnException extends Exception {


	private static final long serialVersionUID = 4773132228013520732L;
	private String message;

	private Object data;

	@Override
	public String getMessage() {
		return message;
	}

	public Object getData() { return data;}

	public void setMessage(String message) {
		this.message = message;
	}


	public BizReturnException() {
		super();
	}

	public BizReturnException(String code,Object data) {
		I18nUtil i18nUtil=(I18nUtil)SpringUtil.getBean("i18nUtil");
		String i18Message=i18nUtil.getI18nMessage(code);
		this.message=i18Message;
		this.data=data;
	}

    
}
