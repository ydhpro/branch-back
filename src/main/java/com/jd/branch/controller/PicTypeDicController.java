package com.jd.branch.controller;


import com.jd.branch.api.request.ReqPicType;
import com.jd.branch.api.request.ReqPicTypeDicSave;
import com.jd.branch.api.request.ReqPicTypeDicSearch;
import com.jd.branch.api.response.RespPicType;
import com.jd.branch.api.response.RespPicTypeDicDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.PicTypeDic;
import com.jd.branch.service.PicTypeDicService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 图片类型字典 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-10-31
 */
@RestController
public class PicTypeDicController extends BaseController {
    @Autowired
    private PicTypeDicService service;

    @PostMapping("/api/v1/pic-type-dic")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request, @RequestBody ReqPicTypeDicSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/pic-type-dic")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request, @Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/pic-type-dic/find-list")
    @ApiOperation("列表查询")
    public Message<List<RespPicTypeDicDetail>> findList(HttpServletRequest request, @RequestBody ReqPicTypeDicSearch item) throws Exception {
        List<RespPicTypeDicDetail> list = service.findList(this.getLoginObj(request), item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/pic-type-dic")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request, @RequestBody ReqPicTypeDicSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/pic-type-dic/detail/{id}")
    @ApiOperation("获取详情")
    public Message<RespPicTypeDicDetail> detail(HttpServletRequest request, @PathVariable(value = "id") Long id) throws Exception {
        RespPicTypeDicDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/pic-type-dic")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespPicTypeDicDetail>> findPaginationNew(HttpServletRequest request, @Validated @ModelAttribute ReqPicTypeDicSearch reqPicTypeDicSearch) throws Exception {

        PageInfo<RespPicTypeDicDetail> search = service.search(this.getLoginObj(request), reqPicTypeDicSearch);
        return new Message<>(search);

    }

    @PostMapping("/api/v1/pic-type-dic/get-pic")
    @ApiOperation("获取单据图片，并且拼上类型名称")
    public Message<List<RespPicType>> getPic(HttpServletRequest request, @RequestBody ReqPicType reqPicType) throws Exception {
        List<RespPicType> pic = service.getPic(this.getLoginObj(request), reqPicType);
        return new Message<>(pic);

    }
}
