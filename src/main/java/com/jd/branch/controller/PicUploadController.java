package com.jd.branch.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.entity.PicAttachment;
import com.jd.branch.mapper.PicAttachmentMapper;
import com.jd.branch.service.PicAttachmentService;
import com.jd.branch.service.impl.TencentCOS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author ydh
 * @version 1.0
 * @date 2021/1/7 21:39
 */
@RestController
public class PicUploadController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PicUploadController.class);

    @Autowired
    private PicAttachmentMapper picAttachmentMapper;

    @PostMapping("/api/v1/pic-upload")
    public Message<String> uploading(@RequestParam("file") MultipartFile file, @RequestParam("modelName") String modelName) throws Exception {

        StringBuffer upload = TencentCOS.upload(file, modelName);
        LOGGER.info("永久文件名：" + upload.toString());
        return new Message<>(upload.toString());

    }

    @PostMapping("hehe")
    public Message<String> hh(@RequestParam("file") MultipartFile file, @RequestParam("modelName") String modelName) throws Exception {
        StringBuffer upload = TencentCOS.upload(file, modelName);
        LOGGER.info("永久文件名：" + upload.toString());
        return new Message<>(upload.toString());

    }

    @GetMapping("/api/v1/delete-upload/{filename}")
    public Message<String> deleteUploaded(HttpServletRequest request, @PathVariable(value = "filename")String filename) throws Exception {
        List<PicAttachment> picAttachments = picAttachmentMapper.selectList(new QueryWrapper<>(PicAttachment.builder().picUrl("https://branch-1257310092.cos.ap-guangzhou.myqcloud.com/branch/" + filename).build()));
        if(picAttachments.size()>0){
            for(PicAttachment pa:picAttachments){
                picAttachmentMapper.deleteById(pa);
                TencentCOS.delete(filename);
            }
        }
        return Message.getSuccess();
    }
}
