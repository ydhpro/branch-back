package com.jd.branch.controller;


import com.jd.branch.api.request.ReqComBrandSave;
import com.jd.branch.api.request.ReqComBrandSearch;
import com.jd.branch.api.response.RespComBrandDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.ComBrand;
import com.jd.branch.service.ComBrandService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 合作品牌相关参数 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-08-26
 */
@RestController
public class ComBrandController extends BaseController {
    @Autowired
    private ComBrandService service;

    @PostMapping("/api/v1/com-brand")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqComBrandSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/com-brand")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/com-brand/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespComBrandDetail>> findList(HttpServletRequest request,@RequestBody ReqComBrandSearch item)throws Exception {
        List<RespComBrandDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/com-brand")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqComBrandSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/com-brand/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespComBrandDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespComBrandDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/com-brand")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespComBrandDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqComBrandSearch reqComBrandSearch) throws Exception {

        PageInfo<RespComBrandDetail> search = service.search(this.getLoginObj(request),reqComBrandSearch);
        return new Message<>(search);

    }
}
