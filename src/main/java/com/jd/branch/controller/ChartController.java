package com.jd.branch.controller;

import com.jd.branch.api.request.ReqBranchBusinessSit;
import com.jd.branch.common.Message;
import com.jd.branch.entity.ChiToKey;
import com.jd.branch.service.ChartService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ChartController {

    @Autowired
    ChartService chartService;

//    @PostMapping("/api/v1/ChiToKey")
    @ApiOperation("网点经营情况")
    public Message<String> branchBusinessSitTotal(HttpServletRequest request, @RequestBody ReqBranchBusinessSit item) throws Exception {
        chartService.branchBusinessSitTotal(item);
        return Message.getSuccess();
    }

    @ApiOperation("工单排版情况")
    public Message<String> save(HttpServletRequest request, @RequestBody ReqBranchBusinessSit item) throws Exception {
        chartService.branchBusinessSitTotal(item);
        return Message.getSuccess();
    }
}
