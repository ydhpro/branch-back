package com.jd.branch.controller;


import com.jd.branch.api.request.ReqAppointModelSave;
import com.jd.branch.api.request.ReqAppointModelSearch;
import com.jd.branch.api.response.RespAppointModelDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.AppointModel;
import com.jd.branch.service.AppointModelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 预约信息模板 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-03-25
 */
@RestController
public class AppointModelController extends BaseController {
    @Autowired
    private AppointModelService service;

    @PostMapping("/api/v1/appoint-model")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqAppointModelSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/appoint-model")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/appoint-model/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespAppointModelDetail>> findList(HttpServletRequest request,@RequestBody ReqAppointModelSearch item)throws Exception {
        List<RespAppointModelDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/appoint-model")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqAppointModelSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/appoint-model/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespAppointModelDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespAppointModelDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/appoint-model")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespAppointModelDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqAppointModelSearch reqAppointModelSearch) throws Exception {

        PageInfo<RespAppointModelDetail> search = service.search(this.getLoginObj(request),reqAppointModelSearch);
        return new Message<>(search);

    }
}
