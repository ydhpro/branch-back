package com.jd.branch.controller;


import com.jd.branch.api.request.ReqWorkerSave;
import com.jd.branch.api.request.ReqWorkerSearch;
import com.jd.branch.api.response.RespWorkerDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.Worker;
import com.jd.branch.service.WorkerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 师傅信息 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@RestController
public class WorkerController extends BaseController {
    @Autowired
    private WorkerService service;

    @PostMapping("/api/v1/worker")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqWorkerSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/worker")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/worker/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespWorkerDetail>> findList(HttpServletRequest request,@RequestBody ReqWorkerSearch item)throws Exception {
        List<RespWorkerDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/worker")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqWorkerSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/worker/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespWorkerDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespWorkerDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/worker")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespWorkerDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqWorkerSearch reqWorkerSearch) throws Exception {

        PageInfo<RespWorkerDetail> search = service.search(this.getLoginObj(request),reqWorkerSearch);
        return new Message<>(search);

    }
}
