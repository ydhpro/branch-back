package com.jd.branch.controller;


import com.jd.branch.api.request.ReqAdminSave;
import com.jd.branch.api.request.ReqAdminSearch;
import com.jd.branch.api.request.ReqInventorySave;
import com.jd.branch.api.request.ReqPhoneCode;
import com.jd.branch.api.response.RespAdminDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.service.AdminService;
import com.jd.branch.service.CommonService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 管理员 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@RestController
@Slf4j
public class CommonController extends BaseController {
    @Autowired
    private CommonService service;

    @PostMapping("/api/v1/phone/code")
    @ApiOperation("发送手机验证码")
    public Message<String> save(HttpServletRequest request, @RequestBody ReqPhoneCode item) throws Exception {
        String phoneCode = service.getPhoneCode(item.getPhone());
        log.info("发送短信" + phoneCode + "到" + item.getPhone());
        return new Message<>(phoneCode);
    }
}
