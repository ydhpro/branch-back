package com.jd.branch.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jd.branch.api.request.*;
import com.jd.branch.api.response.RespSurveyInstallOrderDetail;
import com.jd.branch.common.*;
import com.jd.branch.constant.CommonEnums;
import com.jd.branch.entity.OrderFee;
import com.jd.branch.entity.PicAttachment;
import com.jd.branch.entity.SurveyInstallOrder;
import com.jd.branch.mapper.OrderFeeMapper;
import com.jd.branch.mapper.PicAttachmentMapper;
import com.jd.branch.service.PicAttachmentService;
import com.jd.branch.service.SurveyInstallOrderService;
import com.jd.branch.util.DefindUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 勘安工单 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@RestController
public class SurveyInstallOrderController extends BaseController {
    @Autowired
    private SurveyInstallOrderService service;

    @Autowired
    private PicAttachmentService picAttachmentService;

    @Autowired
    private PicAttachmentMapper picAttachmentMapper;

    @Autowired
    private OrderFeeMapper orderFeeMapper;

    @PostMapping("/api/v1/survey-install-order")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/survey-install-order")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/survey-install-order/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespSurveyInstallOrderDetail>> findList(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSearch item)throws Exception {
        List<RespSurveyInstallOrderDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/survey-install-order")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/survey-install-order/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespSurveyInstallOrderDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespSurveyInstallOrderDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/survey-install-order")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespSurveyInstallOrderDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqSurveyInstallOrderSearch reqSurveyInstallOrderSearch) throws Exception {

        PageInfo<RespSurveyInstallOrderDetail> search = service.search(this.getLoginObj(request),reqSurveyInstallOrderSearch);
        return new Message<>(search);

    }

    @GetMapping("/api/v1/worker/survey-install-order")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespSurveyInstallOrderDetail>> findWorkPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqSurveyInstallOrderSearch reqSurveyInstallOrderSearch) throws Exception {

        PageInfo<RespSurveyInstallOrderDetail> search = service.workSearch(this.getLoginObj(request),reqSurveyInstallOrderSearch);
        return new Message<>(search);

    }

    @PutMapping("/api/v1/survey-install-order/dispatch")
    @ApiOperation("派单")
    public Message<String> surDispatch(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        service.surDispatch(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @PutMapping("/api/v1/survey-install-order/supply-time")
    @ApiOperation("提交预计勘测时间")
    public Message<String> supplyTime(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        service.submitSurTime(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @PutMapping("/api/v1/survey-install-order/supply-install-time")
    @ApiOperation("提交预计安装时间")
    public Message<String> supplyInstallTime(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        service.submitInstallTime(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @PutMapping("/api/v1/survey-install-order/survey-info")
    @ApiOperation("提交勘测信息")
    public Message<String> surveyInfo(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        item.setState(CommonEnums.SIOrderState.KCDSH.getIndex());
        Integer integer = picAttachmentMapper.selectCount(new QueryWrapper<>(PicAttachment.builder().relateId(item.getId()).tenantId(this.getLoginObj(request).getTenantId()).isAudited(0).build()));
        DefindUtils.check(integer>0,"尚且还有驳回的照片，请重新上传提交");
        service.updateById(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @PutMapping("/api/v1/survey-install-order/audit-survey")
    @ApiOperation("审核勘测信息")
    public Message<String> auditSurvey(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderAuditSurvey item) throws Exception {
        service.auditSurvey(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @PutMapping("/api/v1/survey-install-order/finish-install")
    @ApiOperation("安装完成")
    public Message<String> finishInstall(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderAuditSurvey item) throws Exception {
        service.finishInstall(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/survey-install-order/save-material-list")
    @ApiOperation("保存堪安单物料列表")
    public Message<String> saveMaterialList(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSaveMaterialList item) throws Exception {
        service.saveMaterialList(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/survey-install-order/end")
    @ApiOperation("结单")
    public Message<String> end(HttpServletRequest request,@RequestBody ReqCommDel req) throws Exception {
        service.end(this.getLoginObj(request),req);
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/survey-install-order/explain")
    @ApiOperation("解析")
    public Message<JSONObject> explain(HttpServletRequest request,@ModelAttribute @RequestBody ReqSurveyInstallOrderExplain req) throws Exception {
        JSONObject explain = service.explain(this.getLoginObj(request), req);
        return new Message<>(explain);
    }

    @PutMapping("/api/v1/survey-install-order/change-state")
    @ApiOperation("解析")
    public Message<String> changeState(HttpServletRequest request, @RequestBody ReqSurveyInstallOrderSave req) throws Exception {
        service.changeState(this.getLoginObj(request),req);
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/survey-install-order/cal-order-fee")
    @ApiOperation("计算费用")
    public Message<String> calOrderFee(HttpServletRequest request,@RequestBody ReqSurveyInstallOrderSave item) throws Exception {
        OrderFee calFormaleAndResult = service.getCalFormaleAndResult(item.getId());
        List<OrderFee> orderFees = orderFeeMapper.selectList(new QueryWrapper<>(OrderFee.builder().orderId(item.getId()).build()));
        if (orderFees.size() == 0) {
            orderFeeMapper.insert(calFormaleAndResult);
        }else{
            calFormaleAndResult.setId(orderFees.get(0).getId());
            orderFeeMapper.updateById(calFormaleAndResult);
        }

        return Message.getSuccess();
    }
}
