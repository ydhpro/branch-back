package com.jd.branch.controller;

import com.jd.branch.api.request.ReqPicAttachmentSearch;
import com.jd.branch.api.response.RespPicAttachmentDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.service.PicAttachmentService;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@Controller  
public class ImageZipController extends BaseController {

    @Autowired
    private PicAttachmentService picAttachmentService;
    @GetMapping("/download-images-zip")  
    public ResponseEntity<Resource> downloadImagesZip(HttpServletRequest request, @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        List<RespPicAttachmentDetail> list = picAttachmentService.findList(this.getLoginObj(request), ReqPicAttachmentSearch.builder().tenantId(this.getLoginObj(request).getTenantId()).relateId(reqCommDel.getId()).build());
        List<String> collect = list.stream().map(RespPicAttachmentDetail::getPicUrl)
                .collect(Collectors.toList());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
        ZipArchiveOutputStream zos = new ZipArchiveOutputStream(baos);  

        for (String imageUrl : collect) {
            URL url = new URL(imageUrl);  
            InputStream is = url.openStream();  
            ZipArchiveEntry entry = new ZipArchiveEntry(url.getFile());
            zos.putArchiveEntry(entry);  

            byte[] buffer = new byte[1024];  
            int len;  
            while ((len = is.read(buffer)) > 0) {  
                zos.write(buffer, 0, len);  
            }  

            zos.closeArchiveEntry();  
            is.close();  
        }  

        zos.close();  

        byte[] content = baos.toByteArray();  

        HttpHeaders headers = new HttpHeaders();  
        headers.setContentDispositionFormData("attachment", "images.zip");  
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);  

        return ResponseEntity.ok()  
                .headers(headers)  
                .body(new InputStreamResource(new ByteArrayInputStream(content)));
    }  
}