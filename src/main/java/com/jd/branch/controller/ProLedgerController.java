package com.jd.branch.controller;


import com.jd.branch.api.request.ReqProLedgerSave;
import com.jd.branch.api.request.ReqProLedgerSearch;
import com.jd.branch.api.response.RespProLedgerDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.ProLedger;
import com.jd.branch.service.ProLedgerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 物业台账 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@RestController
public class ProLedgerController extends BaseController {
    @Autowired
    private ProLedgerService service;

    @PostMapping("/api/v1/pro-ledger")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqProLedgerSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/pro-ledger")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/pro-ledger/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespProLedgerDetail>> findList(HttpServletRequest request,@RequestBody ReqProLedgerSearch item)throws Exception {
        List<RespProLedgerDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/pro-ledger")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqProLedgerSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/pro-ledger/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespProLedgerDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespProLedgerDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/pro-ledger")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespProLedgerDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqProLedgerSearch reqProLedgerSearch) throws Exception {

        PageInfo<RespProLedgerDetail> search = service.search(this.getLoginObj(request),reqProLedgerSearch);
        return new Message<>(search);

    }
}
