package com.jd.branch.controller;


import cn.hutool.core.util.StrUtil;
import com.jd.branch.api.request.ReqPicAttachmentSave;
import com.jd.branch.api.request.ReqPicAttachmentSearch;
import com.jd.branch.api.response.RespPicAttachmentDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.PicAttachment;
import com.jd.branch.service.PicAttachmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 图片附件 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@RestController
public class PicAttachmentController extends BaseController {
    @Autowired
    private PicAttachmentService service;

    @PostMapping("/api/v1/pic-attachment")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqPicAttachmentSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/pic-attachment")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/pic-attachment/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespPicAttachmentDetail>> findList(HttpServletRequest request,@RequestBody ReqPicAttachmentSearch item)throws Exception {
        List<RespPicAttachmentDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/pic-attachment")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqPicAttachmentSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/pic-attachment/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespPicAttachmentDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespPicAttachmentDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/pic-attachment")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespPicAttachmentDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqPicAttachmentSearch reqPicAttachmentSearch) throws Exception {

        PageInfo<RespPicAttachmentDetail> search = service.search(this.getLoginObj(request),reqPicAttachmentSearch);
        return new Message<>(search);

    }
}
