package com.jd.branch.controller;

import com.jd.branch.api.request.ReqLogin;
import com.jd.branch.api.request.ReqmpLogin;
import com.jd.branch.api.response.RespPersonDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.Message;
import com.jd.branch.service.LoginService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author luoqy4
 */
@Validated
@RestController
public class LoginController extends BaseController {

    @Autowired
    private LoginService loginService;

//    @ApiOperation(value = "登录", httpMethod = "POST")
//    @PostMapping(value = "mplogin")
//    public Message<LoginRedisObj> mpLogin(@Valid @RequestBody @ApiParam(name="json对象",value="传入json格式",required=true) ReqLYmpLogin reqLYmpLogin) throws Exception {
//        LoginRedisObj LoginRedisObj = loginService.loginyMobile(reqLYmpLogin.getMobile(), reqLYmpLogin.getOpenid());
//        return new Message<>(LoginRedisObj);
//    }

    @ApiOperation(value = "微信手机号码登录", httpMethod = "POST")
    @PostMapping(value = "/api/v1/mp-phone-login")
    public Message<LoginRedisObj> mpPhoneLogin(@Valid @RequestBody @ApiParam(name="json对象",value="传入json格式",required=true) ReqmpLogin reqLYmpLogin) throws Exception {
        LoginRedisObj LoginRedisObj = loginService.loginyMobile(reqLYmpLogin.getPhone(),reqLYmpLogin.getOpenid());
        return new Message<>(LoginRedisObj);
    }

    //只用这个了
    @ApiOperation(value = "登录", httpMethod = "POST")
    @PostMapping(value = "mp-login")
    public Message<LoginRedisObj> mpLogin(@Valid @RequestBody @ApiParam(name="json对象",value="传入json格式",required=true) ReqmpLogin reqLYmpLogin) throws Exception {
        LoginRedisObj LoginRedisObj = loginService.loginByOpenid(reqLYmpLogin.getOpenid(),reqLYmpLogin.getTenantId());
        return new Message<>(LoginRedisObj);
    }

    @ApiOperation(value = "登录", httpMethod = "POST")
    @PostMapping(value = "/api/v1/login")
    public Message<LoginRedisObj> login(@Valid @RequestBody @ApiParam(name="json对象",value="传入json格式",required=true) ReqLogin reqLogin) throws Exception {
        LoginRedisObj resp = loginService.Login(reqLogin);
        return new Message<>(resp);
    }

    @ApiOperation(value = "查询当前用户个人信息", httpMethod = "GET")
    @GetMapping(value = "/api/v1/person")
    public Message<RespPersonDetail> detailPerson(HttpServletRequest request) throws Exception {
        RespPersonDetail resp=loginService.detailPerson(this.getLoginObj(request));
        return new Message<RespPersonDetail>(resp);
    }

    @ApiOperation(value = "退出", httpMethod = "GET")
    @PostMapping(value = "logout")
    public Message<String> logout(HttpServletRequest request) throws Exception {
        loginService.logout(this.getLoginObj(request));
        return Message.getSuccess();
    }
}
