package com.jd.branch.controller;

import com.alibaba.fastjson.JSONObject;
import com.jd.branch.common.Message;

import com.jd.branch.service.impl.WeChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author ydh
 * @version 1.0
 * @date 2021/1/11 10:39
 */
@RestController
@RequestMapping("/wx")
public class WeChatController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeChatController.class);




//
//    @RequestMapping(value = "test", method = RequestMethod.POST)
//    public Message<String> msg(){
//        return new Message<>(env);
//    }


//    @RequestMapping(value = "obtain-phone", method = RequestMethod.POST)
//    public Message<LoginLYRedisObj> obtainPhone(HttpServletRequest request, HttpServletResponse response,
//                                                @RequestBody(required = true) String jsonStr) throws Exception {
//        LoginLYRedisObj lro = WeChatService.getPhoneNum(response, jsonStr);
////        JSONObject jsonStrs = JSONObject.parseObject(jsonStr);
////        String openid = (String) jsonStrs.get("openid");
//        LoginLYRedisObj loginLYRedisObj = loginService.loginyMobile(lro.getMobile(), lro.getOpenId());
//        return new Message<>(loginLYRedisObj);
//    }

//    @GetMapping("send")
//    public Message<String> send() throws Exception{
//        WeChatService.sendModelMsg();
//        return Message.getSuccess();
//    }

    @GetMapping("code-get-openid/{code}")
    //通过公众号传回来的code换取access_token和openid
    public Message<String> codeGetOpenid(HttpServletRequest request, HttpServletResponse response, @PathVariable(value = "code") String code) throws Exception {

        String openidByCode = WeChatService.getOpenidByCode(code);
        return new Message<>(openidByCode);
    }


}
