package com.jd.branch.controller;


import com.jd.branch.api.request.ReqSysDicSave;
import com.jd.branch.api.request.ReqSysDicSearch;
import com.jd.branch.api.response.RespSysDicDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.SysDic;
import com.jd.branch.service.SysDicService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 系统字典表 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-08-26
 */
@RestController
public class SysDicController extends BaseController {
    @Autowired
    private SysDicService service;

    @PostMapping("/api/v1/sys-dic")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqSysDicSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/sys-dic")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/sys-dic/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespSysDicDetail>> findList(HttpServletRequest request,@RequestBody ReqSysDicSearch item)throws Exception {
        List<RespSysDicDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/sys-dic")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqSysDicSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/sys-dic/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespSysDicDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespSysDicDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/sys-dic")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespSysDicDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqSysDicSearch reqSysDicSearch) throws Exception {

        PageInfo<RespSysDicDetail> search = service.search(this.getLoginObj(request),reqSysDicSearch);
        return new Message<>(search);

    }
}
