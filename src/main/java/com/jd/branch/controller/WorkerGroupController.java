package com.jd.branch.controller;


import com.jd.branch.api.request.ReqWorkerGroupSave;
import com.jd.branch.api.request.ReqWorkerGroupSearch;
import com.jd.branch.api.response.RespWorkerGroupDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.WorkerGroup;
import com.jd.branch.service.WorkerGroupService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 师傅组 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-03-18
 */
@RestController
public class WorkerGroupController extends BaseController {
    @Autowired
    private WorkerGroupService service;

    @PostMapping("/api/v1/worker-group")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqWorkerGroupSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/worker-group")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/worker-group/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespWorkerGroupDetail>> findList(HttpServletRequest request,@RequestBody ReqWorkerGroupSearch item)throws Exception {
        List<RespWorkerGroupDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/worker-group")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqWorkerGroupSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/worker-group/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespWorkerGroupDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespWorkerGroupDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/worker-group")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespWorkerGroupDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqWorkerGroupSearch reqWorkerGroupSearch) throws Exception {

        PageInfo<RespWorkerGroupDetail> search = service.search(this.getLoginObj(request),reqWorkerGroupSearch);
        return new Message<>(search);

    }
}
