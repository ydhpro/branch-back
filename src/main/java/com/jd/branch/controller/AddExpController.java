package com.jd.branch.controller;


import com.jd.branch.api.request.ReqAddExpBatchSave;
import com.jd.branch.api.request.ReqAddExpSave;
import com.jd.branch.api.request.ReqAddExpSearch;
import com.jd.branch.api.response.RespAddExpDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.AddExp;
import com.jd.branch.service.AddExpService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * 增项费用 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-04-08
 */
@RestController
public class AddExpController extends BaseController {
    @Autowired
    private AddExpService service;

    @PostMapping("/api/v1/add-exp")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody ReqAddExpSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/add-exp/batch-save")
    @ApiOperation("批量保存")
    public Message<String> batchSave(HttpServletRequest request,@RequestBody ReqAddExpBatchSave item) throws Exception {
        service.batchSave(this.getLoginObj(request),item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/add-exp")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/add-exp/find-list")
    @ApiOperation("列表查询")
    public  Message<List<RespAddExpDetail>> findList(HttpServletRequest request,@RequestBody ReqAddExpSearch item)throws Exception {
        List<RespAddExpDetail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/add-exp")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody ReqAddExpSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/add-exp/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<RespAddExpDetail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        RespAddExpDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/add-exp")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespAddExpDetail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute ReqAddExpSearch reqAddExpSearch) throws Exception {

        PageInfo<RespAddExpDetail> search = service.search(this.getLoginObj(request),reqAddExpSearch);
        return new Message<>(search);

    }
}
