package com.jd.branch.controller;


import com.jd.branch.api.request.ReqWorkerGroupBrandSave;
import com.jd.branch.api.request.ReqWorkerGroupBrandSearch;
import com.jd.branch.api.response.RespWorkerGroupBrandDetail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.WorkerGroupBrand;
import com.jd.branch.service.WorkerGroupBrandService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 师傅组品牌维护 前端控制器
 * </p>
 *
 * @author ydh
 * @since 2024-09-07
 */
@RestController
public class WorkerGroupBrandController extends BaseController {
    @Autowired
    private WorkerGroupBrandService service;

    @PostMapping("/api/v1/worker-group-brand")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request, @RequestBody ReqWorkerGroupBrandSave item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/worker-group-brand/batch-save")
    @ApiOperation("批量保存")
    public Message<String> batchSave(HttpServletRequest request, @RequestBody List<ReqWorkerGroupBrandSave> items) throws Exception {
        for (ReqWorkerGroupBrandSave result : items) {
            if (result.getId() != null) {
                service.updateById(this.getLoginObj(request), result);
            } else {
                service.saveMaster(this.getLoginObj(request), result);
            }

        }
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/worker-group-brand")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request, @Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/worker-group-brand/find-list")
    @ApiOperation("列表查询")
    public Message<List<RespWorkerGroupBrandDetail>> findList(HttpServletRequest request, @RequestBody ReqWorkerGroupBrandSearch item) throws Exception {
        List<RespWorkerGroupBrandDetail> list = service.findList(this.getLoginObj(request), item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/worker-group-brand")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request, @RequestBody ReqWorkerGroupBrandSave item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/worker-group-brand/detail/{id}")
    @ApiOperation("获取详情")
    public Message<RespWorkerGroupBrandDetail> detail(HttpServletRequest request, @PathVariable(value = "id") Long id) throws Exception {
        RespWorkerGroupBrandDetail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/worker-group-brand")
    @ApiOperation("分页查询")
    public Message<PageInfo<RespWorkerGroupBrandDetail>> findPaginationNew(HttpServletRequest request, @Validated @ModelAttribute ReqWorkerGroupBrandSearch reqWorkerGroupBrandSearch) throws Exception {

        PageInfo<RespWorkerGroupBrandDetail> search = service.search(this.getLoginObj(request), reqWorkerGroupBrandSearch);
        return new Message<>(search);

    }
}
