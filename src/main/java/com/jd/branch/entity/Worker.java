package com.jd.branch.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 师傅信息
 * </p>
 *
 * @author ydh
 * @since 2024-08-20
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Worker implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 姓名
     */
     @PropertyName(name = "姓名")
    private String name;

    

    /**
     * 手机号码
     */
     @PropertyName(name = "手机号码")
    private String phone;

    

    /**
     * 小程序openid
     */
     @PropertyName(name = "小程序openid")
    private String openId;

    

    private Date createTime;

    

    /**
     * 状态：1正常，2离职
     */
     @PropertyName(name = "状态：1正常，2离职")
    private Integer state;

    

    /**
     * 车牌
     */
     @PropertyName(name = "车牌")
    private String carNum;

    

    private Long tenantId;

    

    @TableLogic
private Integer deleteFlag;

    


}
