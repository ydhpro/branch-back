package com.jd.branch.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 
 * </p>
 *
 * @author ydh
 * @since 2024-08-20
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProLedgerItem implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    /**
     * 凭证类型
     */
     @PropertyName(name = "凭证类型")
    private String type;

    

    /**
     * 文档地址
     */
     @PropertyName(name = "文档地址")
    private String attachUrl;

    

    private Long proLedgerId;

    

    private Long tenantId;

    

    @TableLogic
private Integer deleteFlag;

    


}
