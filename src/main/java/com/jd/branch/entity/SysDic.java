package com.jd.branch.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 系统字典表
 * </p>
 *
 * @author ydh
 * @since 2024-08-26
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SysDic implements Serializable {


    private static final long serialVersionUID = 1L;

    private Long id;


    private String dicKey;


    private String dicVal;


    private Long tenantId;

    @TableLogic
    private Integer deleteFlag;


}
