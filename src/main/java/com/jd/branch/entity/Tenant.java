package com.jd.branch.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 租户
 * </p>
 *
 * @author ydh
 * @since 2024-08-20
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Tenant implements Serializable {


    private static final long serialVersionUID = 1L;

    private Long id;

    

    private String name;

    

    /**
     * 状态：0待启用，1生效中，已过期
     */
     @PropertyName(name = "状态：0待启用，1生效中，已过期")
    private Integer state;

    

    @TableLogic
private Integer deleteFlag;

    


}
