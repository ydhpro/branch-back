package com.jd.branch.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 管理员
 * </p>
 *
 * @author ydh
 * @since 2024-08-20
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Admin implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    private String name;

    

    private String openId;

    

    private String phone;

    

    private Date createTime;

    

    private String password;

    

    /**
     * 状态：0禁用，1启用
     */
     @PropertyName(name = "状态：0禁用，1启用")
    private Integer state;

    

    private Long tenantId;

    

    @TableLogic
private Integer deleteFlag;

    


}
