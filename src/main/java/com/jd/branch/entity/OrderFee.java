package com.jd.branch.entity;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import com.jd.branch.annotation.PropertyName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * <p>
 * 单据费用
 * </p>
 *
 * @author ydh
 * @since 2024-09-02
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderFee implements Serializable {


    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    

    private Long orderId;

    

    /**
     * 物料支出
     */
     @PropertyName(name = "物料支出")
    private BigDecimal materialExpand;

    

    /**
     * 师傅安装结算价
     */
     @PropertyName(name = "师傅安装结算价")
    private BigDecimal workerInstallSettleFee;

    

    /**
     * 师傅勘测结算价
     */
     @PropertyName(name = "师傅勘测结算价")
    private BigDecimal workerSurveySettleFee;

    

    /**
     * 人工支出（师傅工单结算价+师傅总提成+通勤补贴）
     */
     @PropertyName(name = "人工支出（师傅工单结算价+师傅总提成+通勤补贴）")
    private BigDecimal workerExpand;

    

    /**
     * 其他支出
     */
     @PropertyName(name = "其他支出")
    private BigDecimal otherExpand;

    

    /**
     * 通勤补贴
     */
     @PropertyName(name = "通勤补贴")
    private BigDecimal commuteSubsidy;

    

    /**
     * 师傅总提成（套包外米数提成+增项费用提成）
     */
     @PropertyName(name = "师傅总提成（套包外米数提成+增项费用提成）")
    private BigDecimal workerCommision;

    

    /**
     * 增项收入
     */
     @PropertyName(name = "增项收入")
    private BigDecimal addIncome;

    

    /**
     * 师傅总收入计算公式
     */
     @PropertyName(name = "师傅总收入计算公式")
    private String workerIncomeFormula;

    

    /**
     * 其他收入
     */
     @PropertyName(name = "其他收入")
    private BigDecimal otherIncome;

    

    /**
     * 单据收入（主机厂结算价）
     */
     @PropertyName(name = "单据收入（主机厂结算价）")
    private BigDecimal orderIncome;

    

    private Integer deleteFlag;

    

    private Long tenantId;

    


}
