package ${package.Service};


import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.${entity};
import com.jd.branch.api.request.Req${entity}Save;
import com.jd.branch.api.request.Req${entity}Search;
import com.jd.branch.api.response.Resp${entity}Detail;
import java.util.List;


/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${entity}Service {
    PageInfo<Resp${entity}Detail> search(LoginRedisObj loginRedisObj,Req${entity}Search req${entity}Search) throws Exception;

    void removeById(LoginRedisObj loginRedisObj, Long id) throws Exception;

    void updateById(LoginRedisObj loginRedisObj, Req${entity}Save update) throws Exception;

    void saveMaster(LoginRedisObj loginRedisObj, Req${entity}Save save) throws Exception;

    Resp${entity}Detail detail(LoginRedisObj loginRedisObj, Long id) throws Exception;

    List<Resp${entity}Detail> findList(LoginRedisObj loginRedisObj, Req${entity}Search item) throws Exception;
}