package ${package.Controller};


import com.jd.branch.api.request.Req${entity}Save;
import com.jd.branch.api.request.Req${entity}Search;
import com.jd.branch.api.response.Resp${entity}Detail;
import com.jd.branch.common.BaseController;
import com.jd.branch.common.Message;
import com.jd.branch.common.PageInfo;
import com.jd.branch.common.ReqCommDel;
import com.jd.branch.entity.${entity};
import com.jd.branch.service.${entity}Service;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends BaseController {
<#else>
public class ${table.controllerName} {
</#if>
    @Autowired
    private ${table.serviceName} service;

    @PostMapping("/api/v1/${entity}")
    @ApiOperation("新增保存")
    public Message<String> save(HttpServletRequest request,@RequestBody Req${entity}Save item) throws Exception {
        service.saveMaster(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @DeleteMapping("/api/v1/${entity}")
    @ApiOperation("根据id删除")
    public Message<String> delete(HttpServletRequest request,@Validated @ModelAttribute ReqCommDel reqCommDel) throws Exception {
        service.removeById(this.getLoginObj(request), reqCommDel.getId());
        return Message.getSuccess();
    }

    @PostMapping("/api/v1/${entity}/find-list")
    @ApiOperation("列表查询")
    public  Message<List<Resp${entity}Detail>> findList(HttpServletRequest request,@RequestBody Req${entity}Search item)throws Exception {
        List<Resp${entity}Detail> list = service.findList(this.getLoginObj(request),item);
        return new Message<>(list);
    }

    @PutMapping("/api/v1/${entity}")
    @ApiOperation("更新")
    public Message<String> update(HttpServletRequest request,@RequestBody Req${entity}Save item) throws Exception {
        service.updateById(this.getLoginObj(request), item);
        return Message.getSuccess();
    }

    @GetMapping("/api/v1/${entity}/detail/{id}")
    @ApiOperation("获取详情")
    public  Message<Resp${entity}Detail> detail(HttpServletRequest request,@PathVariable(value = "id")Long id) throws Exception {
        Resp${entity}Detail byId = service.detail(this.getLoginObj(request), id);
        return new Message<>(byId);
    }

    @GetMapping("/api/v1/${entity}")
    @ApiOperation("分页查询")
    public Message<PageInfo<Resp${entity}Detail>> findPaginationNew(HttpServletRequest request,@Validated @ModelAttribute Req${entity}Search req${entity}Search) throws Exception {

        PageInfo<Resp${entity}Detail> search = service.search(this.getLoginObj(request),req${entity}Search);
        return new Message<>(search);

    }
}
</#if>
