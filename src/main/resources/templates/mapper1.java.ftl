package ${package.Mapper};

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jd.branch.entity.${entity};
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * <p>
 * ${table.comment!} Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
 @Mapper
public interface ${table.mapperName} extends ${superMapperClass}<${entity}> {

}
</#if>
