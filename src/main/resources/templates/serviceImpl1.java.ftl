package ${package.ServiceImpl};

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jd.branch.api.request.Req${entity}Save;
import com.jd.branch.api.request.Req${entity}Search;
import com.jd.branch.api.response.Resp${entity}Detail;
import com.jd.branch.common.LoginRedisObj;
import com.jd.branch.common.PageInfo;
import com.jd.branch.entity.${entity};
import com.jd.branch.mapper.${entity}Mapper;
import com.jd.branch.service.${entity}Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName}  implements ${table.serviceName} {
    private static final Logger LOGGER = LoggerFactory.getLogger( ${table.serviceImplName}.class);
    @Autowired
    private ${table.mapperName} mapper;


    @Override
    public PageInfo<Resp${entity}Detail> search(LoginRedisObj loginRedisObj, Req${entity}Search param) {
        //1.构造查询条件
        QueryWrapper<${entity}> wrapper = new QueryWrapper<>(
        ${entity}.builder().tenantId(param.getTenantId()).build());

        //        if (StrUtil.isNotEmpty(reqNoticeSearch.getKeyWord())) {
        //            wrapper.and(
        //                    andWrapper -> andWrapper.like(InfoNotice._title, reqNoticeSearch.getKeyWord())
        //                            .or().like(InfoNotice._content, reqNoticeSearch.getKeyWord()));
        //        }


        //2.分页查询
        Page<${entity}> page = new Page<>(param.getCurrentPage(), param.getPageSize());
        IPage<${entity}> iPage = mapper.selectPage(page, wrapper);

        //3.dto转换
        List<Resp${entity}Detail> details = new ArrayList<>();
        for(${entity} item:iPage.getRecords()){
            Resp${entity}Detail resp = DefindBeanUtil.copyProperties(item, Resp${entity}Detail.class);
            details.add(resp);
        }

        //4.适配分页对象
        PageInfo<Resp${entity}Detail> pageInfo = new PageInfo<>(
            param.getCurrentPage(),
            iPage.getTotal(),
            param.getPageSize(),
            details
        );


        return pageInfo;
    }

    @Override
    public List<Resp${entity}Detail> findList(LoginRedisObj loginRedisObj, Req${entity}Search req)throws Exception{
        ${entity} param = JSONObject.parseObject(JSONObject.toJSONString(req), ${entity}.class);
        List<${entity}> list = mapper.selectList(new QueryWrapper<>(param));
        List<Resp${entity}Detail> details = new ArrayList<>();
        for(${entity} item:list){
            Resp${entity}Detail detail = DefindBeanUtil.copyProperties(item, Resp${entity}Detail.class);
            details.add(detail);
        }
        return details;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeById(LoginRedisObj loginRedisObj,Long id) {
        mapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateById(LoginRedisObj loginRedisObj,Req${entity}Save item) {
        ${entity} update = DefindBeanUtil.copyProperties(item, ${entity}.class);
        mapper.updateById(update);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMaster(LoginRedisObj loginRedisObj, Req${entity}Save add){
        ${entity} save = JSONObject.parseObject(JSONObject.toJSONString(add), ${entity}.class);
        mapper.insert(save);
    }

    @Override
    public Resp${entity}Detail detail(LoginRedisObj loginRedisObj, Long id){
        ${entity} byId = mapper.selectById(id);
        Resp${entity}Detail resp = transferOne(byId);
        return resp;
    }

    private Resp${entity}Detail transferOne(${entity} item){
        Resp${entity}Detail resp = DefindBeanUtil.copyProperties(item, Resp${entity}Detail.class);
        return resp;
    }
}
</#if>
